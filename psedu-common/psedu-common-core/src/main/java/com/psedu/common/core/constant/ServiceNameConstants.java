package com.psedu.common.core.constant;

/**
 * 服务名称
 * 
 * @author ruoyi
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "psedu-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "psedu-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "psedu-file";

    /**
     * 党校基础serviceId
     */
    public static final String BASE_SERVICE = "psedu-base";

    /**
     * 党校考试serviceId
     */
    public static final String EXAM_SERVICE = "psedu-exam";
}
