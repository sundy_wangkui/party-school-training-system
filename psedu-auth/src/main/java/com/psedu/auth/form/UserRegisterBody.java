package com.psedu.auth.form;

/**
 * 普通用户注册对象
 * 
 * @author mingyue
 */
public class UserRegisterBody extends LoginBody
{
    private Long deptId;

    private String nickName;

    private String email;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
