## 一、简介
**项目简介：**

基于微服务架构的党校培训管理系统，湖南科技大学毕业生自选的毕业设计

**演示地址：**（请勿恶意修改数据，谢谢）：

- 学员端：https://dangxiao1.mingyuefusu.top
  - 账号：1805010219 密码：123456
- 管理端：https://dangxiao-back.mingyuefusu.top
  - 账号： admin 密码：admin123

**大体总结：**

前端使用Vue.js框架，UI组件库使用Element UI与Ant Design Vue，后端基于Spring Boot，使用Mybatis Plus操作MySQL数据库，使用Dubbo与OpenFeign进行跨服务调用，使用Nacos作为注册中心和配置中心，设计并实现了学员报名、学员签到、学员分组、座位分配、云直播、在线考试、结业证下载等功能模块，另外，系统模块对系统操作日志、基础参数等进行管理，网关模块对非法请求进行过滤

**意义：**

为了提高大学生对党的认识，明确入党动机，各大高校中党组织每学年都需要组织入党培训，培训分为入党积极分子培训、发展对象培训、预备党员培训，每次培训都有上百到上千人不等，党校总领，各分党校分级管理，其中涉及到培训人员的管理、培训过程的管理、结业考试管理、结业证下发等工作，培训人数多，十分耗费人力、时间，许多高校急需一套完整友好的系统帮助管理，提高学生的学习效率，提高党校的培训水平及质量。

## 二、运行指南

电脑建议16G，有Java微服务、Vue学习基础，联系QQ：284908631

### 后端运行

地址：https://gitee.com/mingyuefusu/party-school-training-system

依赖JAVA8、MySQL、Maven、Redis、Nacos2.0.3

- **后端idea导入**，maven自动下载依赖
- **修改项目配置**，psedu-auth、psedu-gateway以及psedu-moudles和psedu-visual中的所有服务的中的bootstrap.yaml，修改server-addr与server-addr为具体的Nacos地址，将完全通过Nacos获取服务配置
- **导入数据**，MySQL运行sql目录下的所有文件
- **运行nacos**，具体查阅 https://nacos.io/zh-cn/docs/quick-start.html
  - 下载2.0.3版本
  - 正确修改conf/application.properties的数据库信息，连接到psedu_nacos数据库
  - `startup.cmd -m standalone`   即可运行
  - 运行成功后，访问127.0.0.1:8848/nacos，账号密码nacos，能正确看到配置列表的配置文件成功
- **修改Nacos**中所有dev结尾的配置文件
  - redis
  - MySQL

- **idea启动服务**
  - psedu-gateway（必须）
  - psedu-auth（必须）
  - psedu-moudles中的
    - psedu-system（必须）
    - psedu-base（主要功能）
    - psedu-exam（主要功能）
    - psedu-file （头像等文件功能，建议）
    - 其他非必须，具体可以学习ruoyi

- 查看控制台是否报错，运行前端联调

### 管理端前端运行

地址https://gitee.com/mingyuefusu/party-school-training-system

前端目录位于psedu-ui，依赖node.js

- 使用`npm install --registry=https://registry.npm.taobao.org`下载依赖
- `npm run dev` 即可启动，将代理访问后端8080网关的端口，通过/dev-api进行路径匹配
- 访问 127.0.0.1:8000 ，或者查看控制台具体地址，浏览器F12查看网络情况，查看后端各个服务的控制台日志
- （选做）：psedu-ui/src/views/psedu-live/live/startLive.vue中直播的腾讯云直播相关信息才可进行直播

### 用户端前端运行

项目位于https://gitee.com/mingyuefusu/party-school-training-system-front

- `npm install --registry=https://registry.npm.taobao.org`
- `npm run serve`
- 访问127.0.0.1:8001，或者查看控制台具体地址

## 三、总体设计

**逻辑视图：**

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656745193241.png)

**功能结构图：**

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656745273898.png)

## 四、设计与实现

### 4.1 数据库设计

**培训基础类数据库表集**

| **编号** | **数据表**          | **解释**   |
| -------- | ------------------- | ---------- |
| 1        | psedu_semester      | 学期       |
| 2        | psedu_course        | 课程       |
| 3        | psedu_apply         | 培训报名   |
| 4        | psedu_semester_dept | 学期分党校 |
| 5        | psedu_group         | 小组       |
| 6        | psedu_sign_record   | 签到记录   |
| 7        | psedu_sign_launch   | 发起签到   |

**培训考试类数据库表集**

| **编号** | **数据表**          | **解释**       |
| -------- | ------------------- | -------------- |
| 1        | ex_exam             | 考试发起       |
| 2        | ex_exam_dept_status | 分党校考试信息 |
| 3        | ex_paper            | 试卷           |
| 4        | ex_question         | 候选题目       |
| 5        | ex_paper_question   | 试卷题目       |
| 6        | ex_answer_option    | 题目选项       |
| 7        | ex_answer_sheet     | 答卷           |

**系统类数据库表集**

| **编号** | **数据表**     | **解释**         |
| -------- | -------------- | ---------------- |
| 1        | sys_config     | 系统设置表       |
| 2        | sys_dept       | 部门表/分党校表  |
| 3        | sys_dict_data  | 字典数据表       |
| 4        | sys_dict_type  | 字典类型表       |
| 5        | sys_logininfor | 系统访问记录     |
| 6        | sys_menu       | 菜单权限表       |
| 7        | sys_notice     | 通知公告表       |
| 8        | sys_oper_log   | 操作日志记录     |
| 9        | sys_role_dept  | 角色和部门关联表 |
| 10       | sys_role_menu  | 角色和菜单关联表 |
| 11       | sys_user       | 用户表           |
| 12       | sys_user_role  | 用户角色表       |

**培训管理基础模块E-R图**

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656745489126.png)

**培训考试模块E-R图**

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656745564124.png)

### 4.2 用户端系统演示

功能兼容手机、PC端

首页

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748724086.png)

手机端首页

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748757201.png)

报名信息

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748810715.png)

考试列表

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748863124.png)

练习

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748995790.png)

自我测试、考试

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748882433.png)



### 4.3 管理后台系统演示

后台登录

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656747936784.png)

培训数据

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656747971415.png)

账号管理

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748020538.png)

学员报名

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748042354.png)

报名数据

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748082118.png)

拖拽分班

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748107222.png)



自动分配座位

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748211752.png)

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748244599.png)

导出结业证书

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748333314.png)

试题管理

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748520335.png)

考试管理

![](https://ming-edu.oss-cn-beijing.aliyuncs.com/1656748636170.png)



## 五、关键问题参考

**重点与难点**

重点功能为用户数据权限的控制、党课的考勤、用户座位的智能分配、考试系统的实现、结业证书的导出。

难点为确保服务安全、完成微服务间的调用、数据一致性的权衡、有服务容错机制、完成服务监控、实现CI/CD、完成用户数据权限的控制、能承受住考试时的并发流量、完成系统的需求分析、数据库设计与前端的实现。

首先进行需求分析，明确系统所有功能点，分模块划分，进行原型图设计，进行数据库设计，熟悉SpringCloud、阿里巴巴的微服务技术方案，如Open Feign、Nacos、Seata等，对系统进行分服务编码实现，搭建Jenkins实现CICD。

**关键问题**

1、服务安全，避免系统漏洞造成信息泄露。

2、微服务间调用，各个服务都是一个进程，无法直接本地调用方法。

3、数据一致性的权衡，多服务下，每个服务使用不同的数据库，用户一个操作可能需要操作多个数据库，需要考虑数据一致性。

4、服务容错，远程调用其他微服务可能会出现网络波动等异常，需要有机制避免远程调用雪崩。

5、服务监控，微服务下需要有统一的监控系统，监控各个系统的状况，提供预警，帮助问题排查。

6、CI/CD自动部署的实现，多服务下手动部署应用十分困难，需要有完善的测试部署体系。

7、用户数据权限的控制，分党校管理员只能操作本分党校的数据，党校管理员可以操作所有。

8、考试时的并发流量，考试时段，几千学员操作对系统的压力会很大，需要保证可用性、并发性、高性能。

9、系统的需求分析、数据库设计，系统涉及的功能较多，完成设计较为复杂。

10、前端的实现，前端界面较多，接口较多，交互复杂。