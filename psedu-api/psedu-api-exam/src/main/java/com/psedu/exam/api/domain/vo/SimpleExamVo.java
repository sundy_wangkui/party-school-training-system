package com.psedu.exam.api.domain.vo;

import lombok.Data;

@Data
public class SimpleExamVo {
    private Long examId;
    private String examName;
}
