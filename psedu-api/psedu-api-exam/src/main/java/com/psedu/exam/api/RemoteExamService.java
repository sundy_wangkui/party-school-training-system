package com.psedu.exam.api;

import com.psedu.exam.api.domain.vo.ExamBo;
import com.psedu.exam.api.domain.vo.SimpleExamVo;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.constant.ServiceNameConstants;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.domain.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(contextId = "remoteExamService", value = ServiceNameConstants.EXAM_SERVICE)
public interface RemoteExamService {

    @GetMapping("/exam/getExamsBySemeIds/{deptId}/{semeIds}")
    public R<List<ExamBo>> getExamsBySemeIdsAndDeptId(
            @PathVariable("deptId") Long deptId,
            @PathVariable("semeIds") List<Long> semeIds,
            @RequestHeader(SecurityConstants.FROM_SOURCE) String source
    );

    @GetMapping("/exam/getExamSelectOptions")
    public R<List<SimpleExamVo>> getExamSelectOptions(
            @RequestHeader(SecurityConstants.FROM_SOURCE) String source
    );
}