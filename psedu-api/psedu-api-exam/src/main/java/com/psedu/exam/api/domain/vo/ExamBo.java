package com.psedu.exam.api.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.psedu.common.core.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ExamBo {
    /** 考试ID */
    private Long examId;

    /** 学期ID */
    private Long semeId;

    /** 考试名称 */
    private String examName;

    /** 考试状态;0关闭，1允许考试 */
    private Integer status;

    /** 允许分党校控制具体考试时间;0不交给分党校，1交给分党校 */
    private Integer controlFlag;

    /** 时间限制;0限制，1不限制 */
    private Integer timeLimitStatus;

    /** 总时间（分钟） */
    private Long totalTime;

    /** 最大重复考试次数 */
    private Long maxRepeatCount;

    /** 是否正式考试 */
    private Integer officialExam;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
}
