package com.psedu.system.api.factory;

import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.system.api.RemoteDeptService;
import com.psedu.system.api.domain.SysFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 部门服务降级处理
 * 
 * @author ruoyi
 */
//@Component
//public class RemoteDeptFallbackFactory implements FallbackFactory<RemoteDeptService>
//{
//    private static final Logger log = LoggerFactory.getLogger(RemoteDeptFallbackFactory.class);
//
//    @Override
//    public RemoteDeptService create(Throwable throwable)
//    {
//        log.error("系统服务调用失败:{}", throwable.getMessage());
//        return new RemoteDeptService()
//        {
//            @Override
//            public AjaxResult getDeptNameByDeptId(List<Long> deptIdList, String source) {
//                return AjaxResult.error("通过ids获取部门信息失败");
//            }
//        };
//    }
//}
