package com.psedu.system.api.factory;

import com.psedu.common.core.constant.SecurityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import com.psedu.common.core.domain.R;
import com.psedu.system.api.RemoteUserService;
import com.psedu.system.api.domain.SysUser;
import com.psedu.system.api.model.LoginUser;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;

/**
 * 用户服务降级处理
 * 
 * @author ruoyi
 */
@Component
public class RemoteUserFallbackFactory implements FallbackFactory<RemoteUserService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteUserFallbackFactory.class);

    @Override
    public RemoteUserService create(Throwable throwable)
    {
        log.error("用户服务调用失败:{}", throwable.getMessage());
        return new RemoteUserService()
        {
            @Override
            public R<LoginUser> getUserInfo(String username, String source)
            {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }

            @Override
            public R<Boolean> registerUserInfo(SysUser sysUser, String source)
            {
                return R.fail("注册用户失败:" + throwable.getMessage());
            }
            @Override
            public R<Long> getPsStudentNumByDeptId(Long deptId, String source) {
                return R.fail("获取部门注册人数失败" +throwable.getMessage());
            }
            @Override
            public R<Long> getPsStudentNum(String source) {
                return R.fail("获取部门注册人数失败" +throwable.getMessage());
            }

            @Override
            public R<Map<Long, String>> getUserProfile(List<Long> userIdList, String source) {
                return R.fail("获取用户头像失败" +throwable.getMessage());
            }

            @Override
            public R<Map<Long, SysUser>> getUserInfo(List<Long> userIdList, String source) {
                return R.fail("获取用户信息失败" +throwable.getMessage());
            }
        };
    }
}
