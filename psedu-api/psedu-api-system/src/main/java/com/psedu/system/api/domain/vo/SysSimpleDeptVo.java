package com.psedu.system.api.domain.vo;

import lombok.Data;

@Data
public class SysSimpleDeptVo {
    /** 部门ID */
    private Long deptId;

    /** 部门名称 */
    private String deptName;
}
