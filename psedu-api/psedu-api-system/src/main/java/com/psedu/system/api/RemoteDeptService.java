package com.psedu.system.api;

import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.constant.ServiceNameConstants;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.domain.AjaxResult;
//import com.psedu.system.api.factory.RemoteDeptFallbackFactory;
import com.psedu.system.api.domain.vo.SysSimpleDeptVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

// TODO fackback
@FeignClient(contextId = "remoteDeptService", value = ServiceNameConstants.SYSTEM_SERVICE)
public interface RemoteDeptService {

    /**
     * 通过部门ID获取部门名称
     * @param deptIdList
     * @param source
     */
    @GetMapping("/dept/getDeptNameByDeptIds")
    public R<List<SysSimpleDeptVo>> getDeptNameByDeptId(@RequestParam("deptIdList") List<Long> deptIdList,
                                                        @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取所有党校部门名称
     */
    @GetMapping("/dept/getAllDeptNameMap")
    public R<Map<Long, String>> getAllDeptNameMap(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 分党校选项
     * @return 分党校选项
     */
    @GetMapping("/dept/psDeptList")
    public R<List<SysSimpleDeptVo>> psDeptlist();

    /**
     * 权限之内的分党校
     * @return 分党校选项
     */
    @GetMapping("/dept/getPsDeptAndChildDeptList/{deptId}")
    public R<List<SysSimpleDeptVo>> getPsDeptAndChildDeptList(@PathVariable("deptId") Long deptId);
}
