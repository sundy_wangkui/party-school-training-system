package com.psedu.system.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.constant.ServiceNameConstants;
import com.psedu.common.core.domain.R;
import com.psedu.system.api.domain.SysUser;
import com.psedu.system.api.factory.RemoteUserFallbackFactory;
import com.psedu.system.api.model.LoginUser;

import java.util.List;
import java.util.Map;

/**
 * 用户服务
 * 
 * @author ruoyi
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService
{
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/user/info/{username}")
    public R<LoginUser> getUserInfo(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/user/getUserProfile")
    public R<Map<Long, String>> getUserProfile(@RequestParam("userIdList") List<Long> userIdList,
                                               @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/user/getUserInfo")
    public R<Map<Long, SysUser>> getUserInfo(@RequestParam("userIdList") List<Long> userIdList,
                                               @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 注册用户信息
     *
     * @param sysUser 用户信息
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("/user/register")
    public R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/user/getPsStudentNumByDeptId/{deptId}")
    public R<Long> getPsStudentNumByDeptId(@PathVariable("deptId") Long deptId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/user/getPsStudentNum")
    public R<Long> getPsStudentNum(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
