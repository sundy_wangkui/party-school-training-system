package com.psedu.base.api.domain.dto;

import lombok.Data;

@Data
public class SemesterTrainObjectDTO {
    private Long semesterId;
    private Integer trainObject;
}
