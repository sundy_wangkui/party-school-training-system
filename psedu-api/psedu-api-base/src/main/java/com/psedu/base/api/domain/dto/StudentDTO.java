package com.psedu.base.api.domain.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StudentDTO {
    private Long applyId;
    private Long semesterId;
    private String hnusterId;
    private String studentName;
    private BigDecimal scores;
}
