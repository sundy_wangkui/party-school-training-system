package com.psedu.base.api;

import com.psedu.base.api.domain.dto.SemesterTrainObjectDTO;
import com.psedu.base.api.domain.dto.StudentDTO;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.constant.ServiceNameConstants;
import com.psedu.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.math.BigDecimal;
import java.util.List;

@FeignClient(contextId = "remoteStudentService", value = ServiceNameConstants.BASE_SERVICE)
public interface RemoteStudentService {

    @GetMapping("/student/studentInfo/{semesterId}/{psUserId}")
    R<StudentDTO> getStudentInfo(@PathVariable("semesterId") Long semesterId,
                                 @PathVariable("psUserId") Long psUserId,
                                 @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/student/updateExamScore/{applyId}/{score}")
    R updateExamStudentScore(@PathVariable("applyId") Long applyId,
                                         @PathVariable("score") BigDecimal score,
                                         @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/student/getAllPassSemester/{psUserId}")
    R<List<Long>> getAllPassSemester(@PathVariable("psUserId") Long psUserId,
                                     @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
