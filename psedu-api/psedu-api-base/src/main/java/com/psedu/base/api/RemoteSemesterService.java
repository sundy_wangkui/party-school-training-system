package com.psedu.base.api;

import com.psedu.base.api.domain.dto.SemesterTrainObjectDTO;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.constant.ServiceNameConstants;
import com.psedu.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@FeignClient(contextId = "remoteSemesterService", value = ServiceNameConstants.BASE_SERVICE)
public interface RemoteSemesterService {

    @GetMapping("/semester/getTrainObjectBySemesterId/{semeId}")
    R<SemesterTrainObjectDTO> getTrainObjectBySemesterId(@PathVariable("semeId") Long semeId,
                                                         @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
