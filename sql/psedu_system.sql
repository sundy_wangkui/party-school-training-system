SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-green', 'Y', 'admin', '2021-12-18 09:34:52', 'admin', '2022-01-23 20:44:56', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-12-18 09:34:52', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-12-18 09:34:52', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2021-12-18 09:34:52', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 229 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '党校', 0, '党校校长', '15888888888', 'ming@qq.com', '0', '0', 'admin', '2021-12-18 09:34:44', 'admin', '2022-04-05 15:24:48');
INSERT INTO `sys_dept` VALUES (200, 100, '0,100', '计算机学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:25', '', NULL);
INSERT INTO `sys_dept` VALUES (201, 100, '0,100', '土木学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (202, 100, '0,100', '资源与安全学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (203, 100, '0,100', '建筑与艺术设计学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (204, 100, '0,100', '教育学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (205, 100, '0,100', '机电工程学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (206, 100, '0,100', '信息与电气工程学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (207, 100, '0,100', '化学化工学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (208, 100, '0,100', '数学与计算科学学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (209, 100, '0,100', '物理与电子科学学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (210, 100, '0,100', '生命科学与健康学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (211, 100, '0,100', '建筑与艺术设计学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (212, 100, '0,100', '人文学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (213, 100, '0,100', '外国语学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (214, 100, '0,100', '马克思主义学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (215, 100, '0,100', '教育学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (216, 100, '0,100', '商学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (217, 100, '0,100', '齐白石艺术学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (218, 100, '0,100', '体育学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (219, 100, '0,100', '商学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (220, 100, '0,100', '法学与公共管理学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (221, 100, '0,100', '材料科学与工程学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (222, 100, '0,100', '潇湘学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (223, 100, '0,100', '后勤党委', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (224, 100, '0,100', '机关党委', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (225, 100, '0,100', '附校党总支', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (226, 100, '0,100', '离退休党委', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (227, 100, '0,100', '图书馆党总支', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);
INSERT INTO `sys_dept` VALUES (228, 100, '0,100', '国际教育学院', 0, NULL, NULL, NULL, '0', '0', 'admin', '2022-02-02 23:39:41', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 0, '入党积极分子', '0', 'ps_train_object', NULL, 'default', 'N', '0', 'admin', '2022-02-09 12:27:45', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (101, 0, '发展对象', '1', 'ps_train_object', NULL, 'default', 'N', '0', 'admin', '2022-02-09 12:27:56', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (102, 0, '预备党员', '2', 'ps_train_object', NULL, 'default', 'N', '0', 'admin', '2022-02-09 12:28:10', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 0, '待审核', '0', 'apply_status', NULL, 'warning', 'N', '0', 'admin', '2022-03-07 15:30:19', 'admin', '2022-03-07 16:20:11', NULL);
INSERT INTO `sys_dict_data` VALUES (104, 0, '通过', '1', 'apply_status', NULL, 'success', 'N', '0', 'admin', '2022-03-07 15:30:30', 'admin', '2022-03-07 16:20:53', NULL);
INSERT INTO `sys_dict_data` VALUES (105, 0, '不通过', '2', 'apply_status', NULL, 'danger', 'N', '0', 'admin', '2022-03-07 15:30:41', 'admin', '2022-03-07 16:20:59', NULL);
INSERT INTO `sys_dict_data` VALUES (106, 10, '简答题', '0', 'question_type', NULL, 'default', 'N', '0', 'admin', '2022-03-30 10:01:32', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 20, '单选题', '1', 'question_type', NULL, 'default', 'N', '0', 'admin', '2022-03-30 10:01:49', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (108, 30, '判断题', '2', 'question_type', NULL, 'default', 'N', '0', 'admin', '2022-03-30 10:01:59', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (109, 40, '多选题', '3', 'question_type', NULL, 'default', 'N', '0', 'admin', '2022-03-30 10:02:08', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (110, 50, '填空题', '4', 'question_type', NULL, 'default', 'N', '0', 'admin', '2022-03-30 10:02:20', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (111, 10, '正式考试', '1', 'exam_type', NULL, 'success', 'N', '0', 'admin', '2022-04-06 10:35:43', 'admin', '2022-04-06 10:39:56', NULL);
INSERT INTO `sys_dict_data` VALUES (112, 20, '模拟考试', '0', 'exam_type', NULL, 'warning', 'N', '0', 'admin', '2022-04-06 10:36:07', 'admin', '2022-04-06 10:40:04', NULL);
INSERT INTO `sys_dict_data` VALUES (113, 10, '开启', '1', 'exam_status', NULL, 'success', 'N', '0', 'admin', '2022-04-06 10:41:06', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 20, '关闭', '0', 'exam_status', NULL, 'warning', 'N', '0', 'admin', '2022-04-06 10:41:19', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-12-18 09:34:50', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-12-18 09:34:51', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, '培训对象', 'ps_train_object', '0', 'admin', '2022-02-09 12:27:09', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (101, '申请审核状态', 'apply_status', '0', 'admin', '2022-03-07 15:29:55', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (102, '题目类型', 'question_type', '0', 'admin', '2022-03-30 10:00:54', 'admin', '2022-03-30 10:01:02', NULL);
INSERT INTO `sys_dict_type` VALUES (103, '考试类型', 'exam_type', '0', 'admin', '2022-04-06 10:35:16', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (104, '考试状态', 'exam_status', '0', 'admin', '2022-04-06 10:40:49', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-12-18 09:34:52', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-12-18 09:34:52', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-12-18 09:34:52', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '提示信息',
  `access_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 626 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2081 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 40, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2021-12-18 09:34:45', 'admin', '2022-04-05 15:23:29', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 100, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2021-12-18 09:34:45', 'admin', '2022-04-05 15:23:21', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 110, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2021-12-18 09:34:45', 'admin', '2022-04-05 15:23:37', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2021-12-18 09:34:45', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2021-12-18 09:34:45', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2021-12-18 09:34:45', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2021-12-18 09:34:45', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2021-12-18 09:34:45', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2021-12-18 09:34:45', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2021-12-18 09:34:45', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2021-12-18 09:34:45', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2021-12-18 09:34:45', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2021-12-18 09:34:45', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2021-12-18 09:34:45', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, 'Sentinel控制台', 2, 3, 'http://localhost:8718', '', '', 0, 0, 'C', '0', '0', 'monitor:sentinel:list', 'sentinel', 'admin', '2021-12-18 09:34:45', '', NULL, '流量控制菜单');
INSERT INTO `sys_menu` VALUES (112, 'Nacos控制台', 2, 4, 'http://localhost:8848/nacos', '', '', 0, 0, 'C', '0', '0', 'monitor:nacos:list', 'nacos', 'admin', '2021-12-18 09:34:45', '', NULL, '服务治理菜单');
INSERT INTO `sys_menu` VALUES (113, 'Admin控制台', 2, 5, 'http://127.0.0.1/:9100/login', '', '', 0, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2021-12-18 09:34:45', 'admin', '2022-04-05 15:20:41', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2021-12-18 09:34:45', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2021-12-18 09:34:45', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'http://localhost:8080/swagger-ui/index.html', '', '', 0, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2021-12-18 09:34:45', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'system/operlog/index', '', 1, 0, 'C', '0', '0', 'system:operlog:list', 'form', 'admin', '2021-12-18 09:34:45', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'system/logininfor/index', '', 1, 0, 'C', '0', '0', 'system:logininfor:list', 'logininfor', 'admin', '2021-12-18 09:34:45', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2021-12-18 09:34:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2021-12-18 09:34:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2021-12-18 09:34:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2021-12-18 09:34:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2021-12-18 09:34:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2021-12-18 09:34:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2021-12-18 09:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:query', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:remove', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:operlog:export', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:query', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:remove', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:logininfor:export', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2021-12-18 09:34:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '学员管理', 0, 10, 'base', NULL, NULL, 1, 0, 'M', '0', '0', '', 'user', 'admin', '2022-01-23 21:02:02', 'admin', '2022-04-05 15:22:27', '');
INSERT INTO `sys_menu` VALUES (2001, '账号信息', 2000, 1, '/userInfo', 'system/user/index', NULL, 1, 0, 'C', '0', '0', 'system:user:list', '#', 'admin', '2022-01-23 21:02:28', 'admin', '2022-04-20 11:54:10', '');
INSERT INTO `sys_menu` VALUES (2002, '学员报名', 2000, 2, 'apply', 'psedu-base/apply/index', NULL, 1, 0, 'C', '0', '0', 'psedu-base:apply:list', '#', 'admin', '2022-01-23 21:03:37', 'admin', '2022-02-02 18:53:55', '');
INSERT INTO `sys_menu` VALUES (2003, '培训模块', 0, 20, 'train', NULL, NULL, 1, 0, 'M', '0', '0', '', 'row', 'admin', '2022-02-09 12:33:33', 'admin', '2022-04-05 15:22:56', '');
INSERT INTO `sys_menu` VALUES (2004, '学期管理', 2003, 10, 'semester', 'psedu-base/semester/index', NULL, 1, 0, 'C', '0', '0', 'psedu-base:semester:list', '#', 'admin', '2022-02-09 12:42:03', 'admin', '2022-04-19 18:55:27', '');
INSERT INTO `sys_menu` VALUES (2005, '学期课程', 2003, 20, 'course', 'psedu-base/course/index', NULL, 1, 1, 'C', '1', '0', 'psedu-base:course:list', 'build', 'admin', '2022-02-10 17:54:07', 'admin', '2022-04-19 18:55:37', '');
INSERT INTO `sys_menu` VALUES (2006, '新增', 2005, 0, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:course:add', '#', 'admin', '2022-02-15 11:40:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '编辑', 2005, 1, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:course:edit', '#', 'admin', '2022-02-15 11:40:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '详细', 2005, 3, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:course:query', '#', 'admin', '2022-02-15 11:50:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '删除', 2005, 2, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:course:remove', '#', 'admin', '2022-02-15 11:51:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '小组管理', 2003, 30, '/psedu-base/group', 'psedu-base/group/index', NULL, 1, 0, 'C', '0', '0', 'psedu-base:group:list', '#', 'admin', '2022-03-07 17:19:22', 'admin', '2022-04-19 18:55:55', '');
INSERT INTO `sys_menu` VALUES (2011, '培训总览', 2003, 5, 'trainData', 'psedu-base/semesterDept/index', NULL, 1, 1, 'C', '0', '0', 'psedu-base:semesterDept:query', '#', 'admin', '2022-03-12 13:22:53', 'admin', '2022-04-19 18:58:20', '');
INSERT INTO `sys_menu` VALUES (2012, '座位分配', 2003, 40, 'train/assignSeat', 'psedu-base/seat/index', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-03-18 17:01:45', 'admin', '2022-04-19 18:56:18', '');
INSERT INTO `sys_menu` VALUES (2013, '考试模块', 0, 30, 'exam', NULL, NULL, 1, 0, 'M', '0', '0', '', 'edit', 'admin', '2022-03-29 14:32:44', 'admin', '2022-04-05 15:23:12', '');
INSERT INTO `sys_menu` VALUES (2014, '题目选项', 2013, 100, 'answerOption', 'psedu-exam/answerOption/index', NULL, 1, 0, 'C', '1', '0', 'psedu-exam:answerOption:list', '#', 'admin', '2022-03-29 14:35:02', 'admin', '2022-05-10 17:45:52', '');
INSERT INTO `sys_menu` VALUES (2015, '答卷', 2013, 120, 'answerSheet', 'psedu-exam/answerSheet/index', NULL, 1, 0, 'C', '0', '0', 'psedu-exam:answerSheet:list', '#', 'admin', '2022-03-30 10:08:18', 'admin', '2022-04-19 12:35:31', '答卷菜单');
INSERT INTO `sys_menu` VALUES (2016, '答卷查询', 2015, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:answerSheet:query', '#', 'admin', '2022-03-30 10:08:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '答卷新增', 2015, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:answerSheet:add', '#', 'admin', '2022-03-30 10:08:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '答卷修改', 2015, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:answerSheet:edit', '#', 'admin', '2022-03-30 10:08:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2019, '答卷删除', 2015, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:answerSheet:remove', '#', 'admin', '2022-03-30 10:08:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '答卷导出', 2015, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:answerSheet:export', '#', 'admin', '2022-03-30 10:08:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2021, '考试发起', 2013, 20, 'exam', 'psedu-exam/exam/index', NULL, 1, 0, 'C', '0', '0', 'psedu-exam:exam:list', '#', 'admin', '2022-03-30 10:08:18', 'admin', '2022-04-19 12:35:07', '考试发起菜单');
INSERT INTO `sys_menu` VALUES (2022, '考试发起查询', 2021, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:exam:query', '#', 'admin', '2022-03-30 10:08:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '考试发起新增', 2021, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:exam:add', '#', 'admin', '2022-03-30 10:08:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '考试发起修改', 2021, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:exam:edit', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '考试发起删除', 2021, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:exam:remove', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '考试发起导出', 2021, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:exam:export', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2027, '试卷', 2013, 110, 'paper', 'psedu-exam/paper/index', NULL, 1, 0, 'C', '0', '0', 'psedu-exam:paper:list', '#', 'admin', '2022-03-30 10:08:19', 'admin', '2022-04-19 12:35:26', '试卷菜单');
INSERT INTO `sys_menu` VALUES (2028, '试卷查询', 2027, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paper:query', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '试卷新增', 2027, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paper:add', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '试卷修改', 2027, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paper:edit', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '试卷删除', 2027, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paper:remove', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '试卷导出', 2027, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paper:export', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '试卷题目', 2013, 130, 'paperQuestion', 'psedu-exam/paperQuestion/index', NULL, 1, 0, 'C', '0', '0', 'psedu-exam:paperQuestion:list', '#', 'admin', '2022-03-30 10:08:19', 'admin', '2022-04-19 12:36:10', '试卷题目菜单');
INSERT INTO `sys_menu` VALUES (2034, '试卷题目查询', 2033, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paperQuestion:query', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '试卷题目新增', 2033, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paperQuestion:add', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '试卷题目修改', 2033, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paperQuestion:edit', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '试卷题目删除', 2033, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paperQuestion:remove', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '试卷题目导出', 2033, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:paperQuestion:export', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '分党校考试', 2013, 30, 'examDeptStatus', 'psedu-exam/examDeptStatus/index', NULL, 1, 0, 'C', '0', '0', 'psedu-exam:examDeptStatus:list', '#', 'admin', '2022-03-30 10:08:19', 'admin', '2022-04-19 13:22:14', '分党校考试信息菜单');
INSERT INTO `sys_menu` VALUES (2040, '分党校考试信息查询', 2039, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:examDeptStatus:query', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '分党校考试信息新增', 2039, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:examDeptStatus:add', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '分党校考试信息修改', 2039, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:examDeptStatus:edit', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '分党校考试信息删除', 2039, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:examDeptStatus:remove', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '分党校考试信息导出', 2039, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:examDeptStatus:export', '#', 'admin', '2022-03-30 10:08:19', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '候选题目', 2013, 10, 'question', 'psedu-exam/question/index', NULL, 1, 0, 'C', '0', '0', 'psedu-exam:question:list', '#', 'admin', '2022-03-30 10:08:19', 'admin', '2022-04-19 12:34:50', '候选题目菜单');
INSERT INTO `sys_menu` VALUES (2046, '候选题目查询', 2045, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:question:query', '#', 'admin', '2022-03-30 10:08:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '候选题目新增', 2045, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:question:add', '#', 'admin', '2022-03-30 10:08:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '候选题目修改', 2045, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:question:edit', '#', 'admin', '2022-03-30 10:08:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '候选题目删除', 2045, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:question:remove', '#', 'admin', '2022-03-30 10:08:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '候选题目导出', 2045, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-exam:question:export', '#', 'admin', '2022-03-30 10:08:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '培训成绩', 2003, 50, 'trainScore', 'psedu-base/trainData/trainScore/index', NULL, 1, 0, 'C', '0', '0', '', '#', 'admin', '2022-04-19 18:59:09', 'admin', '2022-04-19 19:01:20', '');
INSERT INTO `sys_menu` VALUES (2052, '查看小组', 2010, 1, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:group:query', '#', 'admin', '2022-04-19 22:54:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '新增小组', 2010, 1, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:group:add', '#', 'admin', '2022-04-19 22:55:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '编辑小组', 2010, 1, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:group:edit', '#', 'admin', '2022-04-19 22:55:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '删除小组', 2010, 1, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:group:remove', '#', 'admin', '2022-04-19 22:55:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '编辑学期分党校', 2011, 10, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:semesterDept:edit', '#', 'admin', '2022-04-19 23:07:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2057, '编辑报名', 2002, 10, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:apply:edit', '#', 'admin', '2022-04-20 10:44:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2058, '删除报名', 2002, 20, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:apply:remove', '#', 'admin', '2022-04-20 10:45:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2059, '导出', 2002, 30, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:apply:export', '#', 'admin', '2022-04-20 10:45:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '添加报名', 2002, 40, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:apply:add', '#', 'admin', '2022-04-20 10:45:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2061, '查询', 2002, 50, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:apply:query', '#', 'admin', '2022-04-20 10:47:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2062, '查询', 2001, 10, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-04-20 11:51:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2063, '用户新增', 2001, 20, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-04-20 11:51:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2064, '用户编辑', 2001, 40, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-04-20 11:51:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2065, '用户删除', 2001, 30, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-04-20 11:52:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2066, '重置密码', 2001, 50, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-04-20 11:52:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, '直播', 2003, 70, 'psedu-live/live/start', 'psedu-live/live/startLive', NULL, 1, 1, 'C', '1', '0', '', '#', 'admin', '2022-05-09 17:24:34', 'admin', '2022-05-23 17:30:21', '');
INSERT INTO `sys_menu` VALUES (2068, '查看', 2004, 10, '', NULL, NULL, 1, 0, 'F', '0', '0', 'psedu-base:semester:list', '#', 'admin', '2022-05-10 16:02:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, '签到记录', 2003, 1, 'signRecord', 'psedu-base/signRecord/index', NULL, 1, 0, 'C', '1', '0', 'psedu-base:signRecord:list', '#', 'admin', '2022-05-16 19:59:43', 'admin', '2022-05-23 17:23:44', '签到记录菜单');
INSERT INTO `sys_menu` VALUES (2070, '签到记录查询', 2069, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signRecord:query', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2071, '签到记录新增', 2069, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signRecord:add', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2072, '签到记录修改', 2069, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signRecord:edit', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2073, '签到记录删除', 2069, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signRecord:remove', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2074, '签到记录导出', 2069, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signRecord:export', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2075, '发起签到', 2003, 1, 'signLaunch', 'psedu-base/signLaunch/index', NULL, 1, 0, 'C', '1', '0', 'psedu-base:signLaunch:list', '#', 'admin', '2022-05-16 19:59:44', 'admin', '2022-05-23 17:23:51', '发起签到菜单');
INSERT INTO `sys_menu` VALUES (2076, '发起签到查询', 2075, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signLaunch:query', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2077, '发起签到新增', 2075, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signLaunch:add', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2078, '发起签到修改', 2075, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signLaunch:edit', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2079, '发起签到删除', 2075, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signLaunch:remove', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2080, '发起签到导出', 2075, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'psedu-base:signLaunch:export', '#', 'admin', '2022-05-16 19:59:44', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '关于举办2020年下半年预备党员培训班的通知 ', '1', 0x3C703E09266E6273703BE59084E58886E5859AE6A0A1E38081E79BB4E5B19EE5859AE5A794EFBC9A3C2F703E3C703E09E4B8BAE8BF9BE4B880E6ADA5E5A29EE5BCBAE9A284E5A487E5859AE59198E79A84E5859AE680A7E4BFAEE585BBEFBC8CE58AA0E5BCBAE5AFB9E9A284E5A487E5859AE59198E79A84E69599E882B2E7AEA1E79086EFBC8CE68F90E9AB98E6809DE683B3E8A789E6829FEFBC8CE7ABAFE6ADA3E585B6E585A5E5859AE58AA8E69CBAEFBC8CE6A0B9E68DAEE6B996E58D97E7A791E68A80E5A4A7E5ADA6E5859AE6A0A1E59FB9E8AEADE5B7A5E4BD9CE8AEA1E58892EFBC8CE7BB8FE7A094E7A9B6EFBC8CE586B3E5AE9AE4B8BEE58A9E32303230E5B9B4E4B88BE58D8AE5B9B4E9A284E5A487E5859AE59198E59FB9E8AEADE78FADE38082E78EB0E5B086E69C89E585B3E5B7A5E4BD9CE9809AE79FA5E5A682E4B88BEFBC9A3C2F703E3C703E093C7374726F6E673EE4B880E38081266E6273703BE59FB9E8AEADE79BAEE79A843C2F7374726F6E673E3C2F703E3C703E09E69599E882B2E5BC95E5AFBCE9A284E5A487E5859AE59198E6B7B1E588BBE5ADA6E4B9A0E9A286E4BC9AE4B9A0E8BF91E5B9B3E696B0E697B6E4BBA3E4B8ADE59BBDE789B9E889B2E7A4BEE4BC9AE4B8BBE4B989E6809DE683B3E79A84E7B2BEE7A59EE5AE9EE8B4A8E5928CE4B8B0E5AF8CE58685E6B6B5EFBC8CE6B7B1E585A5E4BA86E8A7A3E5859AE79A84E58E86E58FB2EFBC8CE4B8A5E6A0BCE981B5E5AE88E5859AE7ABA0E4B88EE5859AE7BAAAE5859AE8A784EFBC8CE68F90E58D87E5859AE680A7E8A782E5BFB5E5928CE5859AE680A7E4BFAEE585BBEFBC8CE59D9AE5AE9AE79086E683B3E4BFA1E5BFB5EFBC8CE6A091E789A2E2809CE59B9BE4B8AAE6848FE8AF86E2809DEFBC8CE59D9AE5AE9AE2809CE59B9BE4B8AAE887AAE4BFA1E2809DEFBC8CE69BB4E58AA0E59D9AE5AE9AE887AAE8A789E59CB0E5819AE588B0E2809CE4B8A4E4B8AAE7BBB4E68AA4E2809DEFBC8CE58F91E68CA5E5859AE59198E79A84E58588E9948BE6A8A1E88C83E4BD9CE794A8EFBC8CE68EA8E58AA8E59FBAE5B182E5859AE7BB84E7BB87E6B4BBE58A9BE79A84E698BEE89197E68F90E58D87E38082E4B88DE696ADE5BCBAE58C96E5859AE59198E6848FE8AF86EFBC8CE4B88DE696ADE5A29EE5BCBAE9A284E5A487E5859AE59198E79A84E7A4BEE4BC9AE8B4A3E4BBBBE6849FE5928CE58E86E58FB2E4BDBFE591BDE6849FEFBC8CE4BDBFE4BB96E4BBACE79A84E9A9ACE5858BE6809DE4B8BBE4B989E79086E8AEBAE6B0B4E5B9B3E38081E6809DE683B3E694BFE6B2BBE8A789E6829FE5BE97E588B0E8BF9BE4B880E6ADA5E68F90E9AB98EFBC8CE8BF9BE4B880E6ADA5E7ABAFE6ADA3E585A5E5859AE58AA8E69CBAEFBC8CE4BDBFE4B98BE68890E4B8BAE4B880E5908DE59088E6A0BCE79A84E585B1E4BAA7E5859AE59198E380823C2F703E3C703E093C7374726F6E673EE4BA8CE38081266E6273703BE59FB9E8AEADE5AFB9E8B1A13C2F7374726F6E673E3C2F703E3C703E0932303230E5B9B4E4B88AE58D8AE5B9B4E58F91E5B195E79A84E69599E5B7A5E38081E5ADA6E7949FE9A284E5A487E5859AE59198E5928CE69CAAE59FB9E8AEADE8BF87E79A84E9A2843C2F703E3C703E09E5A487E5859AE59198E380823C2F703E3C703E093C7374726F6E673EE4B889E38081266E6273703BE59FB9E8AEADE697B6E997B43C2F7374726F6E673E3C2F703E3C703E0932303230E5B9B43130E69C883235E697A5E887B33131E69C883137E697A5E380823C2F703E3C703E093C7374726F6E673EE59B9BE38081266E6273703BE59FB9E8AEADE696B9E5BC8F3C2F7374726F6E673E3C2F703E3C703E09E4B8BAE5819AE5A5BDE5B8B8E68081E58C96E796ABE68385E998B2E68EA7E5B7A5E4BD9CEFBC8CE7A1AEE4BF9DE59FB9E8AEADE8B4A8E9878FEFBC8CE69CACE6ACA1E58F91E5B195E5AFB9E8B1A1E59FB9E8AEADE98787E794A8E7BABFE4B88AE5928CE7BABFE4B88BE79BB8E7BB93E59088E79A84E59FB9E8AEADE5BDA2E5BC8FE380823C2F703E3C703E09266E6273703B3C2F703E3C703E093C7374726F6E673EE4BA94E38081266E6273703BE59FB9E8AEADE58685E5AEB93C2F7374726F6E673E3C2F703E3C703E09E8AFA6E8A781E99984E4BBB631E380823C2F703E3C703E093C7374726F6E673EE585ADE38081E59FB9E8AEADE8A681E6B1823C2F7374726F6E673E3C2F703E3C703E09EFBC88E4B880EFBC89E9A284E5A487E5859AE59198E5BA94E4BBA5E9A5B1E6BBA1E79A84E783ADE68385E5928CE4B893E6B3A8E79A84E7B2BEE7A59EE58F82E58AA0E5ADA6E4B9A0EFBC8CE4B8A5E6A0BCE981B5E5AE88E5859AE6A0A1E79A84E8A784E7ABA0E588B6E5BAA6E5928CE7BAAAE5BE8BE380823C2F703E3C703E09EFBC88E4BA8CEFBC89E59CA8E5A496E5AE9EE4B9A0E79A84E9A284E5A487E5859AE59198E8A681E98787E58F96E2809CE5AF84E5ADA6E2809DE68896E2809CE98081E5ADA6E2809DE79A84E5BDA2E5BC8FE8BF9BE8A18CEFBC8CE5B9B6E68AA5E5859AE6A0A1E5A487E6A188E380823C2F703E3C703E093C7374726F6E673EE4B883E38081E88083E6A0B8E696B9E5BC8F3C2F7374726F6E673E3C2F703E3C703E09EFBC88E4B880EFBC89E88083E6A0B8E58685E5AEB9E58C85E68BACE88083E58BA4E5928CE5ADA6E4B9A0E5B08FE7BB93E380823C2F703E3C703E0931E38081E88083E58BA4E590ABE587BAE58BA4E78E87E38081E7BAAAE5BE8BE38081E5ADA6E4B9A0E68081E5BAA6E7AD89E696B9E99DA2E380823C2F703E3C703E0932E38081E5ADA6E4B9A0E5B08FE7BB93EFBC9AE5ADA6E59198E8AEA4E79C9FE5819AE5A5BDE5ADA6E4B9A0E680BBE7BB93EFBC8CE5A1ABE58699E3808AE6B996E58D97E7A791E68A80E5A4A7E5ADA6E9A284E5A487E5859AE59198E59FB9E8AEADE78FADE5ADA6E59198E799BBE8AEB0E8A1A8E3808BEFBC8CE692B0E58699E5ADA6E4B9A0E5BF83E5BE9731E7AF87EFBC8C32303030E5AD97E5B7A6E58FB3EFBC8CE4BAA4E59084E58886E5859AE6A0A1EFBC8CE5ADA6E59198E799BBE8AEB0E8A1A8E5B086E4BD9CE4B8BAE9A284E5A487E5859AE59198E8BDACE6ADA3E69D90E69699E4B98BE4B880E380823C2F703E3C703E09EFBC88E4BA8CEFBC89E4B8BAE4BF9DE99A9CE9A284E5A487E5859AE59198E69BB4E5A5BDE79A84E58F82E58AA0E59FB9E8AEADE5ADA6E4B9A0EFBC8CE59084E58886E6A0A1E8A681E8AEA4E79C9FE7BB84E7BB87EFBC8CE4B8A5E6A0BCE8A681E6B182EFBC8CE7A1AEE5AE9AE4B880E5908DE5ADA6E59198E8B49FE8B4A3E88083E58BA4E5B7A5E4BD9CE38082E5AFB9E5ADA6E59198E5ADA6E4B9A0E68385E586B5E58F8AE697B6E9809AE68AA5EFBC8CE5B9B6E4BD9CE4B8BAE9A284E5A487E5859AE59198E8BDACE6ADA3E79A84E58F82E88083E4BE9DE68DAEE380823C2F703E3C703E09E8AFB7E59084E58886E5859AE6A0A1E4BA8E3130E69C883230E697A5E588B0E5859AE6A0A1E58A9EE585ACE5AEA4EFBC88E7AB8BE5BEB7E6A5BC373036EFBC8CE794B5E8AF9DEFBC9A3538323930303036EFBC89E9A286E58F96E59FB9E8AEADE8B584E69699E380823C2F703E3C703E09266E6273703B3C2F703E3C703E09E99984E4BBB631EFBC9A32303230E5B9B4E4B88BE58D8AE5B9B4E9A284E5A487E5859AE59198E59FB9E8AEADE78FADE5ADA6E4B9A0E5AE89E68E92E8A1A83C2F703E3C703E09E99984E4BBB632EFBC9AE6B996E58D97E7A791E68A80E5A4A7E5ADA6E9A284E5A487E5859AE59198E59FB9E8AEADE78FADE5ADA6E59198E5AE88E588993C2F703E3C703E09266E6273703B3C2F703E3C703E09E4B8ADE585B1E6B996E58D97E7A791E68A80E5A4A7E5ADA6E5A794E59198E4BC9AE5859AE6A0A13C2F703E3C703E0932303230E5B9B43130E69C883139E697A53C2F703E3C703E09266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B266E6273703B3C2F703E3C703E093C7374726F6E673EE99984E4BBB631EFBC9A32303230E5B9B4E4B88BE58D8AE5B9B4E9A284E5A487E5859AE59198E59FB9E8AEADE78FADE5ADA6E4B9A0E5AE89E68E92E8A1A83C2F7374726F6E673E3C2F703E3C703E3C62723E3C2F703E3C703EE4B880E38081E5ADA6E4B9A0E5BCBAE59BBDEFBC9AE796ABE68385E998B2E68EA7E4B8ADE79A84E4B8ADE59BBDE588B6E5BAA6E4BC98E58ABFEFBC88E4B88AEFBC89EFBC88E4B8ADEFBC89EFBC88E4B88BEFBC893C2F703E3C703EE4BA8CE38081E4BC98E7A780E5859AE59198E4BA8BE8BFB9EFBC9A3C2F703E3C703EEFBC8831EFBC89E9BB84E69687E7A780EFBC9AE2809CE4B88DE88EB7E585A8E8839C20E7BB9DE4B88DE694B6E585B5E2809D3C2F703E3C703EEFBC8832EFBC89E99A8BE88080E8BEBE2DE689B6E8B4ABE2809CE7A1ACE6B189E2809D3C2F703E3C703EE8A786E9A291E4B88BE8A786E9A291E4B88BE8BDBDE7BD91E59D80EFBC9A3C6120687265663D2268747470733A2F2F64616E677869616F2E68797065726461692E636F6D2F222072656C3D226E6F6F70656E6572206E6F726566657272657222207461726765743D225F626C616E6B22207374796C653D22636F6C6F723A207267622835312C203132322C20313833293B206261636B67726F756E642D636F6C6F723A207472616E73706172656E743B223E68747470733A2F2F64616E677869616F2E68797065726461692E636F6D2F3C2F613E3C2F703E, '0', 'admin', '2021-12-18 09:34:52', 'admin', '2022-04-10 21:43:45', '管理员');
INSERT INTO `sys_notice` VALUES (2, '关于举办2020年下半年发展对象培训班的通知', '1', 0x3C703E09E59084E58886E5859AE6A0A1E38081E79BB4E5B19EE5859AE5A794EFBC9A3C2F703E3C703E09E6A0B9E68DAEE5859AE6A0A1E59FB9E8AEADE5B7A5E4BD9CE8AEA1E58892EFBC8CE5AE9AE4BA8E32303230E5B9B43130E69C883138E697A5E887B33131E69C8838E697A5E4B8BEE58A9E32303230E5B9B4E4B88BE58D8AE5B9B4E58F91E5B195E5AFB9E8B1A1E59FB9E8AEADE78FADE38082E78EB0E5B086E69C89E585B3E4BA8BE9A1B9E9809AE79FA5E5A682E4B88BEFBC9A3C2F703E3C703E093C7374726F6E673EE4B880E38081E59FB9E8AEADE79BAEE79A843C2F7374726F6E673E3C2F703E3C703E09E6A0B9E68DAEE3808AE4B8ADE59BBDE585B1E4BAA7E5859AE58F91E5B195E5859AE59198E5B7A5E4BD9CE7BB86E58899E3808BE8A681E6B182EFBC8CE5AFB9E58F91E5B195E5AFB9E8B1A1E8BF9BE8A18CE79FADE69C9FE99B86E4B8ADE59FB9E8AEADEFBC8CE69599E882B2E5BC95E5AFBCE5859AE59198E58F91E5B195E5AFB9E8B1A1E6B7B1E588BBE5ADA6E4B9A0E9A286E4BC9AE4B9A0E8BF91E5B9B3E696B0E697B6E4BBA3E4B8ADE59BBDE789B9E889B2E7A4BEE4BC9AE4B8BBE4B989E6809DE683B3E79A84E7B2BEE7A59EE5AE9EE8B4A8E5928CE4B8B0E5AF8CE58685E6B6B5EFBC8CE6B7B1E585A5E4BA86E8A7A3E5859AE79A84E58E86E58FB2EFBC8CE4B8A5E6A0BCE981B5E5AE88E5859AE7ABA0E4B88EE5859AE7BAAAE5859AE8A784EFBC8CE68F90E58D87E5859AE680A7E8A782E5BFB5E5928CE5859AE680A7E4BFAEE585BBEFBC8CE59D9AE5AE9AE79086E683B3E4BFA1E5BFB5EFBC8CE6A091E789A2E2809CE59B9BE4B8AAE6848FE8AF86E2809DEFBC8CE59D9AE5AE9AE2809CE59B9BE4B8AAE887AAE4BFA1E2809DEFBC8CE69BB4E58AA0E59D9AE5AE9AE887AAE8A789E59CB0E5819AE588B0E2809CE4B8A4E4B8AAE7BBB4E68AA4E2809DEFBC8CE58F91E68CA5E5859AE59198E58F91E5B195E5AFB9E8B1A1E79A84E58588E9948BE6A8A1E88C83E4BD9CE794A8EFBC8CE58AA0E5BCBAE5859AE680A7E994BBE782BCEFBC8CE7A1AEE7AB8BE5859AE59198E6848FE8AF86EFBC8CE58887E5AE9EE68F90E9AB98E58F91E5B195E5AFB9E8B1A1E79A84E695B4E4BD93E8B4A8E9878FE380823C2F703E3C703E093C7374726F6E673EE4BA8CE38081E59FB9E8AEADE5AFB9E8B1A13C2F7374726F6E673E3C2F703E3C703E09E68B9FE59CA8E4BB8AE5B9B4E4B88BE58D8AE5B9B4E58F91E5B195E585A5E5859AE79A84E58F91E5B195E5AFB9E8B1A1E380823C2F703E3C703E093C7374726F6E673EE4B889E38081E59FB9E8AEADE696B9E5BC8F3C2F7374726F6E673E3C2F703E3C703E09E4B8BAE5819AE5A5BDE5B8B8E68081E58C96E796ABE68385E998B2E68EA7E5B7A5E4BD9CEFBC8CE7A1AEE4BF9DE59FB9E8AEADE8B4A8E9878FEFBC8CE69CACE6ACA1E58F91E5B195E5AFB9E8B1A1E59FB9E8AEADE98787E794A8E7BABFE4B88AE5928CE7BABFE4B88BE79BB8E7BB93E59088E79A84E59FB9E8AEADE5BDA2E5BC8FE380823C2F703E3C703E093C7374726F6E673EE59B9BE38081E59FB9E8AEADE5AE89E68E923C2F7374726F6E673E3C2F703E3C703E09E59FB9E8AEADE8AFBEE697B6E680BBE695B0E4B8BA3234E5ADA6E697B6EFBC8CE585B6E4B8ADE5BC80E5ADA6E585B8E7A4BC31E5ADA6E697B6EFBC8CE79086E8AEBAE69599E5ADA63130E5ADA6E697B6EFBC8CE8A782E79C8BE5859AE680A7E69599E882B2E7898732E5ADA6E697B6EFBC8CE58886E7BB84E8AEA8E8AEBA32E5ADA6E697B6EFBC8CE7A4BEE4BC9AE5AE9EE8B7B534E5ADA6E697B6EFBC8CE887AAE5ADA632E5ADA6E697B6EFBC8CE692B0E58699E5ADA6E4B9A0E5BF83E5BE9732E5ADA6E697B6EFBC8CE7BB93E4B89AE88083E8AF9531E5ADA6E697B6E380823C2F703E3C703E09312EE79086E8AEBAE69599E5ADA6EFBC883130E5ADA6E697B6EFBC893C2F703E3C703E09E79086E8AEBAE69599E5ADA6E8AEBEE4BA94E4B8AAE4B893E9A298EFBC8CE58886E588ABE698AFE68A97E796ABE5A4A7E88083E5BDB0E698BEE4B8ADE59BBDE789B9E889B2E7A4BEE4BC9AE4B8BBE4B989E588B6E5BAA6E79A84E698BEE89197E4BC98E58ABFE28094E28094E6B7B1E585A5E5ADA6E4B9A0E5859AE79A84E58D81E4B99DE5B18AE59B9BE4B8ADE585A8E4BC9AE7B2BEE7A59EE38081E4B9A0E8BF91E5B9B3E696B0E697B6E4BBA3E4B8ADE59BBDE789B9E889B2E7A4BEE4BC9AE4B8BBE4B989E6809DE683B3E79A84E7A791E5ADA6E4BD93E7B3BBE38081E6A091E7AB8BE5859AE7ABA0E6848FE8AF86266E6273703BE5819AE696B0E697B6E4BBA3E59088E6A0BCE5859AE59198E38081E6B7B1E585A5E5ADA6E4B9A0E2809CE58786E58899E2809DE5928CE2809CE69DA1E4BE8BE2809D20E58887E5AE9EE981B5E5AE88E5859AE79A84E8A784E7ABA0E5928CE7BAAAE5BE8BE38081E7AB8BE5BEB7E6A091E4BABA266E6273703BE5BCBAE58C96E4BBB7E580BCE5BC95E9A286E28094E5ADA6E4B9A0E4B9A0E8BF91E5B9B3E585B3E4BA8EE69599E882B2E79A84E9878DE8A681E8AEBAE8BFB0E380823C2F703E3C703E09322EE8A782E79C8BE5859AE680A7E69599E882B2E78987EFBC8832E5ADA6E697B6EFBC893C2F703E3C703E09E8A782E79C8BE7BAA2E889B2E69599E882B2E78987E38081E696B0E586A0E882BAE7828EE796ABE68385E998B2E68EA7E58588E8BF9BE4BA8BE8BFB9E7BAAAE5BD95E78987E7AD89EFBC8CE794B1E5ADA6E999A2E887AAE8A18CE5AE89E68E92E697B6E997B4EFBC8CE9809AE8BF87E6B996E58D97E7A791E68A80E5A4A7E5ADA6E5859AE6A0A1E59FB9E8AEADE7AEA1E79086E7B3BBE7BB9FEFBC883C6120687265663D2268747470733A2F2F64616E677869616F2E686E7573742E6564752E636E2F222072656C3D226E6F6F70656E6572206E6F726566657272657222207461726765743D225F626C616E6B22207374796C653D22636F6C6F723A207267622835312C203132322C20313833293B206261636B67726F756E642D636F6C6F723A207472616E73706172656E743B223E3C753E68747470733A2F2F64616E677869616F2E686E7573742E6564752E636E2F3C2F753E3C2F613EEFBC89E8BF9BE8A18CE8A782E79C8BE380823C2F703E3C703E09332EE58886E7BB84E8AEA8E8AEBAEFBC8832E5ADA6E697B6EFBC893C2F703E3C703E09E7BB84E7BB87E5ADA6E59198E59BB4E7BB95E2809CE5ADA6E5859AE7ABA0E5859AE8A784EFBC8CE5ADA6E4B9A0E8BF91E5B9B3E696B0E697B6E4BBA3E4B8ADE59BBDE789B9E889B2E7A4BEE4BC9AE4B8BBE4B989E6809DE683B3EFBC8CE4BA89E5819AE59088E6A0BCE5859AE59198E2809DE38081E2809CE5BF97E5AD98E9AB98E8BF9CEFBC8CE8849AE8B88FE5AE9EE59CB0EFBC8CE58B87E5819AE697B6E4BBA3E79A84E5BC84E6BDAEE584BFE2809DE7AD89E4B8BBE9A298E58886E7BB84E8AEA8E8AEBAEFBC8CE5B8AEE58AA9E5ADA6E59198E58AA0E6B7B1E5AFB9E5859AE79A84E59FBAE69CACE79FA5E8AF86E79A84E79086E8A7A3EFBC8CE4B88DE696ADE68F90E9AB98E8AEA4E8AF86EFBC8CE4BA92E79BB8E5B8AEE58AA9EFBC8CE585B1E5908CE8BF9BE6ADA5E38082E8A681E6B182E6AF8FE4BD8DE5ADA6E59198E59CA8E5B08FE7BB84E8AEA8E8AEBAE58F91E8A880E697B6EFBC8CE58786E5A487E58F91E8A880E68F90E7BAB2EFBC8CE5B9B6E79599E5AD98E5A487E6A188E380823C2F703E3C703E09342EE7A4BEE4BC9AE5AE9EE8B7B5EFBC8834E5ADA6E697B6EFBC893C2F703E3C703E09E7BB84E7BB87E5ADA6E59198E9809AE8BF87E58886E78FADEFBC88E7BB84EFBC89E99B86E4B8ADE5BC80E5B195E7A4BEE4BC9AE8B083E7A094E38081E585ACE79B8AE6B4BBE58AA8E68896E5BF97E684BFE88085E69C8DE58AA1E7AD89E4B8BBE9A298E7A4BEE4BC9AE5AE9EE8B7B5E6B4BBE58AA8EFBC8CE58887E5AE9EE8B7B5E8A18CE7A4BEE4BC9AE4B8BBE4B989E6A0B8E5BF83E4BBB7E580BCE8A782EFBC8CE68F90E58D87E5ADA6E59198E98082E5BA94E7A4BEE4BC9AEFBC8CE69C8DE58AA1E7A4BEE4BC9AE79A84E883BDE58A9BE380823C2F703E3C703E09352EE887AAE5ADA6E58F8AE692B0E58699E5ADA6E4B9A0E5BF83E5BE97E4BD93E4BC9AEFBC8834E5ADA6E697B6EFBC893C2F703E3C703E09E8A681E6B182E5ADA6E59198E9878DE782B9E5ADA6E4B9A0E5859AE79A84E58D81E4B99DE5A4A7E7B2BEE7A59EE5928CE4B9A0E8BF91E5B9B3E696B0E697B6E4BBA3E4B8ADE59BBDE789B9E889B2E7A4BEE4BC9AE4B8BBE4B989E6809DE683B3E38081E3808AE4B8ADE59BBDE585B1E4BAA7E5859AE7ABA0E7A88BE3808BE38081E3808AE585B3E4BA8EE696B0E5BDA2E58ABFE4B88BE5859AE58685E694BFE6B2BBE7949FE6B4BBE79A84E88BA5E5B9B2E58786E58899E3808BE38081E3808AE4B8ADE59BBDE585B1E4BAA7E5859AE5BB89E6B481E887AAE5BE8BE58786E58899E3808BE38081E58D81E4B99DE5B18AE59B9BE4B8ADE585A8E4BC9AE7B2BEE7A59EE7AD89EFBC8CE5B9BFE6B39BE5BC80E5B195E4B8AAE4BABAE887AAE5ADA6EFBC8CE5819AE5A5BDE8AFBBE4B9A6E7AC94E8AEB0EFBC8CE5B9B6E7BB93E59088E69599E5ADA6E58685E5AEB9EFBC8CE59CA8E59FB9E8AEADE7BB93E69D9FE5898DE692B0E58699E4B880E7AF8732303030E5AD97E4BBA5E4B88AE79A84E5ADA6E4B9A0E5BF83E5BE97EFBC8CE4B88DE58581E8AEB8E68A84E8A2ADEFBC8CE5ADA6E59198E9809AE8BF87E8B088E8AEA4E8AF86E38081E8AEB2E694B6E88EB7E38081E689BEE5B7AEE8B79DEFBC8CE8BF9BE4B880E6ADA5E7ABAFE6ADA3E585A5E5859AE58AA8E69CBAEFBC8CE6988EE7A1AEE58AAAE58A9BE696B9E59091EFBC8CE4BA89E58F96E697A9E697A5E58AA0E585A5E4B8ADE59BBDE585B1E4BAA7E5859AE38082E68EA8E88D90E5ADA6E4B9A0E5B9B3E58FB0EFBC9A3C2F703E, '0', 'admin', '2021-12-18 09:34:52', 'admin', '2022-04-10 21:44:10', '管理员');
INSERT INTO `sys_notice` VALUES (10, '关于做好2020年下半年预备党员培训报名工作的通知', '1', 0x3C703E3C6120687265663D2268747470733A2F2F64616E677869616F2E686E7573742E636E2F696E6465782E7068703F733D2F696E6465782F6E6F746963652F6169642F3238222072656C3D226E6F6F70656E6572206E6F726566657272657222207461726765743D225F626C616E6B22207374796C653D22636F6C6F723A20726762283130322C203130322C20313032293B223EE585B3E4BA8EE5819AE5A5BD32303230E5B9B4E4B88BE58D8AE5B9B4E9A284E5A487E5859AE59198E59FB9E8AEADE68AA5E5908DE5B7A5E4BD9CE79A84E9809AE79FA53C2F613E3C2F703E3C703E3C62723E3C2F703E, '0', 'admin', '2022-04-10 21:46:01', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1121 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2021-12-18 09:34:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2021-12-18 09:34:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2021-12-18 09:34:44', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2021-12-18 09:34:44', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2021-12-18 09:34:45', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '分党校管理员', 'samll_dept', 2, '4', 1, 1, '0', '0', 'admin', '2021-12-18 09:34:45', 'admin', '2022-05-23 17:27:24', '分党校管理员');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 2000);
INSERT INTO `sys_role_menu` VALUES (2, 2001);
INSERT INTO `sys_role_menu` VALUES (2, 2002);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2006);
INSERT INTO `sys_role_menu` VALUES (2, 2007);
INSERT INTO `sys_role_menu` VALUES (2, 2008);
INSERT INTO `sys_role_menu` VALUES (2, 2009);
INSERT INTO `sys_role_menu` VALUES (2, 2010);
INSERT INTO `sys_role_menu` VALUES (2, 2011);
INSERT INTO `sys_role_menu` VALUES (2, 2013);
INSERT INTO `sys_role_menu` VALUES (2, 2015);
INSERT INTO `sys_role_menu` VALUES (2, 2016);
INSERT INTO `sys_role_menu` VALUES (2, 2027);
INSERT INTO `sys_role_menu` VALUES (2, 2028);
INSERT INTO `sys_role_menu` VALUES (2, 2029);
INSERT INTO `sys_role_menu` VALUES (2, 2030);
INSERT INTO `sys_role_menu` VALUES (2, 2031);
INSERT INTO `sys_role_menu` VALUES (2, 2032);
INSERT INTO `sys_role_menu` VALUES (2, 2039);
INSERT INTO `sys_role_menu` VALUES (2, 2040);
INSERT INTO `sys_role_menu` VALUES (2, 2042);
INSERT INTO `sys_role_menu` VALUES (2, 2044);
INSERT INTO `sys_role_menu` VALUES (2, 2051);
INSERT INTO `sys_role_menu` VALUES (2, 2052);
INSERT INTO `sys_role_menu` VALUES (2, 2053);
INSERT INTO `sys_role_menu` VALUES (2, 2054);
INSERT INTO `sys_role_menu` VALUES (2, 2055);
INSERT INTO `sys_role_menu` VALUES (2, 2056);
INSERT INTO `sys_role_menu` VALUES (2, 2057);
INSERT INTO `sys_role_menu` VALUES (2, 2058);
INSERT INTO `sys_role_menu` VALUES (2, 2059);
INSERT INTO `sys_role_menu` VALUES (2, 2060);
INSERT INTO `sys_role_menu` VALUES (2, 2061);
INSERT INTO `sys_role_menu` VALUES (2, 2062);
INSERT INTO `sys_role_menu` VALUES (2, 2063);
INSERT INTO `sys_role_menu` VALUES (2, 2064);
INSERT INTO `sys_role_menu` VALUES (2, 2065);
INSERT INTO `sys_role_menu` VALUES (2, 2066);
INSERT INTO `sys_role_menu` VALUES (2, 2067);
INSERT INTO `sys_role_menu` VALUES (2, 2068);
INSERT INTO `sys_role_menu` VALUES (2, 2069);
INSERT INTO `sys_role_menu` VALUES (2, 2070);
INSERT INTO `sys_role_menu` VALUES (2, 2071);
INSERT INTO `sys_role_menu` VALUES (2, 2072);
INSERT INTO `sys_role_menu` VALUES (2, 2073);
INSERT INTO `sys_role_menu` VALUES (2, 2074);
INSERT INTO `sys_role_menu` VALUES (2, 2075);
INSERT INTO `sys_role_menu` VALUES (2, 2076);
INSERT INTO `sys_role_menu` VALUES (2, 2077);
INSERT INTO `sys_role_menu` VALUES (2, 2078);
INSERT INTO `sys_role_menu` VALUES (2, 2079);
INSERT INTO `sys_role_menu` VALUES (2, 2080);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 100, 'admin', '明明', '00', 'ming@163.com', '15888888888', '0', 'https://file.mingyuefusu.top/statics/2022/04/19/03459c3a-fe38-46ce-8ee8-7d540a2bd643.jpeg', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-12-18 09:34:44', 'admin', '2021-12-18 09:34:44', '', '2022-04-19 22:29:48', '管理员');
INSERT INTO `sys_user` VALUES (2, 100, 'system', '系统管理员', '00', 'system@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-12-18 09:34:44', 'admin', '2021-12-18 09:34:44', 'admin', '2022-05-10 15:36:46', '测试员');
INSERT INTO `sys_user` VALUES (100, 100, 'test', 'test', '00', '', '', '0', '', '$2a$10$zk7S3GI4LzkwxZfOV.S4g.K6U4XI1rXLMiZBun5jDO3uoW.fufRUy', '0', '0', '', NULL, 'admin', '2022-02-01 23:34:20', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (101, 201, 'tumu', '土木管理员', '00', '', '', '0', '', '$2a$10$Mr6zGokpwg4D6jSEwsBkneXlGVXVODDkTJ/HRrlnAZk/hQ6Bj7Rze', '0', '0', '', NULL, 'admin', '2022-02-02 23:41:13', 'admin', '2022-02-02 23:42:24', NULL);
INSERT INTO `sys_user` VALUES (102, 200, 'jisuanji', '计算机管理员', '00', '', '', '0', '', '$2a$10$QQkWLdccHhpSAd9bQ.soQ.bVhS0IINgcGkeHRPX.GbERWz.F/YAmO', '0', '0', '', NULL, 'admin', '2022-02-02 23:42:38', 'admin', '2022-02-15 11:34:30', NULL);
INSERT INTO `sys_user` VALUES (104, 200, '1805010219', '明月', '01', '', '', '0', 'https://file.mingyuefusu.top/statics/2022/04/23/a1edce26-8ac3-42c8-aa5e-cf64a69458fd.jpeg', '$2a$10$xt7MJUzK0wnAM.xz03PeSOd6lmUlSf/w1YAWXmypFSWj6/Z.mZM1m', '0', '0', '', NULL, '', '2022-02-21 18:30:37', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (101, 2);
INSERT INTO `sys_user_role` VALUES (102, 2);

SET FOREIGN_KEY_CHECKS = 1;
