SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2493 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (1, 'application-dev.yml', 'DEFAULT_GROUP', 'spring:\n  main:\n    allow-circular-references: true\n    allow-bean-definition-overriding: true\n  autoconfigure:\n    exclude: com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure\n  mvc:\n    pathmatch:\n      matching-strategy: ant_path_matcher\n  cloud:\n    sentinel:\n      filter:\n        # sentinel 在 springboot 2.6.x 不兼容问题的处理\n        enabled: false\n\n# feign 配置\nfeign:\n  sentinel:\n    enabled: true\n  okhttp:\n    enabled: true\n  httpclient:\n    enabled: false\n  client:\n    config:\n      default:\n        connectTimeout: 10000\n        readTimeout: 10000\n  compression:\n    request:\n      enabled: true\n    response:\n      enabled: true\n\ndubbo:\n  application:\n    logger: slf4j\n  protocol:\n    # 使用dubbo协议通信\n    name: dubbo\n    # dubbo 协议端口(-1表示自增端口,从20880开始)\n    port: -1\n  # 注册中心配置\n  registry:\n    address: nacos://127.0.0.1:8849\n    group: DUBBO_GROUP\n    # URL采用精简模式(与元数据中心配合)\n    simplified: true\n  # 消费者相关配置\n  consumer:\n    # 结果缓存(LRU算法)\n    cache: true\n    # 支持校验注解\n    validation: true\n    # 超时时间\n    timeout: 3000\n    # 初始化检查\n    check: false\n  scan:\n    # 接口实现类扫描\n    base-packages: com.psedu.**.dubbo\n  # 自定义配置\n  custom:\n    # 全局请求log\n    request-log: true\n    # info 基础信息 param 参数信息 full 全部\n    log-level: info\n\n# 暴露监控端点\nmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: \'*\'\n', '50086501c6d11dab469b4cd55e8bdf45', '2020-05-20 12:00:00', '2022-05-07 01:57:42', 'nacos', '111.8.73.131', '', '', '通用配置', 'null', 'null', 'yaml', 'null');
INSERT INTO `config_info` VALUES (10, 'psedu-base-dev.yml', 'DEFAULT_GROUP', '# dubbo 订阅配置\ndubbo:\n  cloud:\n    # 需要远程调用的服务 多个用逗号分割\n    subscribed-services: psedu-system\n# spring配置\nspring: \n  devtools:\n    restart:\n      enabled: true\n  freemarker:\n    cache: false\n\n  redis:\n    host: dangxiao-redis.dangxiao\n    port: 6379\n    password: 123456\n  datasource:\n    druid:\n      stat-view-servlet:\n        enabled: true\n        loginUsername: admin\n        loginPassword: 123456\n    dynamic:\n      druid:\n        initial-size: 5\n        min-idle: 5\n        maxActive: 20\n        maxWait: 60000\n        timeBetweenEvictionRunsMillis: 60000\n        minEvictableIdleTimeMillis: 300000\n        validationQuery: SELECT 1 FROM DUAL\n        testWhileIdle: true\n        testOnBorrow: false\n        testOnReturn: false\n        poolPreparedStatements: true\n        maxPoolPreparedStatementPerConnectionSize: 20\n        filters: stat,slf4j\n        connectionProperties: druid.stat.mergeSql\\=true;druid.stat.slowSqlMillis\\=5000\n      datasource:\n          # 主库数据源\n          master:\n            driver-class-name: com.mysql.cj.jdbc.Driver\n            url: jdbc:mysql://dangxiao-mysql.dangxiao:3306/psedu_base?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true\n            username: root\n            password: 123456\n          # 从库数据源\n          # slave:\n            # username: \n            # password: \n            # url: \n            # driver-class-name: \n      # seata: true    # 开启seata代理，开启后默认每个数据源都代理，如果某个不需要代理可单独关闭\n\n# seata配置\nseata:\n  # 默认关闭，如需启用spring.datasource.dynami.seata需要同时开启\n  enabled: false\n  # Seata 应用编号，默认为 ${spring.application.name}\n  application-id: ${spring.application.name}\n  # Seata 事务组编号，用于 TC 集群名\n  tx-service-group: ${spring.application.name}-group\n  # 关闭自动代理\n  enable-auto-data-source-proxy: false\n  # 服务配置项\n  service:\n    # 虚拟组和分组的映射\n    vgroup-mapping:\n      ruoyi-system-group: default\n  config:\n    type: nacos\n    nacos:\n      serverAddr: 127.0.0.1:8848\n      group: SEATA_GROUP\n      namespace:\n  registry:\n    type: nacos\n    nacos:\n      application: seata-server\n      server-addr: 127.0.0.1:8848\n      namespace:\n\n# mybatis配置\nmybatis-plus:\n  # 搜索指定包别名\n  typeAliasesPackage: com.psedu.base.domain\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\n  mapperLocations: classpath:mapper/**/*.xml\n  configuration:\n    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl\n  #map-underscore-to-camel-case: true\n# swagger配置\nswagger:\n  title: 党校基础模块接口文档\n  license: 明月\n  licenseUrl: http://www.mingyuefusu.top', 'dddd8e8e7d00157b2b080c9fbf8fc7f8', '2022-01-31 15:42:46', '2022-06-30 14:42:44', 'nacos', '192.168.1.3', '', '', '', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (30, 'ruoyi-system', 'mapping-com.ruoyi.system.api.RemoteDataScopeService', '1650720702370', '4c36b20f4b161737b44353e37b83a6b9', '2022-02-03 05:54:30', '2022-04-23 08:31:42', NULL, '172.25.50.35', '', 'DUBBO_GROUP', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (31, 'com.ruoyi.system.api.RemoteDataScopeService:::provider:ruoyi-system', 'DUBBO_GROUP', '{\"parameters\":{\"side\":\"provider\",\"release\":\"2.7.8\",\"methods\":\"getDeptAndChild,getRoleCustom\",\"logger\":\"slf4j\",\"deprecated\":\"false\",\"dubbo\":\"2.0.2\",\"interface\":\"com.ruoyi.system.api.RemoteDataScopeService\",\"qos.enable\":\"false\",\"generic\":\"false\",\"metadata-type\":\"remote\",\"application\":\"ruoyi-system\",\"dynamic\":\"true\",\"anyhost\":\"true\"},\"canonicalName\":\"com.ruoyi.system.api.RemoteDataScopeService\",\"codeSource\":\"jar:file:/opt/project/ruoyi-modules-system.jar!/BOOT-INF/lib/ruoyi-api-system-3.4.0.jar!/\",\"methods\":[{\"name\":\"getRoleCustom\",\"parameterTypes\":[\"java.lang.Long\"],\"returnType\":\"java.lang.String\"},{\"name\":\"getDeptAndChild\",\"parameterTypes\":[\"java.lang.Long\"],\"returnType\":\"java.lang.String\"}],\"types\":[{\"type\":\"java.lang.String\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"char\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"int\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"long\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"java.lang.Long\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"}]}', '1565d607717f67d6975ce1be1d6c7dfd', '2022-02-03 05:54:31', '2022-05-06 13:27:17', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (32, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:ruoyi-system:provider:ruoyi-system', 'DUBBO_GROUP', '{\"parameters\":{\"side\":\"provider\",\"release\":\"2.7.8\",\"methods\":\"getAllServiceKeys,getServiceRestMetadata,getExportedURLs,getAllExportedURLs\",\"logger\":\"slf4j\",\"deprecated\":\"false\",\"dubbo\":\"2.0.2\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"generic\":\"false\",\"revision\":\"2021.1\",\"metadata-type\":\"remote\",\"application\":\"ruoyi-system\",\"dynamic\":\"true\",\"group\":\"ruoyi-system\",\"anyhost\":\"true\"},\"canonicalName\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"codeSource\":\"jar:file:/opt/project/ruoyi-modules-system.jar!/BOOT-INF/lib/spring-cloud-starter-dubbo-2021.1.jar!/\",\"methods\":[{\"name\":\"getAllServiceKeys\",\"parameterTypes\":[],\"returnType\":\"java.util.Set\\u003cjava.lang.String\\u003e\"},{\"name\":\"getExportedURLs\",\"parameterTypes\":[\"java.lang.String\",\"java.lang.String\",\"java.lang.String\"],\"returnType\":\"java.lang.String\"},{\"name\":\"getServiceRestMetadata\",\"parameterTypes\":[],\"returnType\":\"java.lang.String\"},{\"name\":\"getAllExportedURLs\",\"parameterTypes\":[],\"returnType\":\"java.util.Map\\u003cjava.lang.String,java.lang.String\\u003e\"}],\"types\":[{\"type\":\"java.lang.String\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"char\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"int\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"}]}', '17c0813dc5b14b2ff393126badc83c34', '2022-02-03 05:54:33', '2022-05-06 13:27:18', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (33, 'ruoyi-system', 'mapping-com.alibaba.cloud.dubbo.service.DubboMetadataService', '1650720703361', 'd68e8085174f46c8c31a28062d1fdb69', '2022-02-03 05:54:33', '2022-04-23 08:31:43', NULL, '172.25.50.35', '', 'DUBBO_GROUP', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (34, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:ruoyi-system:consumer:psedu-base', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"psedu-base\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"ruoyi-system\"}', '279fc83535047ac07959139788653270', '2022-02-03 05:55:31', '2022-05-07 02:07:27', NULL, '10.152.49.6', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (35, 'com.ruoyi.system.api.RemoteDataScopeService:::consumer:psedu-base', 'DUBBO_GROUP', '{\"init\":\"false\",\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"methods\":\"getDeptAndChild,getRoleCustom\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.ruoyi.system.api.RemoteDataScopeService\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"metadata-type\":\"remote\",\"application\":\"psedu-base\",\"sticky\":\"false\",\"validation\":\"true\"}', '0b0b8ca35e71f225c92282f40ca91e16', '2022-02-03 05:55:31', '2022-05-06 16:10:11', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (36, 'psedu-base', 'mapping-com.alibaba.cloud.dubbo.service.DubboMetadataService', '1655477315486', '94288bdcc12c904b48d63c06cfda5cd3', '2022-02-03 05:55:37', '2022-06-17 09:48:38', NULL, '192.168.1.13', '', 'DUBBO_GROUP', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (37, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-base:provider:psedu-base', 'DUBBO_GROUP', '{\"parameters\":{\"side\":\"provider\",\"release\":\"2.7.8\",\"methods\":\"getAllServiceKeys,getServiceRestMetadata,getExportedURLs,getAllExportedURLs\",\"logger\":\"slf4j\",\"deprecated\":\"false\",\"dubbo\":\"2.0.2\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"generic\":\"false\",\"revision\":\"2021.1\",\"metadata-type\":\"remote\",\"application\":\"psedu-base\",\"dynamic\":\"true\",\"group\":\"psedu-base\",\"anyhost\":\"true\"},\"canonicalName\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"codeSource\":\"spring-cloud-starter-dubbo-2021.1.jar\",\"methods\":[{\"name\":\"getExportedURLs\",\"parameterTypes\":[\"java.lang.String\",\"java.lang.String\",\"java.lang.String\"],\"returnType\":\"java.lang.String\"},{\"name\":\"getAllExportedURLs\",\"parameterTypes\":[],\"returnType\":\"java.util.Map\\u003cjava.lang.String,java.lang.String\\u003e\"},{\"name\":\"getServiceRestMetadata\",\"parameterTypes\":[],\"returnType\":\"java.lang.String\"},{\"name\":\"getAllServiceKeys\",\"parameterTypes\":[],\"returnType\":\"java.util.Set\\u003cjava.lang.String\\u003e\"}],\"types\":[{\"type\":\"int\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"char\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"java.lang.String\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"}]}', '94f753a21e11939b699694c35215856e', '2022-02-03 05:55:37', '2022-06-17 09:48:38', NULL, '192.168.1.13', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (38, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-base:consumer:ruoyi-system', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"ruoyi-system\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"psedu-base\"}', 'de74e98a9a19e6e4813900ee86e5f841', '2022-02-03 05:55:54', '2022-05-06 13:27:18', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1206, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:ruoyi-system:consumer:psedu-exam', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"psedu-exam\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"ruoyi-system\"}', '92c8aca2dc132d9cca6a8bd7cdb18f20', '2022-03-28 21:14:45', '2022-03-28 21:19:30', NULL, '192.168.153.1', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1207, 'com.ruoyi.system.api.RemoteDataScopeService:::consumer:psedu-exam', 'DUBBO_GROUP', '{\"init\":\"false\",\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"methods\":\"getDeptAndChild,getRoleCustom\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.ruoyi.system.api.RemoteDataScopeService\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"metadata-type\":\"remote\",\"application\":\"psedu-exam\",\"sticky\":\"false\",\"validation\":\"true\"}', '36c99caf65d46da3eaf0760c9aa31d9c', '2022-03-28 21:14:45', '2022-03-28 21:19:30', NULL, '192.168.153.1', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1208, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-exam:provider:psedu-exam', 'DUBBO_GROUP', '{\"parameters\":{\"side\":\"provider\",\"release\":\"2.7.8\",\"methods\":\"getAllServiceKeys,getServiceRestMetadata,getExportedURLs,getAllExportedURLs\",\"logger\":\"slf4j\",\"deprecated\":\"false\",\"dubbo\":\"2.0.2\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"generic\":\"false\",\"revision\":\"2021.1\",\"metadata-type\":\"remote\",\"application\":\"psedu-exam\",\"dynamic\":\"true\",\"group\":\"psedu-exam\",\"anyhost\":\"true\"},\"canonicalName\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"codeSource\":\"spring-cloud-starter-dubbo-2021.1.jar\",\"methods\":[{\"name\":\"getServiceRestMetadata\",\"parameterTypes\":[],\"returnType\":\"java.lang.String\"},{\"name\":\"getAllExportedURLs\",\"parameterTypes\":[],\"returnType\":\"java.util.Map\\u003cjava.lang.String,java.lang.String\\u003e\"},{\"name\":\"getExportedURLs\",\"parameterTypes\":[\"java.lang.String\",\"java.lang.String\",\"java.lang.String\"],\"returnType\":\"java.lang.String\"},{\"name\":\"getAllServiceKeys\",\"parameterTypes\":[],\"returnType\":\"java.util.Set\\u003cjava.lang.String\\u003e\"}],\"types\":[{\"type\":\"int\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"java.lang.String\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"byte\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"}]}', 'fafd55ce43be93815b6cc0f15fa02893', '2022-03-28 21:14:52', '2022-03-28 21:19:35', NULL, '192.168.153.1', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1209, 'psedu-exam', 'mapping-com.alibaba.cloud.dubbo.service.DubboMetadataService', '1648520375146', 'b517bf41e512550e8f4cabf9d5494406', '2022-03-28 21:14:52', '2022-03-28 21:19:35', NULL, '192.168.153.1', '', 'DUBBO_GROUP', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1210, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-exam:consumer:ruoyi-system', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"ruoyi-system\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"psedu-exam\"}', 'b92d9a4d5bfcb007ea769c5c2c78dbec', '2022-03-28 21:15:18', '2022-04-01 14:12:58', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1211, 'psedu-exam-dev.yml', 'DEFAULT_GROUP', '# dubbo 订阅配置\ndubbo:\n  cloud:\n    # 需要远程调用的服务 多个用逗号分割\n    subscribed-services: psedu-system\n# spring配置\nspring: \n  devtools:\n    restart:\n      enabled: true\n  freemarker:\n    cache: false\n\n  redis:\n    host: dangxiao-redis.dangxiao\n    port: 6379\n    password: 123456\n  datasource:\n    druid:\n      stat-view-servlet:\n        enabled: true\n        loginUsername: admin\n        loginPassword: 123456\n    dynamic:\n      druid:\n        initial-size: 5\n        min-idle: 5\n        maxActive: 20\n        maxWait: 60000\n        timeBetweenEvictionRunsMillis: 60000\n        minEvictableIdleTimeMillis: 300000\n        validationQuery: SELECT 1 FROM DUAL\n        testWhileIdle: true\n        testOnBorrow: false\n        testOnReturn: false\n        poolPreparedStatements: true\n        maxPoolPreparedStatementPerConnectionSize: 20\n        filters: stat,slf4j\n        connectionProperties: druid.stat.mergeSql\\=true;druid.stat.slowSqlMillis\\=5000\n      datasource:\n          # 主库数据源\n          master:\n            driver-class-name: com.mysql.cj.jdbc.Driver\n            url: jdbc:mysql://dangxiao-mysql.dangxiao:3306/psedu_exam?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true\n            username: root\n            password: 123456\n          # 从库数据源\n          # slave:\n            # username: \n            # password: \n            # url: \n            # driver-class-name: \n      # seata: true    # 开启seata代理，开启后默认每个数据源都代理，如果某个不需要代理可单独关闭\n\n# seata配置\nseata:\n  # 默认关闭，如需启用spring.datasource.dynami.seata需要同时开启\n  enabled: false\n  # Seata 应用编号，默认为 ${spring.application.name}\n  application-id: ${spring.application.name}\n  # Seata 事务组编号，用于 TC 集群名\n  tx-service-group: ${spring.application.name}-group\n  # 关闭自动代理\n  enable-auto-data-source-proxy: false\n  # 服务配置项\n  service:\n    # 虚拟组和分组的映射\n    vgroup-mapping:\n      ruoyi-system-group: default\n  config:\n    type: nacos\n    nacos:\n      serverAddr: 127.0.0.1:8848\n      group: SEATA_GROUP\n      namespace:\n  registry:\n    type: nacos\n    nacos:\n      application: seata-server\n      server-addr: 127.0.0.1:8848\n      namespace:\n\n# mybatis配置\nmybatis-plus:\n  # 搜索指定包别名\n  typeAliasesPackage: com.psedu.exam.domain\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\n  mapperLocations: classpath:mapper/**/*.xml\n  configuration:\n    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl\n  #map-underscore-to-camel-case: true\n# swagger配置\nswagger:\n  title: 考试模块接口文档\n  license: 明月\n  licenseUrl: http://mingyuefusu.top', '90e4724a93720bd5dec83ed5f80339c1', '2022-03-28 21:17:09', '2022-07-01 02:12:24', 'nacos', '192.168.1.3', '', '', '', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (1909, 'psedu-gateway-dev.yml', 'DEFAULT_GROUP', 'spring:\n  redis:\n    host: 127.0.0.1\n    port: 6379\n    password: 123456\n  cloud:\n    gateway:\n      discovery:\n        locator:\n          lowerCaseServiceId: true\n          enabled: true\n      routes:\n        - id: psedu-base\n          uri: lb://psedu-base\n          predicates:\n            - Path=/psedu-base/**\n          filters:\n            - StripPrefix=1\n        - id: psedu-exam\n          uri: lb://psedu-exam\n          predicates:\n            - Path=/psedu-exam/**\n          filters:\n            - StripPrefix=1\n        # 认证中心\n        - id: psedu-auth\n          uri: lb://psedu-auth\n          predicates:\n            - Path=/auth/**\n          filters:\n            # 验证码处理\n            - CacheRequestFilter\n            - ValidateCodeFilter\n            - StripPrefix=1\n        # 代码生成\n        - id: psedu-gen\n          uri: lb://psedu-gen\n          predicates:\n            - Path=/code/**\n          filters:\n            - StripPrefix=1\n        # 定时任务\n        - id: psedu-job\n          uri: lb://psedu-job\n          predicates:\n            - Path=/schedule/**\n          filters:\n            - StripPrefix=1\n        # 系统模块\n        - id: psedu-system\n          uri: lb://psedu-system\n          predicates:\n            - Path=/system/**\n          filters:\n            - StripPrefix=1\n        # 文件服务\n        - id: psedu-file\n          uri: lb://psedu-file\n          predicates:\n            - Path=/file/**\n          filters:\n            - StripPrefix=1\n        \n\n# 安全配置\nsecurity:\n  # 验证码\n  captcha:\n    enabled: false\n    type: math\n  # 防止XSS攻击\n  xss:\n    enabled: true\n    excludeUrls:\n      - /system/notice\n      - /psedu-exam/question\n  # 不校验白名单\n  ignore:\n    whites:\n      - /auth/user-register\n      - /auth/logout\n      - /auth/login\n      - /auth/register\n      - /system/notice/list\n      - /system/notice/*\n      - /system/dept/psDeptList\n      - /*/v2/api-docs\n      - /csrf\n', '8b139864c6b67853fdb0b30e23504ece', '2022-05-07 01:58:13', '2022-05-07 01:58:50', 'nacos', '111.8.73.131', '', '', '网关模块', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (1910, 'psedu-auth-dev.yml', 'DEFAULT_GROUP', 'spring: \n  redis:\n    host: dangxiao-redis.dangxiao\n    port: 6379\n    password: 123456\n', '25e705547642b397aa46929b36e95132', '2022-05-07 01:58:13', '2022-07-01 02:12:41', 'nacos', '192.168.1.3', '', '', '认证中心', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (1911, 'psedu-monitor-dev.yml', 'DEFAULT_GROUP', '# spring\r\nspring: \r\n  security:\r\n    user:\r\n      name: ruoyi\r\n      password: 123456\r\n  boot:\r\n    admin:\r\n      ui:\r\n        title: 若依服务状态监控\r\n', 'd8997d0707a2fd5d9fc4e8409da38129', '2022-05-07 01:58:13', '2022-05-07 01:58:13', NULL, '111.8.73.131', '', '', '监控中心', NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (1912, 'psedu-system-dev.yml', 'DEFAULT_GROUP', '# spring配置\nspring: \n  # cloud:\n  #   inetutils:\n  #     ignored-interfaces: [\'lo.*\',\'eth.*\',\'docker.*\']\n  #     # 忽略网卡，eth.*，正则表达式\n  #     # 选择符合前缀的IP作为服务注册IP, discovery.server-ip就是你的公网IP\n  #     preferred-networks: 127.0.0.1\n  redis:\n    host: dangxiao-redis.dangxiao\n    port: 6379\n    password: 123456\n  datasource:\n    druid:\n      stat-view-servlet:\n        enabled: true\n        loginUsername: admin\n        loginPassword: 123456\n    dynamic:\n      druid:\n        initial-size: 5\n        min-idle: 5\n        maxActive: 20\n        maxWait: 60000\n        timeBetweenEvictionRunsMillis: 60000\n        minEvictableIdleTimeMillis: 300000\n        validationQuery: SELECT 1 FROM DUAL\n        testWhileIdle: true\n        testOnBorrow: false\n        testOnReturn: false\n        poolPreparedStatements: true\n        maxPoolPreparedStatementPerConnectionSize: 20\n        filters: stat,slf4j\n        connectionProperties: druid.stat.mergeSql\\=true;druid.stat.slowSqlMillis\\=5000\n      datasource:\n          # 主库数据源\n          master:\n            driver-class-name: com.mysql.cj.jdbc.Driver\n            url: jdbc:mysql://dangxiao-mysql.dangxiao:3306/psedu_system?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\n            username: root\n            password: 123456\n          # 从库数据源\n          # slave:\n            # username: \n            # password: \n            # url: \n            # driver-class-name: \n      # seata: true    # 开启seata代理，开启后默认每个数据源都代理，如果某个不需要代理可单独关闭\n\n# seata配置\nseata:\n  # 默认关闭，如需启用spring.datasource.dynami.seata需要同时开启\n  enabled: false\n  # Seata 应用编号，默认为 ${spring.application.name}\n  application-id: ${spring.application.name}\n  # Seata 事务组编号，用于 TC 集群名\n  tx-service-group: ${spring.application.name}-group\n  # 关闭自动代理\n  enable-auto-data-source-proxy: false\n  # 服务配置项\n  service:\n    # 虚拟组和分组的映射\n    vgroup-mapping:\n      psedu-system-group: default\n  config:\n    type: nacos\n    nacos:\n      serverAddr: 127.0.0.1:8848\n      group: SEATA_GROUP\n      namespace:\n  registry:\n    type: nacos\n    nacos:\n      application: seata-server\n      server-addr: 127.0.0.1:8848\n      namespace:\n\n# mybatis配置\nmybatis:\n  # 搜索指定包别名\n  typeAliasesPackage: com.psedu.system\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\n  mapperLocations: classpath:mapper/**/*.xml\n  configuration:\n    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl\n# swagger配置\nswagger:\n  title: 系统模块接口文档\n  license: Powered By psedu\n  licenseUrl: https://psedu.vip', 'f600eb18f41d8dbdc17d7553bcd29cda', '2022-05-07 01:58:13', '2022-07-01 02:13:47', 'nacos', '192.168.1.3', '', '', '系统模块', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (1913, 'psedu-gen-dev.yml', 'DEFAULT_GROUP', '# spring配置\nspring: \n  redis:\n    host: dangxiao-redis.dangxiao\n    port: 6379\n    password: 123456\n  datasource: \n    driver-class-name: com.mysql.cj.jdbc.Driver\n    url: jdbc:mysql://dangxiao-mysql.dangxiao:3306/psedu_base?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\n    username: root\n    password: 123456\n    hikari:\n      minimum-idle: 3\n      maximum-pool-size: 5\n      max-lifetime: 30000\n      connection-test-query: SELECT 1\n\n\n# mybatis配置\nmybatis:\n    # 搜索指定包别名\n    typeAliasesPackage: com.psedu.gen.domain\n    # 配置mapper的扫描，找到所有的mapper.xml映射文件\n    mapperLocations: classpath:mapper/**/*.xml\n\n# swagger配置\nswagger:\n  title: 代码生成接口文档\n  license: Powered By psedu\n  licenseUrl: https://psedu.vip\n\n# 代码生成\ngen: \n  # 作者\n  author: mingyue\n  # 默认生成包路径 system 需改成自己的模块名称 如 system monitor tool\n  packageName: com.psedu.exam\n  # 自动去除表前缀，默认是false\n  autoRemovePre: false\n  # 表前缀（生成类名不会包含表前缀，多个用逗号分隔）\n  tablePrefix: psedu\n', '58669308d51809584c6edbd7aca06887', '2022-05-07 01:58:13', '2022-07-01 02:14:23', 'nacos', '192.168.1.3', '', '', '', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (1914, 'psedu-job-dev.yml', 'DEFAULT_GROUP', '# spring配置\nspring: \n  redis:\n    host: localhost\n    port: 6379\n    password: \n  datasource:\n    driver-class-name: com.mysql.cj.jdbc.Driver\n    url: jdbc:mysql://localhost:3306/ry-cloud?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8\n    username: root\n    password: password\n\n# mybatis配置\nmybatis:\n    # 搜索指定包别名\n    typeAliasesPackage: com.psedu.job.domain\n    # 配置mapper的扫描，找到所有的mapper.xml映射文件\n    mapperLocations: classpath:mapper/**/*.xml\n\n# swagger配置\nswagger:\n  title: 定时任务接口文档\n  license: Powered By psedu\n  licenseUrl: https://psedu.vip\n', '2eb63c9ea37845fed77c48614f6c8261', '2022-05-07 01:58:13', '2022-05-07 02:00:19', 'nacos', '111.8.73.131', '', '', '定时任务', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (1915, 'psedu-file-dev.yml', 'DEFAULT_GROUP', '# 本地文件上传    \nfile:\n  domain: https://file.mingyuefusu.top\n  path: /opt/project/file\n  prefix: /statics\n\n# FastDFS配置\nfdfs:\n  domain: http://8.129.231.12\n  soTimeout: 3000\n  connectTimeout: 2000\n  trackerList: 8.129.231.12:22122\n\n# Minio配置\nminio:\n  url: http://8.129.231.12:9000\n  accessKey: minioadmin\n  secretKey: minioadmin\n  bucketName: test', 'de9b6d7d6e84eacf5b4403d0786b54b4', '2022-05-07 01:58:13', '2022-05-18 03:09:42', 'nacos', '36.148.125.81', '', '', '文件服务', '', '', 'yaml', '');
INSERT INTO `config_info` VALUES (1916, 'sentinel-psedu-gateway', 'DEFAULT_GROUP', '[\n    {\n        \"resource\": \"psedu-auth\",\n        \"count\": 500,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    },\n	{\n        \"resource\": \"psedu-system\",\n        \"count\": 1000,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    },\n	{\n        \"resource\": \"psedu-gen\",\n        \"count\": 200,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    },\n	{\n        \"resource\": \"psedu-job\",\n        \"count\": 300,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    }\n]', 'eaba282d603d62e173c5a124648e65e7', '2022-05-07 01:58:13', '2022-05-07 01:59:39', 'nacos', '111.8.73.131', '', '', '限流策略', '', '', 'json', '');
INSERT INTO `config_info` VALUES (1923, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:ruoyi-system:consumer:psedu-system', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"psedu-system\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"ruoyi-system\"}', '5ecf862f11e515402c57620459c53b9f', '2022-05-07 02:05:54', '2022-05-07 02:05:54', NULL, '10.152.49.6', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1924, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-base:consumer:psedu-system', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"psedu-system\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"psedu-base\"}', '1462de0b5020cb232cf4aeae4aeae49b', '2022-05-07 02:05:54', '2022-06-01 16:07:43', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1925, 'psedu-system', 'mapping-com.psedu.system.api.RemoteDataScopeService', '1653277431436', '9bf6711f65629759d68269781c79ad28', '2022-05-07 02:05:56', '2022-05-22 22:43:51', NULL, '172.25.50.35', '', 'DUBBO_GROUP', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1926, 'com.psedu.system.api.RemoteDataScopeService:::provider:psedu-system', 'DUBBO_GROUP', '{\"parameters\":{\"side\":\"provider\",\"release\":\"2.7.8\",\"methods\":\"getDeptAndChild,getRoleCustom\",\"logger\":\"slf4j\",\"deprecated\":\"false\",\"dubbo\":\"2.0.2\",\"interface\":\"com.psedu.system.api.RemoteDataScopeService\",\"qos.enable\":\"false\",\"generic\":\"false\",\"metadata-type\":\"remote\",\"application\":\"psedu-system\",\"dynamic\":\"true\",\"anyhost\":\"true\"},\"canonicalName\":\"com.psedu.system.api.RemoteDataScopeService\",\"codeSource\":\"jar:file:/opt/project/psedu-modules-system.jar!/BOOT-INF/lib/psedu-api-system-3.4.0.jar!/\",\"methods\":[{\"name\":\"getRoleCustom\",\"parameterTypes\":[\"java.lang.Long\"],\"returnType\":\"java.lang.String\"},{\"name\":\"getDeptAndChild\",\"parameterTypes\":[\"java.lang.Long\"],\"returnType\":\"java.lang.String\"}],\"types\":[{\"type\":\"int\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"char\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"java.lang.String\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"java.lang.Long\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"long\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"}]}', '5a7da81e2ea86a0952b92c0d7d5b3384', '2022-05-07 02:05:56', '2022-06-01 16:07:42', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1927, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-system:provider:psedu-system', 'DUBBO_GROUP', '{\"parameters\":{\"side\":\"provider\",\"release\":\"2.7.8\",\"methods\":\"getAllServiceKeys,getServiceRestMetadata,getExportedURLs,getAllExportedURLs\",\"logger\":\"slf4j\",\"deprecated\":\"false\",\"dubbo\":\"2.0.2\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"generic\":\"false\",\"revision\":\"2021.1\",\"metadata-type\":\"remote\",\"application\":\"psedu-system\",\"dynamic\":\"true\",\"group\":\"psedu-system\",\"anyhost\":\"true\"},\"canonicalName\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"codeSource\":\"jar:file:/opt/project/psedu-modules-system.jar!/BOOT-INF/lib/spring-cloud-starter-dubbo-2021.1.jar!/\",\"methods\":[{\"name\":\"getServiceRestMetadata\",\"parameterTypes\":[],\"returnType\":\"java.lang.String\"},{\"name\":\"getAllExportedURLs\",\"parameterTypes\":[],\"returnType\":\"java.util.Map\\u003cjava.lang.String,java.lang.String\\u003e\"},{\"name\":\"getAllServiceKeys\",\"parameterTypes\":[],\"returnType\":\"java.util.Set\\u003cjava.lang.String\\u003e\"},{\"name\":\"getExportedURLs\",\"parameterTypes\":[\"java.lang.String\",\"java.lang.String\",\"java.lang.String\"],\"returnType\":\"java.lang.String\"}],\"types\":[{\"type\":\"int\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"char\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"},{\"type\":\"java.lang.String\",\"typeBuilderName\":\"org.apache.dubbo.metadata.definition.builder.DefaultTypeBuilder\"}]}', 'a2cc5bf05eb5fbf8b1f28f59a1d7a859', '2022-05-07 02:05:56', '2022-06-01 16:07:41', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1928, 'psedu-system', 'mapping-com.alibaba.cloud.dubbo.service.DubboMetadataService', '1653277432770', '702667ba6e70db132c60aad5384a23f5', '2022-05-07 02:05:56', '2022-05-22 22:43:53', NULL, '172.25.50.35', '', 'DUBBO_GROUP', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1929, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-system:consumer:ruoyi-system', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"ruoyi-system\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"psedu-system\"}', 'f33e49c819b35ea94073ca2ca7622b15', '2022-05-07 02:06:12', '2022-05-07 02:06:12', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1931, 'com.psedu.system.api.RemoteDataScopeService:::consumer:psedu-base', 'DUBBO_GROUP', '{\"init\":\"false\",\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"methods\":\"getDeptAndChild,getRoleCustom\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.psedu.system.api.RemoteDataScopeService\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"metadata-type\":\"remote\",\"application\":\"psedu-base\",\"sticky\":\"false\",\"validation\":\"true\"}', '491e33b626f5479ed22e90f675caaaea', '2022-05-07 02:07:28', '2022-06-17 09:48:33', NULL, '192.168.1.13', '', '', NULL, NULL, NULL, 'text', NULL);
INSERT INTO `config_info` VALUES (1973, 'com.alibaba.cloud.dubbo.service.DubboMetadataService:1.0.0:psedu-system:consumer:psedu-base', 'DUBBO_GROUP', '{\"side\":\"consumer\",\"cache\":\"true\",\"release\":\"2.7.8\",\"logger\":\"slf4j\",\"dubbo\":\"2.0.2\",\"check\":\"false\",\"interface\":\"com.alibaba.cloud.dubbo.service.DubboMetadataService\",\"version\":\"1.0.0\",\"qos.enable\":\"false\",\"timeout\":\"3000\",\"generic\":\"true\",\"metadata-type\":\"remote\",\"application\":\"psedu-base\",\"sticky\":\"false\",\"validation\":\"true\",\"group\":\"psedu-system\"}', '685af684f8e0ec4e5e0bbc96e1bb6720', '2022-05-09 09:04:30', '2022-06-01 16:27:50', NULL, '172.25.50.35', '', '', NULL, NULL, NULL, 'text', NULL);

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime(0) NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of config_info_aggr
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of config_info_beta
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of config_info_tag
-- ----------------------------

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of config_tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of group_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(64) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2492 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resource` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `action` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  UNIQUE INDEX `uk_role_permission`(`role`, `resource`, `action`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  UNIQUE INDEX `idx_user_role`(`username`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tenant_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', 1);

SET FOREIGN_KEY_CHECKS = 1;
