SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'psedu_apply', '培训报名', NULL, NULL, 'Apply', 'crud', 'com.psedu.base', 'psedu-base', 'apply', '培训报名', 'mingyue', '0', '/', '{\"parentMenuId\":\"2002\"}', 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:15', NULL);
INSERT INTO `gen_table` VALUES (3, 'psedu_dept', '分党校', NULL, NULL, 'PseduDept', 'crud', 'com.psedu.base', 'base', 'dept', '分党校', 'mingyue', '0', '/', NULL, 'admin', '2022-01-30 11:07:37', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (4, 'psedu_evaluate', '评教', NULL, NULL, 'PseduEvaluate', 'crud', 'com.psedu.base', 'base', 'evaluate', '评教', 'mingyue', '0', '/', NULL, 'admin', '2022-01-30 11:07:37', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (5, 'psedu_merge_class', '合班', NULL, NULL, 'PseduMergeClass', 'crud', 'com.psedu.base', 'base', 'class', '合班', 'mingyue', '0', '/', NULL, 'admin', '2022-01-30 11:07:38', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (7, 'psedu_semester_dept', '学期分党校', NULL, NULL, 'SemesterDept', 'crud', 'com.psedu.base', 'psedu-base', 'semesterDept', '学期分党校', 'mingyue', '0', '/', '{}', 'admin', '2022-01-30 11:07:38', '', '2022-03-12 10:36:36', NULL);
INSERT INTO `gen_table` VALUES (8, 'psedu_sign_launch', '发起签到', '', '', 'SignLaunch', 'crud', 'com.psedu.base', 'psedu-base', 'signLaunch', '发起签到', 'mingyue', '0', '/', '{\"parentMenuId\":2003}', 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59', NULL);
INSERT INTO `gen_table` VALUES (9, 'psedu_sign_record', '签到记录', NULL, NULL, 'SignRecord', 'crud', 'com.psedu.base', 'psedu-base', 'signRecord', '签到记录', 'mingyue', '0', '/', '{\"parentMenuId\":2003}', 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:53:10', NULL);
INSERT INTO `gen_table` VALUES (21, 'psedu_semester', '学期', NULL, NULL, 'Semester', 'crud', 'com.psedu.base', 'psedu-base', 'semester', '学期', 'mingyue', '0', '/', '{\"parentMenuId\":\"2003\"}', 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19', NULL);
INSERT INTO `gen_table` VALUES (22, 'psedu_course', '课程', NULL, NULL, 'Course', 'crud', 'com.psedu.base', 'psedu-base', 'course', '课程', 'mingyue', '0', '/', '{\"parentMenuId\":2003}', 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:18', NULL);
INSERT INTO `gen_table` VALUES (23, 'psedu_group', '小组', NULL, NULL, 'Group', 'crud', 'com.psedu.base', 'psedu-base', 'group', '小组', 'mingyue', '0', '/', '{\"parentMenuId\":\"2003\"}', 'admin', '2022-03-07 17:12:46', '', '2022-03-07 17:27:26', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 224 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'apply_id', '申请ID', 'int(11)', 'Long', 'applyId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (2, '1', 'real_name', '真实姓名', 'varchar(255)', 'String', 'realName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (3, '1', 'ps_user_id', '党校用户ID', 'int(11)', 'Long', 'psUserId', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (4, '1', 'seme_id', '报名学期ID', 'int(11)', 'Long', 'semeId', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (5, '1', 'party_data', '本人关于党的信息;年月日，列为入党积极分子日期(入党积极分子培训)/入党日期(预备党员培训)/参加入党积极分子培训结业时间(发展对象培训)', 'varchar(30)', 'String', 'partyData', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (6, '1', 'sex', '性别', 'char(4)', 'String', 'sex', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'select', 'sys_user_sex', 6, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (7, '1', 'nation', '民族', 'varchar(32)', 'String', 'nation', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (8, '1', 'native_place', '籍贯', 'varchar(255)', 'String', 'nativePlace', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (9, '1', 'edu_background', '学历', 'varchar(20)', 'String', 'eduBackground', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-01-30 11:07:35', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (10, '1', 'classes', '所在班级', 'varchar(50)', 'String', 'classes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (11, '1', 'phone', '联系电话', 'varchar(20)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 11, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (12, '1', 'email', 'E-mail', 'varchar(32)', 'String', 'email', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 12, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (13, '1', 'qq', 'QQ', 'varchar(16)', 'String', 'qq', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 13, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (14, '1', 'submit_paper_date', '首次申请入党日期', 'varchar(40)', 'String', 'submitPaperDate', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 14, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (15, '1', 'checked', '审核状态', 'tinyint(4)', 'Integer', 'checked', '0', '0', NULL, '1', NULL, '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (16, '1', 'scores', '结业考试得分', 'float(11,2)', 'BigDecimal', 'scores', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 16, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (17, '1', 'summary', '个人小结', 'varchar(255)', 'String', 'summary', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 17, 'admin', '2022-01-30 11:07:36', '', '2022-01-31 10:13:16');
INSERT INTO `gen_table_column` VALUES (35, '3', 'dept_id', NULL, 'int(11)', 'Long', 'deptId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (36, '3', 'dept_name', NULL, 'int(11)', 'Long', 'deptName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (37, '4', 'evaluate_id', '评教ID', 'int(11)', 'Long', 'evaluateId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (38, '4', 'course_id', '课程ID', 'int(11)', 'Long', 'courseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (39, '4', 'ps_user_id', '党校用户ID', 'int(11)', 'Long', 'psUserId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (40, '4', 'score1', NULL, 'int(11)', 'Long', 'score1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (41, '4', 'score2', NULL, 'int(11)', 'Long', 'score2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (42, '4', 'score3', NULL, 'int(11)', 'Long', 'score3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (43, '4', 'score4', NULL, 'int(11)', 'Long', 'score4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (44, '4', 'score5', NULL, 'int(11)', 'Long', 'score5', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (45, '4', 'comment', '评价', 'varchar(500)', 'String', 'comment', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 9, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (46, '4', 'create_time', NULL, 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-01-30 11:07:37', '', NULL);
INSERT INTO `gen_table_column` VALUES (47, '5', 'merge_class_id', '合班ID', 'int(11)', 'Long', 'mergeClassId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-30 11:07:38', '', NULL);
INSERT INTO `gen_table_column` VALUES (48, '5', 'seme_id', '培训学期ID', 'int(11)', 'Long', 'semeId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-01-30 11:07:38', '', NULL);
INSERT INTO `gen_table_column` VALUES (49, '5', 'merge_class_title', '合班名称', 'varchar(255)', 'String', 'mergeClassTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-01-30 11:07:38', '', NULL);
INSERT INTO `gen_table_column` VALUES (50, '5', 'people_count', '人数', 'int(255)', 'Long', 'peopleCount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-01-30 11:07:38', '', NULL);
INSERT INTO `gen_table_column` VALUES (59, '7', 'dept_id', '分党校ID', 'int(11)', 'Long', 'deptId', '1', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-30 11:07:38', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (60, '7', 'seme_id', '学期ID', 'int(11)', 'Long', 'semeId', '1', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2022-01-30 11:07:38', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (61, '7', 'merge_class_id', '分班ID', 'int(11)', 'Long', 'mergeClassId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 3, 'admin', '2022-01-30 11:07:38', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (62, '7', 'manager_name', '管理员名字', 'varchar(255)', 'String', 'managerName', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'input', '', 4, 'admin', '2022-01-30 11:07:38', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (63, '7', 'manager_phone', '管理员电话', 'varchar(255)', 'String', 'managerPhone', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2022-01-30 11:07:39', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (64, '7', 'manager_qq', '管理员QQ', 'varchar(255)', 'String', 'managerQq', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 6, 'admin', '2022-01-30 11:07:39', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (65, '7', 'monitor_name', '班长名字', 'varchar(255)', 'String', 'monitorName', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'input', '', 7, 'admin', '2022-01-30 11:07:39', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (66, '7', 'monitor_phone', '班长电话', 'varchar(255)', 'String', 'monitorPhone', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 8, 'admin', '2022-01-30 11:07:39', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (67, '7', 'monitor_qq', '班长QQ', 'varchar(255)', 'String', 'monitorQq', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, 'admin', '2022-01-30 11:07:39', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (68, '7', 'teacher_name', '分党校校长名', 'varchar(255)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', NULL, 'LIKE', 'input', '', 10, 'admin', '2022-01-30 11:07:39', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (69, '7', 'started', '开启报名;0关闭，1开启', 'tinyint(3)', 'Integer', 'started', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 11, 'admin', '2022-01-30 11:07:39', '', '2022-03-12 10:36:37');
INSERT INTO `gen_table_column` VALUES (70, '8', 'sign_launch_id', '签到ID', 'int(11)', 'Long', 'signLaunchId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (71, '8', 'start_time', '开始时间', 'datetime', 'Date', 'startTime', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'datetime', '', 3, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (72, '8', 'end_time', '结束时间', 'datetime', 'Date', 'endTime', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'datetime', '', 4, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (73, '8', 'course_id', '课程ID', 'int(11)', 'Long', 'courseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (74, '8', 'longitude', '签到经度', 'double', 'Long', 'longitude', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 6, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (75, '8', 'latitude', '签到纬度', 'double', 'Long', 'latitude', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (76, '8', 'accuracy', '限制精度', 'double', 'Long', 'accuracy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (77, '8', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (78, '8', 'deleted', '已删除;0未删除，1已删除', 'tinyint(4)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:52:59');
INSERT INTO `gen_table_column` VALUES (79, '9', 'sign_record_id', '签到记录ID', 'int(11)', 'Long', 'signRecordId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-01-30 11:07:39', '', '2022-05-16 19:53:10');
INSERT INTO `gen_table_column` VALUES (80, '9', 'sign_launch_id', '签到场次ID', 'int(11)', 'Long', 'signLaunchId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-01-30 11:07:40', '', '2022-05-16 19:53:10');
INSERT INTO `gen_table_column` VALUES (81, '9', 'psedu_user_id', '签到用户ID', 'int(11)', 'Long', 'pseduUserId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-01-30 11:07:40', '', '2022-05-16 19:53:10');
INSERT INTO `gen_table_column` VALUES (82, '9', 'create_time', '签到时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 4, 'admin', '2022-01-30 11:07:40', '', '2022-05-16 19:53:10');
INSERT INTO `gen_table_column` VALUES (83, '9', 'longitude', '签到经度', 'double', 'Long', 'longitude', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2022-01-30 11:07:40', '', '2022-05-16 19:53:10');
INSERT INTO `gen_table_column` VALUES (84, '9', 'latitude', '签到纬度', 'double', 'Long', 'latitude', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 6, 'admin', '2022-01-30 11:07:40', '', '2022-05-16 19:53:10');
INSERT INTO `gen_table_column` VALUES (85, '9', 'accuracy', '精度', 'double', 'Long', 'accuracy', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 7, 'admin', '2022-01-30 11:07:40', '', '2022-05-16 19:53:10');
INSERT INTO `gen_table_column` VALUES (185, '21', 'seme_id', '学期ID', 'int(11)', 'Long', 'semeId', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (186, '21', 'seme_name', '学期名', 'varchar(255)', 'String', 'semeName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (187, '21', 'train_object', '培训对象;0入党积极分子，1发展对象，2预备党员', 'tinyint(4)', 'Integer', 'trainObject', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'ps_train_object', 3, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (188, '21', 'active', '启动报名', 'tinyint(4)', 'Integer', 'active', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (189, '21', 'allow_evaluate', '允许评教', 'tinyint(4)', 'Integer', 'allowEvaluate', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 5, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (190, '21', 'start_time', '开始时间', 'datetime', 'Date', 'startTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (191, '21', 'end_time', '结束时间', 'datetime', 'Date', 'endTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (192, '21', 'certificate_comment', '证书建议', 'varchar(255)', 'String', 'certificateComment', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 8, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (193, '21', 'party_comment', '党校建议', 'varchar(255)', 'String', 'partyComment', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (194, '21', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (195, '21', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (196, '21', 'deleted', '已删除', 'tinyint(4)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (197, '21', 'create_by', '创建者', 'varchar(255)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (198, '21', 'update_by', '更新者', 'varchar(255)', 'String', 'updateBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2022-02-10 12:38:02', '', '2022-02-10 12:45:19');
INSERT INTO `gen_table_column` VALUES (199, '22', 'course_id', '课程ID', 'int(11)', 'Long', 'courseId', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:18');
INSERT INTO `gen_table_column` VALUES (200, '22', 'course_title', '课程名称', 'varchar(255)', 'String', 'courseTitle', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:18');
INSERT INTO `gen_table_column` VALUES (201, '22', 'seme_id', '培训学期', 'int(11)', 'Long', 'semeId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:18');
INSERT INTO `gen_table_column` VALUES (202, '22', 'dept_id', '所属部门ID', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 4, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (203, '22', 'merge_class_id', '所属合班ID', 'int(11)', 'Long', 'mergeClassId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (204, '22', 'image_url', '封面图片链接', 'varchar(500)', 'String', 'imageUrl', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'imageUpload', '', 6, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (205, '22', 'realize_course_id', '实现的ID', 'int(11)', 'Long', 'realizeCourseId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (206, '22', 'classroom', '上课地点', 'varchar(255)', 'String', 'classroom', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 8, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (207, '22', 'content', '课程内容', 'varchar(255)', 'String', 'content', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'editor', '', 9, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (208, '22', 'order_num', '排序', 'int(11)', 'Long', 'orderNum', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 10, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (209, '22', 'teacher', '授课教师', 'varchar(255)', 'String', 'teacher', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 11, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (210, '22', 'evaluate', '需要评教', 'tinyint(4)', 'Integer', 'evaluate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (211, '22', 'start_time', '课程开始时间', 'datetime', 'Date', 'startTime', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (212, '22', 'end_time', '结束时间', 'datetime', 'Date', 'endTime', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'datetime', '', 14, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (213, '22', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (214, '22', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 16, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (215, '22', 'deleted', '已删除', 'tinyint(4)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 17, 'admin', '2022-02-10 17:38:38', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (216, '22', 'create_by', '创建者', 'varchar(255)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 18, 'admin', '2022-02-10 17:38:39', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (217, '22', 'update_by', '更新者', 'varchar(255)', 'String', 'updateBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 19, 'admin', '2022-02-10 17:38:39', '', '2022-02-10 17:46:19');
INSERT INTO `gen_table_column` VALUES (218, '23', 'group_id', '分组ID', 'int(11)', 'Long', 'groupId', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-03-07 17:12:46', '', '2022-03-07 17:27:26');
INSERT INTO `gen_table_column` VALUES (219, '23', 'seme_id', '所属学期', 'int(11)', 'Long', 'semeId', '0', '0', '1', NULL, NULL, NULL, '1', 'EQ', 'input', '', 2, 'admin', '2022-03-07 17:12:46', '', '2022-03-07 17:27:26');
INSERT INTO `gen_table_column` VALUES (220, '23', 'dept_id', '所属分党校', 'bigint(20)', 'Long', 'deptId', '0', '0', '1', NULL, NULL, NULL, '1', 'EQ', 'input', '', 3, 'admin', '2022-03-07 17:12:46', '', '2022-03-07 17:27:26');
INSERT INTO `gen_table_column` VALUES (221, '23', 'group_name', '分组名称', 'varchar(40)', 'String', 'groupName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2022-03-07 17:12:46', '', '2022-03-07 17:27:27');
INSERT INTO `gen_table_column` VALUES (222, '23', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2022-03-07 17:12:46', '', '2022-03-07 17:27:27');
INSERT INTO `gen_table_column` VALUES (223, '23', 'create_by', '创建者', 'varchar(40)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2022-03-07 17:12:46', '', '2022-03-07 17:27:27');

-- ----------------------------
-- Table structure for psedu_apply
-- ----------------------------
DROP TABLE IF EXISTS `psedu_apply`;
CREATE TABLE `psedu_apply`  (
  `apply_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '申请ID',
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '真实姓名',
  `hnuster_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '学号/工号',
  `ps_user_id` int(11) NULL DEFAULT NULL COMMENT '党校用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `seme_id` int(11) NULL DEFAULT NULL COMMENT '报名学期ID',
  `party_data` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '本人关于党的信息;年月日，列为入党积极分子日期(入党积极分子培训)/入党日期(预备党员培训)/参加入党积极分子培训结业时间(发展对象培训)',
  `sex` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '性别',
  `nation` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '民族',
  `native_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '籍贯',
  `edu_background` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '学历',
  `classes` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '所在班级',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '出生日期',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'E-mail',
  `qq` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'QQ',
  `submit_paper_date` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '首次申请入党日期',
  `checked` tinyint(4) NULL DEFAULT NULL COMMENT '审核状态;0未审核，1通过，2不通过',
  `scores` float(11, 2) NULL DEFAULT NULL COMMENT '结业考试得分',
  `group_id` int(11) NULL DEFAULT NULL COMMENT '分组ID',
  `seat_num` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '座位号；1001001，合班ID*1000000：行*1000：列',
  `summary` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '个人小结',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新者',
  `deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '已删除',
  PRIMARY KEY (`apply_id`) USING BTREE,
  INDEX `semester_user`(`ps_user_id`, `seme_id`) USING BTREE COMMENT '一次学期每个用户只能申请一次'
) ENGINE = InnoDB AUTO_INCREMENT = 435 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '培训报名' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_apply
-- ----------------------------
INSERT INTO `psedu_apply` VALUES (1, '明月', '1805010220', 105, 200, 1, '2022-03-07T05:11:34.543Z', '1', '汉族', '湘潭', '本科', '计科1班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '111', '2022-03-07T05:11:32.577Z', 1, 11.00, 8, '1037', '总结啦', '2022-02-01 23:33:26', '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (2, '明明2', '1805010220', 106, 200, 1, '2020', '1', '汉族', '湘潭', '硕士', '计科2班', '2022-03-07 13:11:31', '19977777777', '111', '111', '2020', 1, 99.00, NULL, '1040', NULL, '2022-02-01 23:33:26', '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (3, '明明3', '1805010220', 107, 200, 1, '2021', '1', '汉族', '钦州', '硕士', '计科2班', '2022-03-07 13:11:31', '19977777777', 'xxx', 'xxx', '2021.01.01', 1, 88.00, 9, '1045', NULL, '2022-02-01 23:33:26', '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (4, '明明4', '1805010220', 108, 200, 1, '1', '1', '1', '1', '本科', '计科2班', '2022-03-07 13:11:31', '19977777777', '1', '1', '1', 1, 44.00, 9, '1003', NULL, '2022-02-01 23:33:26', '2022-05-26 10:19:22', 'admin', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (15, '明明15', '1805010220', 110, 200, 1, '2022-03-07', '1', '汉族', '湘潭', '博士', '计科3班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 0, 89.00, NULL, '1008', NULL, '2022-03-07 09:28:29', '2022-05-26 10:19:22', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (16, '明明16', '1805010220', 111, 200, 2, '2022-03-07', '1', '汉族', '湘潭', '博士', '计科3班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, 94.00, NULL, '1004', NULL, '2022-03-07 09:33:03', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (17, '明明17', '1805010220', 112, 200, 2, '2022-03-07', '1', '汉族', '湘潭', '本科', '计科3班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, 100.00, 1, '1005', NULL, '2022-03-07 09:40:29', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (18, '明明18', '1805010220', 113, 200, 2, '2022-03-07', '0', '汉族', '湘潭', '硕士', '计科3班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, 99.00, 1, '1006', NULL, '2022-03-07 09:44:55', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (19, '明月19', '1805010220', 114, 200, 2, '2022-03-07', '0', '汉族', '湘潭', '本科', '计科4班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, 88.00, 1, '1000', NULL, '2022-03-07 10:20:02', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (20, '明月20', '1805010220', 115, 200, 2, '2022-03-07', '0', '汉族', '湘潭', '本科', '计科4班', '2000-01-01 00:00:00', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, 1, '1001', NULL, '2022-03-07 13:25:03', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (21, '明月21', '1805010220', 116, 201, 1, '2022-03-07', '0', '汉族', '湘潭', '本科', '计科1班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, '1054', '总结啦', '2022-02-01 23:33:26', '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (22, '明明22', '1805010220', 117, 202, 1, '2022-03-07', '0', '汉族', '湘潭', '硕士', '计科2班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, '1085', NULL, '2022-02-01 23:33:26', '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (23, '明明23', '1805010220', 118, 203, 1, '2022-03-07', '0', '汉族', '湘潭', '硕士', '计科2班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, '1130', NULL, '2022-02-01 23:33:26', '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (24, '明明24', '1805010220', 119, 204, 1, '2022-03-07', '0', '汉族', '湘潭', '本科', '计科2班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, '1174', NULL, '2022-02-01 23:33:26', '2022-05-26 10:19:22', 'admin', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (25, '明月25', '1805010220', 120, 205, 1, '2022-03-07', '0', '汉族', '湘潭', '博士', '计科1班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, '2031', NULL, '2022-03-07 08:52:06', '2022-05-26 10:19:23', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (26, '明月26', '1805010220', 121, 202, 1, '2022-03-07', '0', '汉族', '湘潭', '博士', '计科3班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, '1092', NULL, '2022-03-07 09:28:29', '2022-05-26 10:19:22', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (27, '明月27', '1805010220', 122, 203, 2, '2022-03-07', '0', '汉族', '湘潭', '博士', '计科3班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, NULL, NULL, '2022-03-07 09:33:03', '2022-03-07 09:33:03', '1805010219', '1805010219', 0);
INSERT INTO `psedu_apply` VALUES (28, '明月28', '1805010220', 123, 204, 2, '2022-03-07', '0', '汉族', '湘潭', '本科', '计科3班', '2022-03-07 13:11:31', '19977777777', '284908631@qq.com', '284908631', '2021-01-01', 1, NULL, NULL, '1007', NULL, '2022-03-07 09:40:29', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (29, '明明29', '1805010220', 124, 222, 2, NULL, '0', NULL, NULL, '硕士', '计科3班', NULL, '19977777777', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2022-03-07 09:44:55', '2022-03-07 09:44:55', '1805010219', '1805010219', 0);
INSERT INTO `psedu_apply` VALUES (30, '明月30', '1805010220', 125, 204, 2, NULL, '0', '汉族', '湘潭', '本科', '计科4班', NULL, '19977777777', '234', NULL, NULL, 1, NULL, NULL, '1008', NULL, '2022-03-07 10:20:02', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (31, '明月31', '1805010220', 126, 202, 2, NULL, NULL, NULL, NULL, '本科', '计科4班', '2000-01-01 00:00:00', NULL, NULL, NULL, '2021-01-01', 1, NULL, NULL, '1007', NULL, '2022-03-07 13:25:03', '2022-04-20 13:53:53', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (32, '唐纯', '1805010220', 127, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3033', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (33, '戚攻痱', '1805010220', 128, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1095', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (34, '折讲骟', '1805010220', 129, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1226', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (35, '法嵋', '1805010220', 130, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3036', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (36, '李吠凛', '1805010220', 131, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1099', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (37, '潘爽', '1805010220', 132, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2078', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (38, '贝越犬', '1805010220', 133, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1144', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (39, '百里纱', '1805010220', 134, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4026', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (40, '舜帻筹', '1805010220', 135, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1106', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (41, '储镫篦', '1805010220', 136, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1108', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (42, '靳厢', '1805010220', 137, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1078', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (43, '仲长堰峒', '1805010220', 138, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2084', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (44, '公仪砻', '1805010220', 139, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2040', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (45, '贡劾豌', '1805010220', 140, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1198', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (46, '崔驹祺', '1805010220', 141, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4029', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (47, '乌驷', '1805010220', 142, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1247', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (48, '官祥飑', '1805010220', 143, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1115', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (49, '强歪', '1805010220', 144, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2002', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (50, '羊岷伲', '1805010220', 145, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1208', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (51, '辜峰', '1805010220', 146, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1055', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (52, '侴渎', '1805010220', 147, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1126', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (53, '公廾繁', '1805010220', 148, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 9, '1014', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (54, '温阃扳', '1805010220', 149, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2004', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (55, '毕筝', '1805010220', 150, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2008', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (56, '农签', '1805010220', 151, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3039', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (57, '谢萝胭', '1805010220', 152, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2012', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (58, '楚蚋', '1805010220', 153, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1176', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (59, '栾谑禁', '1805010220', 154, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4032', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (60, '宗滓聩', '1805010220', 155, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1061', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (61, '解靳', '1805010220', 156, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3000', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (62, '郇庶蓝', '1805010220', 157, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1096', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (63, '饶爷', '1805010220', 158, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4000', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (64, '辛欹', '1805010220', 159, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2053', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (65, '贺兰伟', '1805010220', 160, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2018', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (66, '彭骨', '1805010220', 161, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3003', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (67, '束舯', '1805010220', 162, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (68, '邱做', '1805010220', 163, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1066', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (69, '单岫劾', '1805010220', 164, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 8, '1030', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (70, '窦雁', '1805010220', 165, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1234', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (71, '闾丘顸', '1805010220', 166, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4002', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (72, '辜慑析', '1805010220', 167, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3006', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (73, '宿贯炅', '1805010220', 168, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (74, '竺新', '1805010220', 169, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4004', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (75, '太史祚', '1805010220', 170, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1073', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (76, '哈缟酾', '1805010220', 171, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 8, '1036', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (77, '家豚舅', '1805010220', 172, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2023', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (78, '毋镡玲', '1805010220', 173, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1195', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (79, '饶箫茏', '1805010220', 174, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1244', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (80, '穆迹兴', '1805010220', 175, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1156', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (81, '雍门愣埔', '1805010220', 176, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2069', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (82, '孔溪', '1805010220', 177, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3010', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (83, '水庞', '1805010220', 178, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1048', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (84, '南郭蘧', '1805010220', 179, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1210', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (85, '乜俘您', '1805010220', 180, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3013', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (86, '空碍', '1805010220', 181, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1213', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (87, '官仝', '1805010220', 182, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3016', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (88, '有洪', '1805010220', 183, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4006', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (89, '傅鲔', '1805010220', 184, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4008', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (90, '微生虏惕', '1805010220', 185, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3019', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (91, '巴馨汉', '1805010220', 186, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1122', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (92, '相构灶', '1805010220', 187, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1168', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (93, '疏蛄', '1805010220', 188, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 9, '1015', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (94, '云搋奢', '1805010220', 189, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1020', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (95, '杜脘神', '1805010220', 190, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1094', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (96, '鲜于赠亭', '1805010220', 191, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1097', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (97, '年厝', '1805010220', 192, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2026', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (98, '霍梢局', '1805010220', 193, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2028', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (99, '弥蛤对', '1805010220', 194, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1138', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (100, '湛于', '1805010220', 195, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1100', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (101, '倪傻', '1805010220', 196, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1032', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (102, '路嫌踝', '1805010220', 197, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1107', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (103, '官泞', '1805010220', 198, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1193', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (104, '房裰健', '1805010220', 199, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1243', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (105, '召擎', '1805010220', 200, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3021', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (106, '通顶', '1805010220', 201, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1081', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (107, '严灌', '1805010220', 202, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 8, '1000', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (108, '那涩', '1805010220', 203, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1050', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (109, '璩嘛', '1805010220', 204, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2076', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (110, '俞柴', '1805010220', 205, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1009', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (111, '相里郎', '1805010220', 206, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1058', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (112, '麦触务', '1805010220', 207, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1216', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (113, '农萌襄', '1805010220', 208, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1172', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (114, '罗蓑', '1805010220', 209, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2080', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (115, '汤笑瘀', '1805010220', 210, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1060', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (116, '芮鸩廖', '1805010220', 211, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1023', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (117, '蒙级愚', '1805010220', 212, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1225', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (118, '马素蛲', '1805010220', 213, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1140', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (119, '鲁锡', '1805010220', 214, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1102', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (120, '竹喙剿', '1805010220', 215, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1033', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (121, '彭舟', '1805010220', 216, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1150', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (122, '淳于确僵', '1805010220', 217, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4010', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (123, '司城奈万', '1805010220', 218, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1239', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (124, '经省', '1805010220', 219, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1080', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (125, '翁罄馄', '1805010220', 220, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3023', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (126, '古肖疑', '1805010220', 221, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1201', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (127, '司空蠛澍', '1805010220', 222, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2036', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (128, '宿耍', '1805010220', 223, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1004', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (129, '仉郫跖', '1805010220', 224, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1159', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (130, '还法', '1805010220', 225, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1011', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (131, '白喧疆', '1805010220', 226, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1124', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (132, '舒灬', '1805010220', 227, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1170', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (133, '逄糊', '1805010220', 228, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1219', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (134, '朱颦罴', '1805010220', 229, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4012', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (135, '毋觖乳', '1805010220', 230, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1021', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (136, '赖意', '1805010220', 231, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1063', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (137, '路傧卫', '1805010220', 232, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1027', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (138, '冼钹蒎', '1805010220', 233, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1230', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (139, '綦觇计', '1805010220', 234, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 9, '1031', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (140, '于楔', '1805010220', 235, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1072', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (141, '竹尹', '1805010220', 236, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1151', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (142, '巢霾详', '1805010220', 237, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2000', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (143, '挚佤奂', '1805010220', 238, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2045', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (144, '别慈', '1805010220', 239, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3025', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (145, '柏卧', '1805010220', 240, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2005', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (146, '余爹', '1805010220', 241, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1240', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (147, '祝裒怔', '1805010220', 242, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1199', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (148, '年忾肮', '1805010220', 243, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1248', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (149, '劳隧', '1805010220', 244, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1206', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (150, '是饶', '1805010220', 245, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1209', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (151, '于韵', '1805010220', 246, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1211', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (152, '卞蔚费', '1805010220', 247, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1086', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (153, '康宙戽', '1805010220', 248, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1218', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (154, '第五庳', '1805010220', 249, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3027', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (155, '翟瘠朔', '1805010220', 250, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1177', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (156, '蒋侍', '1805010220', 251, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1223', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (157, '陈癌毗', '1805010220', 252, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2010', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (158, '席侏唼', '1805010220', 253, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1065', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (159, '戈兹鹧', '1805010220', 254, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4014', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (160, '柳亠', '1805010220', 255, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4016', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (161, '蓬涤', '1805010220', 256, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4018', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (162, '慕容氐', '1805010220', 257, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2051', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (163, '鲜于窿愍', '1805010220', 258, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1067', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (164, '水强瘀', '1805010220', 259, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1143', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (165, '赫连鉴', '1805010220', 260, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1145', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (166, '柏坦谪', '1805010220', 261, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1075', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (167, '薛右', '1805010220', 262, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1111', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (168, '萧炙', '1805010220', 263, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1079', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (169, '姚茳', '1805010220', 264, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1042', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (170, '亓亳炊', '1805010220', 265, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3029', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (171, '随铹', '1805010220', 266, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2015', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (172, '利惫', '1805010220', 267, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1157', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (173, '相食', '1805010220', 268, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1049', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (174, '章脬', '1805010220', 269, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1084', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (175, '刘亚酢', '1805010220', 270, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1120', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (176, '长孙惧', '1805010220', 271, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3031', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (177, '公仪封憎', '1805010220', 272, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2056', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (178, '谭淝', '1805010220', 273, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1057', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (179, '崔透献', '1805010220', 274, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1215', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (180, '胶惝', '1805010220', 275, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 9, '1016', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (181, '穆脞劐', '1805010220', 276, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2059', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (182, '尹箱膝', '1805010220', 277, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4020', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (183, '帅泉轮', '1805010220', 278, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1091', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (184, '梁嗍', '1805010220', 279, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1062', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (185, '璩坏', '1805010220', 280, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3034', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (186, '言颚', '1805010220', 281, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1098', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (187, '公羊钳极', '1805010220', 282, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2063', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (188, '贝巽', '1805010220', 283, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1229', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (189, '狄数遣', '1805010220', 284, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2066', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (190, '南郭粮勾', '1805010220', 285, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3037', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (191, '贺撕', '1805010220', 286, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4022', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (192, '空嗾', '1805010220', 287, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1231', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (193, '哈囤', '1805010220', 288, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3040', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (194, '焦茴', '1805010220', 289, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1105', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (195, '仲孙皈', '1805010220', 290, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3001', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (196, '微生遢娶', '1805010220', 291, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1189', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (197, '云若', '1805010220', 292, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2025', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (198, '潘龊钯', '1805010220', 293, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2073', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (199, '巴枉', '1805010220', 294, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2075', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (200, '洪蕴蟪', '1805010220', 295, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1152', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (201, '龙首蛙', '1805010220', 296, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1196', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (202, '臧反', '1805010220', 297, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, 9, '1043', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (203, '米朽', '1805010220', 298, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3004', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (204, '武葛厄', '1805010220', 299, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1002', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (205, '陆锶', '1805010220', 300, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1051', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (206, '伍采四', '1805010220', 301, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3007', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (207, '钮裎乖', '1805010220', 302, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2077', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (208, '齐枉葆', '1805010220', 303, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1010', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (209, '夏锊', '1805010220', 304, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2082', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (210, '巫马跆逼', '1805010220', 305, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1167', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (211, '汪陆', '1805010220', 306, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1217', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (212, '强孕刍', '1805010220', 307, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1090', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (213, '卢瘳诊', '1805010220', 308, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2038', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (214, '雍门曳宣', '1805010220', 309, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1222', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (215, '胡母钢', '1805010220', 310, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1182', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (216, '麦疔', '1805010220', 311, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4024', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (217, '伏珀忘', '1805010220', 312, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1235', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (218, '别剧', '1805010220', 313, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1149', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (219, '颜孛啡', '1805010220', 314, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1194', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (220, '谢悄', '1805010220', 315, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2001', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (221, '倪舸', '1805010220', 316, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1113', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (222, '温扛', '1805010220', 317, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1246', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (223, '靳禅禚', '1805010220', 318, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1158', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (224, '法眼滂', '1805010220', 319, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1082', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (225, '车正漠', '1805010220', 320, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1163', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (226, '贺兰咧厍', '1805010220', 321, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1123', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (227, '独孤茺揆', '1805010220', 322, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1131', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (228, '韩姻', '1805010220', 323, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1018', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (229, '沃颁', '1805010220', 324, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2047', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (230, '郎麻', '1805010220', 325, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1093', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (231, '殳雌亮', '1805010220', 326, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1136', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (232, '麦栌', '1805010220', 327, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1026', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (233, '岳浪', '1805010220', 328, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1141', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (234, '綦乜团', '1805010220', 329, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1103', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (235, '陆觚掌', '1805010220', 330, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1237', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (236, '伍祥', '1805010220', 331, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2006', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (237, '方爱', '1805010220', 332, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3011', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (238, '逯铖', '1805010220', 333, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4027', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (239, '冀贵', '1805010220', 334, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1109', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (240, '宣齿', '1805010220', 335, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1038', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (241, '南郭芹蹋', '1805010220', 336, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1154', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (242, '宁烫', '1805010220', 337, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1202', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (243, '空秒', '1805010220', 338, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1005', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (244, '那阿皱', '1805010220', 339, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2049', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (245, '瞿俘', '1805010220', 340, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3014', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (246, '太叔照', '1805010220', 341, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1119', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (247, '郝疑', '1805010220', 342, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1164', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (248, '殷勉', '1805010220', 343, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2050', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (249, '楚昏', '1805010220', 344, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1059', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (250, '抗韶', '1805010220', 345, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1087', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (251, '商茳', '1805010220', 346, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2014', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (252, '邓累', '1805010220', 347, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1019', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (253, '苍俅紊', '1805010220', 348, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1134', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (254, '劳郸遑', '1805010220', 349, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3017', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (255, '高值', '1805010220', 350, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2016', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (256, '鲁璐轰', '1805010220', 351, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3020', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (257, '霍蒈缫', '1805010220', 352, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1064', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (258, '钮嵝', '1805010220', 353, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1028', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (259, '谷江咎', '1805010220', 354, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1069', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (260, '仲孙辖吖', '1805010220', 355, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1104', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (261, '范傥挎', '1805010220', 356, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1074', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (262, '濮畎', '1805010220', 357, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1110', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (263, '冼榫艘', '1805010220', 358, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4030', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (264, '万俟粕', '1805010220', 359, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4033', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (265, '荣金', '1805010220', 360, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1242', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (266, '司寇秘腚', '1805010220', 361, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1245', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (267, '祝莫', '1805010220', 362, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2057', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (268, '毋桑乇', '1805010220', 363, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1204', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (269, '壤驷哳身', '1805010220', 364, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1207', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (270, '强碴', '1805010220', 365, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1083', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (271, '漆酡', '1805010220', 366, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1214', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (272, '魏颔', '1805010220', 367, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1056', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (273, '迟掼效', '1805010220', 368, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1128', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (274, '詹箧味', '1805010220', 369, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1089', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (275, '沃恸', '1805010220', 370, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2060', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (276, '蒯匏均', '1805010220', 371, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1220', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (277, '郭凤袈', '1805010220', 372, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1022', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (278, '郁萧', '1805010220', 373, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1024', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (279, '枚薪', '1805010220', 374, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1184', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (280, '花鲕蒗', '1805010220', 375, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2022', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (281, '元禄榭', '1805010220', 376, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4001', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (282, '秦胖', '1805010220', 377, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3022', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (283, '茹占', '1805010220', 378, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2067', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (284, '高肠', '1805010220', 379, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2070', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (285, '崔柘差', '1805010220', 380, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2027', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (286, '冼肭', '1805010220', 381, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2030', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (287, '召癍', '1805010220', 382, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2034', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (288, '居吗膘', '1805010220', 383, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1101', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (289, '璩壤哮', '1805010220', 384, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1187', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (290, '勾测', '1805010220', 385, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1034', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (291, '柯逃猬', '1805010220', 386, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1076', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (292, '严搀总', '1805010220', 387, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1039', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (293, '张湔', '1805010220', 388, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2083', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (294, '裴酱', '1805010220', 389, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1200', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (295, '陆勺抹', '1805010220', 390, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3024', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (296, '车齐', '1805010220', 391, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2039', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (297, '汤觋', '1805010220', 392, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1001', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (298, '颜觳', '1805010220', 393, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3026', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (299, '邹屁彩', '1805010220', 394, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1116', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (300, '木缔', '1805010220', 395, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1161', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (301, '繁郎敏', '1805010220', 396, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3028', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (302, '贺兰醺痃', '1805010220', 397, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1166', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (303, '慕禁隆', '1805010220', 398, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2043', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (304, '皇鳞', '1805010220', 399, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1129', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (305, '沃贶', '1805010220', 400, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2003', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (306, '蒯痼酵', '1805010220', 401, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2007', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (307, '楼拱诨', '1805010220', 402, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1017', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (308, '禄屣', '1805010220', 403, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2011', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (309, '施骢竿', '1805010220', 404, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1221', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (310, '向蛙', '1805010220', 405, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2052', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (311, '陆噢胚', '1805010220', 406, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1181', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (312, '凤偬', '1805010220', 407, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1025', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (313, '郏葶喑', '1805010220', 408, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1068', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (314, '公仪绨蚯', '1805010220', 409, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1186', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (315, '闻人邂', '1805010220', 410, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1236', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (316, '唐鹑', '1805010220', 411, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (317, '端木世', '1805010220', 412, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3030', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (318, '仲拮', '1805010220', 413, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2058', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (319, '兀官鲷', '1805010220', 414, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4003', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (320, '井於首', '1805010220', 415, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2061', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (321, '计折颇', '1805010220', 416, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2064', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (322, '松哭栈', '1805010220', 417, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2068', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (323, '厉猱', '1805010220', 418, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1238', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (324, '达漤', '1805010220', 419, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1112', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (325, '赫连拘', '1805010220', 420, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1041', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (326, '纪巳姬', '1805010220', 421, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1046', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (327, '霍汤腱', '1805010220', 422, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1047', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (328, '熊淼蠢', '1805010220', 423, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1118', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (329, '高朦蕻', '1805010220', 424, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2071', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (330, '崔廾', '1805010220', 425, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1121', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (331, '诸郑悚', '1805010220', 426, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3032', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (332, '令狐荒俐', '1805010220', 427, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1125', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (333, '申耍', '1805010220', 428, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2074', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (334, '轩辕筘宴', '1805010220', 429, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3035', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (335, '后躜', '1805010220', 430, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4005', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (336, '卫咣', '1805010220', 431, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1132', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (337, '卜汲', '1805010220', 432, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1175', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (338, '平孜碹', '1805010220', 433, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1179', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (339, '司城膘炔', '1805010220', 434, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1137', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (340, '福敦', '1805010220', 435, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3038', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (341, '余宕嗟', '1805010220', 436, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1227', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (342, '熊振玟', '1805010220', 437, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4007', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (343, '尤醢', '1805010220', 438, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1142', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (344, '童框', '1805010220', 439, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4009', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (345, '扈傅', '1805010220', 440, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1233', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (346, '羿蜣', '1805010220', 441, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4011', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (347, '曲价狩', '1805010220', 442, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1148', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (348, '于惑也', '1805010220', 443, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4013', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (349, '随驹腋', '1805010220', 444, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3041', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (350, '利芷菰', '1805010220', 445, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1191', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (351, '柴役寅', '1805010220', 446, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1241', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (352, '郁嫩', '1805010220', 447, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3002', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (353, '阙拶', '1805010220', 448, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1114', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (354, '鞠陧', '1805010220', 449, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1203', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (355, '冉状', '1805010220', 450, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1006', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (356, '敖枳樾', '1805010220', 451, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1052', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (357, '余刻', '1805010220', 452, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1212', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (358, '篁汜', '1805010220', 453, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3005', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (359, '仓聍', '1805010220', 454, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2032', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (360, '吉肜嗅', '1805010220', 455, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1127', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (361, '召无', '1805010220', 456, 202, 1, NULL, NULL, NULL, NULL, '本科', '资源与安全学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1088', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (362, '顾铞', '1805010220', 457, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2079', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (363, '火跫毡', '1805010220', 458, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2035', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (364, '桓脒', '1805010220', 459, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1178', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (365, '舒驺硷', '1805010220', 460, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2041', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (366, '项蚂锶', '1805010220', 461, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1180', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (367, '岳龋', '1805010220', 462, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1183', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (368, '董顸', '1805010220', 463, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4015', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (369, '却陌罅', '1805010220', 464, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1228', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (370, '许轵畋', '1805010220', 465, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2044', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (371, '经贷坎', '1805010220', 466, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1185', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (372, '长孙膂', '1805010220', 467, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1146', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (373, '童冂抄', '1805010220', 468, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3008', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (374, '公西曦', '1805010220', 469, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2048', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (375, '解嗯糇', '1805010220', 470, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1188', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (376, '魏牡', '1805010220', 471, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4017', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (377, '桂钜蒽', '1805010220', 472, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1192', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (378, '奚祷', '1805010220', 473, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4019', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (379, '荆犷矸', '1805010220', 474, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2009', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (380, '伏耖', '1805010220', 475, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2013', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (381, '梅帅拓', '1805010220', 476, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4021', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (382, '折瀚佴', '1805010220', 477, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2054', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (383, '达戋', '1805010220', 478, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1153', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (384, '屈萆', '1805010220', 479, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1155', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (385, '祝摁', '1805010220', 480, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1249', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (386, '舒燕遮', '1805010220', 481, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1007', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (387, '南宫睃', '1805010220', 482, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2055', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (388, '钮驷猖', '1805010220', 483, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4023', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (389, '赏埕', '1805010220', 484, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1160', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (390, '端木尚', '1805010220', 485, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1053', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (391, '缑颊粜', '1805010220', 486, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (392, '濮煊听', '1805010220', 487, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3009', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (393, '昝姻', '1805010220', 488, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2062', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (394, '易泺涔', '1805010220', 489, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1165', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (395, '施殷喱', '1805010220', 490, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1169', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (396, '谷梁擐菅', '1805010220', 491, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3012', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (397, '狄耥', '1805010220', 492, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1171', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (398, '须责', '1805010220', 493, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1133', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (399, '宗恸', '1805010220', 494, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1135', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (400, '姜薏菩', '1805010220', 495, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2065', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (401, '应蛆琨', '1805010220', 496, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1224', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (402, '师续', '1805010220', 497, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1139', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (403, '宋咦噗', '1805010220', 498, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (404, '隗靠', '1805010220', 499, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1232', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (405, '于跬韬', '1805010220', 500, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3015', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (406, '商清聘', '1805010220', 501, 209, 1, NULL, NULL, NULL, NULL, '本科', '物理与电子科学学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '3018', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (407, '伏钝', '1805010220', 502, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1071', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (408, '石腴醒', '1805010220', 503, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1190', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (409, '常脎灯', '1805010220', 504, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1077', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (410, '苏塄很', '1805010220', 505, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2072', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (411, '邬海锨', '1805010220', 506, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2029', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (412, '李绿', '1805010220', 507, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2033', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (413, '司徒圳', '1805010220', 508, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4025', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (414, '兀官锌', '1805010220', 509, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2081', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (415, '蓬氆释', '1805010220', 510, 205, 1, NULL, NULL, NULL, NULL, '本科', '机电工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2037', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (416, '车漕歃', '1805010220', 511, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4028', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (417, '蔡耒野', '1805010220', 512, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1197', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (418, '练薪宸', '1805010220', 513, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院2班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4031', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (419, '湛搐', '1805010220', 514, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1044', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (420, '宁荼烩', '1805010220', 515, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2042', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (421, '曾唇璀', '1805010220', 516, 206, 1, NULL, NULL, NULL, NULL, '本科', '信息与电气工程学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '4034', NULL, NULL, '2022-05-26 10:19:24', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (422, '籍卦', '1805010220', 517, 207, 1, NULL, NULL, NULL, NULL, '本科', '化学化工学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1205', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (423, '曹撖', '1805010220', 518, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1117', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (424, '钟莒', '1805010220', 519, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1162', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (425, '包禹傥', '1805010220', 520, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1012', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (426, '刘鞍', '1805010220', 521, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1013', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (427, '颜澳裙', '1805010220', 522, 208, 1, NULL, NULL, NULL, NULL, '本科', '数学与计算科学学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2046', NULL, NULL, '2022-05-26 10:19:23', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (428, '章蘅', '1805010220', 523, 204, 1, NULL, NULL, NULL, NULL, '本科', '教育学院4班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1173', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (429, '养蹁', '1805010220', 524, 200, 1, NULL, NULL, NULL, NULL, '本科', '计算机学院5班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1029', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (430, '公冶龙桄', '1805010220', 525, 201, 1, NULL, NULL, NULL, NULL, '本科', '土木学院3班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1070', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (431, '双蛳挚', '1805010220', 526, 203, 1, NULL, NULL, NULL, NULL, '本科', '建筑与艺术设计学院1班', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '1147', NULL, NULL, '2022-05-26 10:19:22', NULL, 'admin', 0);
INSERT INTO `psedu_apply` VALUES (432, 'sumingyue', '1805010220', 527, 200, 2, NULL, '男', '汉族', '关系', NULL, '计科二班', '2000-01-01 00:00:00', NULL, NULL, NULL, '2021-01-01', 1, NULL, 4, '1002', NULL, '2022-04-10 21:09:08', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (433, '明月', '1805010219', 104, 200, 2, '2021-01-07T16:00:00.000Z', '男', '汉族', '湘潭', '本科', '计科二班', '2000-01-01 00:00:00', '19977777777', NULL, NULL, '2021-01-06T16:00:00.000Z', 1, 99.00, 1, '1003', NULL, '2022-04-10 21:36:38', '2022-05-23 17:35:46', '1805010219', 'admin', 0);
INSERT INTO `psedu_apply` VALUES (434, '明月', '1805010219', 104, 200, 1, '2021-01-11T16:00:00.000Z', '男', '汉族', '湘潭', '本科', '计科二班', '2000-01-01 00:00:00', '19977777777', NULL, NULL, '2021-01-01', 1, 2.00, 9, '1035', NULL, '2022-04-20 13:13:26', '2022-05-26 10:53:31', '1805010219', '1805010219', 0);

-- ----------------------------
-- Table structure for psedu_course
-- ----------------------------
DROP TABLE IF EXISTS `psedu_course`;
CREATE TABLE `psedu_course`  (
  `course_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程ID',
  `course_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '课程名称',
  `seme_id` int(11) NULL DEFAULT NULL COMMENT '培训学期',
  `dept_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '所属部门ID',
  `merge_class_id` int(11) NULL DEFAULT NULL COMMENT '所属合班ID',
  `video_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '课程视频链接',
  `image_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '封面图片链接',
  `realize_course_id` int(11) NOT NULL DEFAULT 0 COMMENT '实现的ID;本记录是党校交由分党校分配的具体实现,0无',
  `classroom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '上课地点',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '课程内容',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `teacher` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '授课教师',
  `evaluate` tinyint(4) NULL DEFAULT NULL COMMENT '评教;1需要评教，0不需要',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '课程开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '已删除',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`course_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '课程' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_course
-- ----------------------------
INSERT INTO `psedu_course` VALUES (1, '开学典礼', 1, 0, 1, 'https://file.mingyuefusu.top/statics/2022/05/14/33c7276a-e949-4e36-96f9-b5a6e3d74335.mp4', 'https://file.mingyuefusu.top/statics/2022/05/18/fc506d33-b278-4c9a-ab43-b03955220f53.jpg', 0, '俱乐部', '开学典礼', NULL, '老师', 0, '2022-02-09 00:00:00', '2022-02-17 00:00:00', '2022-02-10 19:43:10', '2022-05-24 09:27:21', 0, 'admin', 'admin');
INSERT INTO `psedu_course` VALUES (2, '22', 2, 0, 1, 'https://file.mingyuefusu.top/statics/2022/05/14/33c7276a-e949-4e36-96f9-b5a6e3d74335.mp4', 'https://file.mingyuefusu.top/statics/2022/02/10/14c4689a-f7e1-480e-b43e-e8b572ea063e.png', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 20:21:21', '2022-02-10 20:37:17', 0, 'admin', 'admin');
INSERT INTO `psedu_course` VALUES (3, '党的指导思想', 1, 0, 1, 'https://file.mingyuefusu.top/statics/2022/05/14/33c7276a-e949-4e36-96f9-b5a6e3d74335.mp4', 'https://file.mingyuefusu.top/statics/2022/02/10/14c4689a-f7e1-480e-b43e-e8b572ea063e.png', 0, '俱乐部', '', NULL, '张老师', NULL, NULL, NULL, '2022-02-14 22:23:07', '2022-05-23 17:46:18', 0, 'admin', 'jisuanji');
INSERT INTO `psedu_course` VALUES (4, '社会实践', 1, 0, 1, 'https://file.mingyuefusu.top/statics/2022/05/14/33c7276a-e949-4e36-96f9-b5a6e3d74335.mp4', NULL, 0, '五教', '社会实践', NULL, NULL, NULL, '2022-03-02 00:00:00', NULL, '2022-02-14 22:23:07', '2022-05-23 17:50:51', 0, 'admin', 'jisuanji');
INSERT INTO `psedu_course` VALUES (5, '爱护环境', 1, 200, 1, 'https://file.mingyuefusu.top/statics/2022/05/14/33c7276a-e949-4e36-96f9-b5a6e3d74335.mp4', NULL, 4, '五教门口', '清理卫生', NULL, NULL, NULL, '2022-03-02 00:00:00', NULL, '2022-02-14 22:23:07', '2022-05-15 17:34:27', 0, 'admin', 'admin');
INSERT INTO `psedu_course` VALUES (6, '养老院活动', 1, 201, 1, 'https://file.mingyuefusu.top/statics/2022/05/14/33c7276a-e949-4e36-96f9-b5a6e3d74335.mp4', NULL, 3, '五教', '养老院活动', NULL, NULL, NULL, NULL, NULL, '2022-02-14 22:23:07', '2022-05-10 15:55:59', 0, 'admin', 'admin');
INSERT INTO `psedu_course` VALUES (10, '为什么入党', 1, 0, 1, 'http://127.0.0.1:9300/statics/2022/05/23/1daf27af-5898-43d5-96c9-83fba02f2298.mp4', NULL, 0, NULL, '“你为什么入党？“', NULL, NULL, NULL, '2022-02-15 00:00:00', NULL, '2022-05-15 17:27:28', '2022-05-23 17:51:56', 0, 'admin', 'jisuanji');
INSERT INTO `psedu_course` VALUES (11, '开学典礼', 1, 100, 1, 'https://file.mingyuefusu.top/statics/2022/05/14/33c7276a-e949-4e36-96f9-b5a6e3d74335.mp4', 'https://file.mingyuefusu.top/statics/2022/02/10/14c4689a-f7e1-480e-b43e-e8b572ea063e.png', 1, '俱乐部', '开学典礼', NULL, '老师', 0, '2022-02-09 00:00:00', '2022-02-17 00:00:00', '2022-02-10 19:43:10', '2022-02-10 19:43:10', 0, 'admin', 'admin');
INSERT INTO `psedu_course` VALUES (12, '测试课程签到', 1, 0, 1, NULL, NULL, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, '2022-05-17 14:05:30', '2022-05-17 14:05:30', 0, 'admin', 'admin');
INSERT INTO `psedu_course` VALUES (13, '测试课程签到2', 1, 0, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-17 14:07:53', '2022-05-17 14:07:53', 0, 'admin', 'admin');
INSERT INTO `psedu_course` VALUES (14, '测试课程签到3', 1, 0, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-17 14:08:14', '2022-05-17 15:01:06', 0, 'admin', 'admin');

-- ----------------------------
-- Table structure for psedu_dept
-- ----------------------------
DROP TABLE IF EXISTS `psedu_dept`;
CREATE TABLE `psedu_dept`  (
  `dept_id` int(11) NULL DEFAULT NULL,
  `dept_name` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '分党校' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_dept
-- ----------------------------

-- ----------------------------
-- Table structure for psedu_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `psedu_evaluate`;
CREATE TABLE `psedu_evaluate`  (
  `evaluate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '评教ID',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程ID',
  `ps_user_id` int(11) NULL DEFAULT NULL COMMENT '党校用户ID',
  `score1` int(11) NULL DEFAULT NULL,
  `score2` int(11) NULL DEFAULT NULL,
  `score3` int(11) NULL DEFAULT NULL,
  `score4` int(11) NULL DEFAULT NULL,
  `score5` int(11) NULL DEFAULT NULL,
  `comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '评价',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`evaluate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '评教' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_evaluate
-- ----------------------------

-- ----------------------------
-- Table structure for psedu_group
-- ----------------------------
DROP TABLE IF EXISTS `psedu_group`;
CREATE TABLE `psedu_group`  (
  `group_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分组ID',
  `seme_id` int(11) NOT NULL COMMENT '所属学期',
  `dept_id` bigint(20) NOT NULL COMMENT '所属分党校',
  `group_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分组名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '小组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_group
-- ----------------------------
INSERT INTO `psedu_group` VALUES (1, 2, 200, '计算机第一组', NULL, NULL);
INSERT INTO `psedu_group` VALUES (4, 2, 200, '计算机第二组', '2022-03-11 11:10:07', NULL);
INSERT INTO `psedu_group` VALUES (5, 2, 201, '学院第一组', '2022-03-11 11:11:31', NULL);
INSERT INTO `psedu_group` VALUES (6, 2, 201, '学院第二组', '2022-03-11 11:11:42', NULL);
INSERT INTO `psedu_group` VALUES (7, 2, 201, '学院第三组', '2022-03-12 08:59:32', NULL);
INSERT INTO `psedu_group` VALUES (8, 1, 200, '第一小组', '2022-04-04 22:20:28', NULL);
INSERT INTO `psedu_group` VALUES (9, 1, 200, '第二小组', '2022-04-23 22:21:05', NULL);
INSERT INTO `psedu_group` VALUES (10, 3, 200, NULL, '2022-05-12 09:26:46', NULL);

-- ----------------------------
-- Table structure for psedu_merge_class
-- ----------------------------
DROP TABLE IF EXISTS `psedu_merge_class`;
CREATE TABLE `psedu_merge_class`  (
  `merge_class_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '合班ID',
  `seme_id` int(11) NULL DEFAULT NULL COMMENT '培训学期ID',
  `merge_class_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '合班名称',
  `people_count` int(255) NULL DEFAULT NULL COMMENT '人数',
  PRIMARY KEY (`merge_class_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '合班' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_merge_class
-- ----------------------------

-- ----------------------------
-- Table structure for psedu_semester
-- ----------------------------
DROP TABLE IF EXISTS `psedu_semester`;
CREATE TABLE `psedu_semester`  (
  `seme_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学期ID',
  `seme_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '学期名',
  `train_object` tinyint(4) NULL DEFAULT NULL COMMENT '培训对象;0入党积极分子，1发展对象，2预备党员',
  `active` tinyint(4) NOT NULL DEFAULT 0 COMMENT '启动报名',
  `allow_evaluate` tinyint(4) NOT NULL DEFAULT 0 COMMENT '允许评教',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `certificate_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '证书建议',
  `party_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '党校建议',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '已删除',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`seme_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '学期' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_semester
-- ----------------------------
INSERT INTO `psedu_semester` VALUES (1, '2020年下半年入党积极分子培训', 0, 1, 1, '2022-02-08 00:00:00', '2022-02-19 00:00:00', NULL, NULL, '2022-02-09 12:52:25', '2022-05-12 09:06:33', 0, NULL, 'admin');
INSERT INTO `psedu_semester` VALUES (2, '2020年下半年发展对象培训', 1, 1, 1, '2022-02-08 00:00:00', '2022-04-22 00:00:00', NULL, NULL, '2022-02-09 13:02:35', '2022-05-12 09:06:34', 0, 'admin', 'admin');
INSERT INTO `psedu_semester` VALUES (3, '2022年预备党员培训', 2, 1, 1, '2022-04-20 00:00:00', '2022-04-29 00:00:00', NULL, NULL, '2022-04-20 13:25:12', '2022-05-12 09:06:34', 0, 'admin', 'admin');

-- ----------------------------
-- Table structure for psedu_semester_dept
-- ----------------------------
DROP TABLE IF EXISTS `psedu_semester_dept`;
CREATE TABLE `psedu_semester_dept`  (
  `dept_id` int(11) NOT NULL COMMENT '分党校ID',
  `seme_id` int(11) NOT NULL COMMENT '学期ID',
  `merge_class_id` int(11) NULL DEFAULT NULL COMMENT '分班ID',
  `manager_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员名字',
  `manager_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员电话',
  `manager_qq` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员QQ',
  `monitor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '班长名字',
  `monitor_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '班长电话',
  `monitor_qq` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '班长QQ',
  `teacher_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '分党校校长名',
  `started` tinyint(3) NULL DEFAULT NULL COMMENT '开启报名;0关闭，1开启',
  PRIMARY KEY (`dept_id`, `seme_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '学期分党校' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_semester_dept
-- ----------------------------
INSERT INTO `psedu_semester_dept` VALUES (200, 1, 1, 'ming', NULL, '284908531', NULL, NULL, '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (200, 2, 1, 'ming', '19977777777', '284908531', 'ming', '19977777777', '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (201, 1, 1, 'ming1', NULL, '284908531', NULL, '19977777777', '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (201, 2, 0, '明月', '19977777777', '284908531', 'ming', '19977777777', '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (202, 1, 1, 'ming', NULL, '284908531', 'ming', '19977777777', '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (202, 2, 0, 'ming1', NULL, '284908531', 'ming', '19977777777', '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (203, 1, 1, 'ming1', NULL, '284908531', 'ming', '19977777777', '284908631', 'ming', NULL);
INSERT INTO `psedu_semester_dept` VALUES (204, 1, 1, 'ming1', NULL, '284908531', 'ming', '19977777777', '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (204, 2, 1, '明月', '19977777777', '284908531', 'ming', '19977777777', '284908631', 'ming', 0);
INSERT INTO `psedu_semester_dept` VALUES (205, 1, 2, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (206, 1, 4, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (207, 1, 1, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (208, 1, 2, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (209, 1, 3, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (210, 1, 0, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (211, 1, 0, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (212, 1, 0, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (213, 1, 0, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `psedu_semester_dept` VALUES (214, 1, 0, 'ming1', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for psedu_sign_launch
-- ----------------------------
DROP TABLE IF EXISTS `psedu_sign_launch`;
CREATE TABLE `psedu_sign_launch`  (
  `sign_launch_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '签到ID',
  `sign_title` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '签到名称',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程ID',
  `longitude` double NULL DEFAULT NULL COMMENT '签到经度',
  `latitude` double NULL DEFAULT NULL COMMENT '签到纬度',
  `accuracy` double NULL DEFAULT NULL COMMENT '限制精度',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `deleted` tinyint(4) NULL DEFAULT NULL COMMENT '已删除;0未删除，1已删除',
  PRIMARY KEY (`sign_launch_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '发起签到' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_sign_launch
-- ----------------------------
INSERT INTO `psedu_sign_launch` VALUES (6, '第一轮签到', '2022-05-21 00:00:00', '2022-05-31 00:00:00', 1, 112.928238701727, 27.908427188223474, 100, '2022-05-16 23:26:32', NULL);
INSERT INTO `psedu_sign_launch` VALUES (8, '第一轮签到', '2022-05-04 00:00:00', '2022-05-05 00:00:00', 14, 112.92986545851521, 27.909757637340434, NULL, '2022-05-17 14:08:14', NULL);
INSERT INTO `psedu_sign_launch` VALUES (9, '第一轮签到', '2022-05-24 00:00:00', '2022-05-25 00:00:00', 3, 112.92917640750971, 27.906611806842957, 11, '2022-05-23 17:46:18', NULL);

-- ----------------------------
-- Table structure for psedu_sign_record
-- ----------------------------
DROP TABLE IF EXISTS `psedu_sign_record`;
CREATE TABLE `psedu_sign_record`  (
  `sign_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '签到记录ID',
  `sign_launch_id` int(11) NULL DEFAULT NULL COMMENT '签到场次ID',
  `psedu_user_id` int(11) NULL DEFAULT NULL COMMENT '签到用户ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '签到时间',
  `longitude` double NULL DEFAULT NULL COMMENT '签到经度',
  `latitude` double NULL DEFAULT NULL COMMENT '签到纬度',
  `accuracy` double NULL DEFAULT NULL COMMENT '精度',
  PRIMARY KEY (`sign_record_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '签到记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_sign_record
-- ----------------------------
INSERT INTO `psedu_sign_record` VALUES (14, 6, 104, NULL, 112.92839341677, 27.9079904521, 50.94386629426958);

-- ----------------------------
-- Table structure for psedu_user
-- ----------------------------
DROP TABLE IF EXISTS `psedu_user`;
CREATE TABLE `psedu_user`  (
  `ps_user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户党校ID',
  `student_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '学号工号',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '密码',
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '真实姓名',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '分党校部门ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ps_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '党校用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of psedu_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
