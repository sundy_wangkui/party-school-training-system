package com.psedu.exam.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 试卷题目对象 ex_paper_question
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@TableName(value = "ex_paper_question", excludeProperty = {
        "createBy", "updateBy", "createTime", "updateTime"
})
@EqualsAndHashCode(callSuper = true)
@Data
public class PaperQuestion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 试卷题目 */
    @TableId(type = IdType.AUTO)
    private Long paperQuestionId;

    /** 试卷ID */
    @Excel(name = "试卷ID")
    private Long paperId;

    /** 题目ID */
    @Excel(name = "题目ID")
    private Long questionId;

    /** 题目内容*/
    private String content;

    /** 解析 */
    @Excel(name = "解析")
    private String analysis;

    /** 题目类型;0简答题，1单选题，2判断题，3多选题，4填空题 */
    @Excel(name = "题目类型;0简答题，1单选题，2判断题，3多选题，4填空题")
    private Integer type;

    /** 题目分值 */
    @Excel(name = "题目分值")
    private Integer score;

    /** 答案 */
    @Excel(name = "答案")
    private String answer;

    private String optionA;

    private String optionB;

    private String optionC;

    private String optionD;

    private String optionE;

    private String optionF;
}
