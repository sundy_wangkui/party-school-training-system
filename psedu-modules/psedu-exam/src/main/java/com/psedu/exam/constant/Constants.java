package com.psedu.exam.constant;

public class Constants {
    /** 考试状态*/
    public static final int DEPT_EXAM_WAIT = 0;
    public static final int DEPT_EXAM_RUNNING = 1;
    public static final int DEPT_EXAM_END = 2;

    /** 答卷状态*/
    public static final int ANSWER_SHEET_CREATING = -1;
    public static final int ANSWER_SHEET_CREATED = 0;
    public static final int ANSWER_SHEET_IN_USE = 1;
    public static final int ANSWER_SHEET_SUBMITTED = 2;

    /** 题目类型*/
    public static final int SINGLE_QUESTION = 1;
    public static final int JUDGE_QUESTION = 2;
    public static final int MULTI_QUESTION = 3;

    /** 培训对象*/
    public static final int TRAIN_OBJECT_ACTIVE = 0;
    public static final int TRAIN_OBJECT_DEV = 1;
    public static final int TRAIN_OBJECT_PRE = 2;

    /** 试题数量 */
    public static final int SINGLE_COUNT = 50;
    public static final int JUDGE_COUNT = 30;
    public static final int MULTI_COUNT = 10;

    /** 题目单题分数 */
    public static final int SINGLE_PER_SCORE = 1;
    public static final int JUDGE_PER_SCORE = 1;
    public static final int MULTI_PER_SCORE = 2;


    public static final int PASS_SCORE = 60;
}
