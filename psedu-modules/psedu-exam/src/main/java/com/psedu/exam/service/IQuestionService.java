package com.psedu.exam.service;

import java.util.List;
import java.util.Map;

import com.psedu.exam.domain.Question;
import com.psedu.exam.domain.vo.AnswerQuestion;
import com.psedu.exam.domain.vo.QuestionDetail;
import com.psedu.exam.domain.vo.QuestionVo;

/**
 * 候选题目Service接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface IQuestionService 
{
    /**
     * 查询候选题目
     * 
     * @param questionId 候选题目主键
     * @return 候选题目
     */
    public Question selectQuestionByQuestionId(Long questionId);

    /**
     * 查询候选题目列表
     * 
     * @param question 候选题目
     * @return 候选题目集合
     */
    public List<Question> selectQuestionList(Question question);

    /**
     * 新增候选题目
     * 
     * @param question 候选题目
     * @return 结果
     */
    public int insertQuestion(Question question);

    /**
     * 修改候选题目
     * 
     * @param question 候选题目
     * @return 结果
     */
    public int updateQuestion(Question question);

    /**
     * 批量删除候选题目
     * 
     * @param questionIds 需要删除的候选题目主键集合
     * @return 结果
     */
    public int deleteQuestionByQuestionIds(Long[] questionIds);

    /**
     * 删除候选题目信息
     * 
     * @param questionId 候选题目主键
     * @return 结果
     */
    public int deleteQuestionByQuestionId(Long questionId);

    /**
     * 添加试题和选项、答案
     * @param questionVo questionVo
     * @return 结果
     */
    int insertQuestionAndAnswer(QuestionVo questionVo);

    /**
     * 获取题目详细
     * @param questionId 题目ID
     * @return 题目和选项信息
     */
    QuestionVo getQuestionDetail(Long questionId);

    /**
     * 修改题目和答案
     * @param questionVo 题目和选项信息
     * @return 修改结果
     */
    int updateQuestionAndAnswer(QuestionVo questionVo);

    QuestionDetail getOneRandomQuestionDetail(Integer trainObject);

    boolean checkAnswer(AnswerQuestion answerQuestion);

    /**
     * 对题目回答进行评卷
     * @param answerQuestionList 题目回答list
     * @return map questionId, isTrue
     */
    Map<Long, Boolean> checkAnswer(List<AnswerQuestion> answerQuestionList);

    /**
     * 获取题目 list 正确答案
     * @param questionIds 问题 Id 列表
     * @return Map questionId : [answerId...]
     */
    Map<Long, List<Long>> getCorrectAnswer(List<Long> questionIds);
}
