package com.psedu.exam.mapper;

import java.util.List;
import com.psedu.exam.domain.AnswerOption;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Mapper;

/**
 * 题目选项Mapper接口
 * 
 * @author mingyue
 * @date 2022-03-29
 */
public interface AnswerOptionMapper extends BaseMapperPlus<AnswerOptionMapper, AnswerOption, AnswerOption>
{
    /**
     * 查询题目选项
     * 
     * @param answerOptionId 题目选项主键
     * @return 题目选项
     */
    public AnswerOption selectAnswerOptionByAnswerOptionId(Long answerOptionId);

    /**
     * 查询题目选项列表
     * 
     * @param answerOption 题目选项
     * @return 题目选项集合
     */
    public List<AnswerOption> selectAnswerOptionList(AnswerOption answerOption);

    /**
     * 新增题目选项
     * 
     * @param answerOption 题目选项
     * @return 结果
     */
    public int insertAnswerOption(AnswerOption answerOption);

    /**
     * 修改题目选项
     * 
     * @param answerOption 题目选项
     * @return 结果
     */
    public int updateAnswerOption(AnswerOption answerOption);

    /**
     * 删除题目选项
     * 
     * @param answerOptionId 题目选项主键
     * @return 结果
     */
    public int deleteAnswerOptionByAnswerOptionId(Long answerOptionId);

    /**
     * 批量删除题目选项
     * 
     * @param answerOptionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAnswerOptionByAnswerOptionIds(Long[] answerOptionIds);
}
