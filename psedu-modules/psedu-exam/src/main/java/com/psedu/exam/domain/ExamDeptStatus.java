package com.psedu.exam.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 分党校考试信息对象 ex_exam_dept_status
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@TableName(value = "ex_exam_dept_status", excludeProperty = {
        "updateBy", "updateTime"
})
@EqualsAndHashCode(callSuper = true)
@Data
public class ExamDeptStatus extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分党校场次ID */
    @TableId(type = IdType.AUTO)
    private Long examDeptStatusId;

    /** 部门ID */
    private Long deptId;

    /** 考试ID */
    @Excel(name = "考试ID")
    private Long examId;

    /** 当前考试重复次数 */
    @Excel(name = "当前考试重复次数")
    private Integer repeatCount;

    /** 考试状态 */
    private Integer status;

    /** 开始时间 */
    @JsonFormat(pattern = "yy-MM-dd HH:mm")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yy-MM-dd HH:mm")
    private Date endTime;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
