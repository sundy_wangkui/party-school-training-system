package com.psedu.exam.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class MarkTestPaperVo {

    private Integer score;
    private List<AnswerStatus> singleStatusList;
    private List<AnswerStatus> judgeStatusList;
    private List<AnswerStatus> multiStatusList;



}
