package com.psedu.exam.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

import java.util.Date;

/**
 * 候选题目对象 ex_question
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@TableName(value = "ex_question")
@EqualsAndHashCode(callSuper = true)
@Data
public class Question extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 题目ID */
    @TableId(type = IdType.AUTO)
    private Long questionId;

    /** 题目类型;0简答题，1单选题，2判断题，3多选题，4填空题 */
    private Integer type;

    /** 题目内容 */
    private String content;

    /** 难度等级;1-5 */
    private Integer difficultyRating;

    /** 适用入党积极分子;0不适用，1适用 */
    private Integer suitAct;

    /** 适用发展对象 */
    private Integer suitDev;

    /** 适用预备党员 */
    private Integer suitPre;

    /** 适用于正式考试 */
    private Integer realExam;

   /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
