package com.psedu.exam.service;

import java.util.List;
import com.psedu.exam.domain.PaperQuestion;

/**
 * 试卷题目Service接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface IPaperQuestionService 
{
    /**
     * 查询试卷题目
     * 
     * @param paperQuestionId 试卷题目主键
     * @return 试卷题目
     */
    public PaperQuestion selectPaperQuestionByPaperQuestionId(Long paperQuestionId);

    /**
     * 查询试卷题目列表
     * 
     * @param paperQuestion 试卷题目
     * @return 试卷题目集合
     */
    public List<PaperQuestion> selectPaperQuestionList(PaperQuestion paperQuestion);

    /**
     * 新增试卷题目
     * 
     * @param paperQuestion 试卷题目
     * @return 结果
     */
    public int insertPaperQuestion(PaperQuestion paperQuestion);

    /**
     * 修改试卷题目
     * 
     * @param paperQuestion 试卷题目
     * @return 结果
     */
    public int updatePaperQuestion(PaperQuestion paperQuestion);

    /**
     * 批量删除试卷题目
     * 
     * @param paperQuestionIds 需要删除的试卷题目主键集合
     * @return 结果
     */
    public int deletePaperQuestionByPaperQuestionIds(Long[] paperQuestionIds);

    /**
     * 删除试卷题目信息
     * 
     * @param paperQuestionId 试卷题目主键
     * @return 结果
     */
    public int deletePaperQuestionByPaperQuestionId(Long paperQuestionId);
}
