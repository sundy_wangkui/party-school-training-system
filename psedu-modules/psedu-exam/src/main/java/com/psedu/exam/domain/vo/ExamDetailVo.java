package com.psedu.exam.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.psedu.common.core.annotation.Excel;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ExamDetailVo {
    /** 考试ID */
    @TableId(type = IdType.AUTO)
    private Long examId;

    /** 学期ID */
    private Long semeId;

    /** 考试名称 */
    private String examName;

    /** 考试状态;0关闭，1允许考试 */
    private Integer allowStart;

    /** 时间限制;0限制，1不限制 */
    private Integer timeLimitStatus;

    /** 总时间（分钟） */
    private Long totalTime;

    /** 最大重复考试次数 */
    private Long maxRepeatCount;

    /** 是否正式考试 */
    private Integer officialExam;

    /** 分党校考试列表 */
    private List<DeptExamVo> deptExamList;

    @Data
    public static class DeptExamVo {
        /** 分党校场次ID */
        private Long examDeptStatusId;

        /** 部门ID */
        private Long deptId;

        /** 考试ID */
        private Long examId;

        /** 当前考试重复次数 */
        private Integer repeatCount;

        /** 考试状态 */
        private Integer status;

        /** 开始时间 */
        @JsonFormat(pattern = "yy-MM-dd HH:mm")
        private Date startTime;

        /** 结束时间 */
        @JsonFormat(pattern = "yy-MM-dd HH:mm")
        private Date endTime;
    }
}
