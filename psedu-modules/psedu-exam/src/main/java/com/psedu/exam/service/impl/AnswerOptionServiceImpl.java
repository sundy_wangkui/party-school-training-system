package com.psedu.exam.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.psedu.exam.mapper.AnswerOptionMapper;
import com.psedu.exam.domain.AnswerOption;
import com.psedu.exam.service.IAnswerOptionService;

/**
 * 题目选项Service业务层处理
 * 
 * @author mingyue
 * @date 2022-03-29
 */
@Service
public class AnswerOptionServiceImpl implements IAnswerOptionService 
{
    @Autowired
    private AnswerOptionMapper answerOptionMapper;

    /**
     * 查询题目选项
     * 
     * @param answerOptionId 题目选项主键
     * @return 题目选项
     */
    @Override
    public AnswerOption selectAnswerOptionByAnswerOptionId(Long answerOptionId)
    {
        return answerOptionMapper.selectAnswerOptionByAnswerOptionId(answerOptionId);
    }

    /**
     * 查询题目选项列表
     * 
     * @param answerOption 题目选项
     * @return 题目选项
     */
    @Override
    public List<AnswerOption> selectAnswerOptionList(AnswerOption answerOption)
    {
        return answerOptionMapper.selectAnswerOptionList(answerOption);
    }

    /**
     * 新增题目选项
     * 
     * @param answerOption 题目选项
     * @return 结果
     */
    @Override
    public int insertAnswerOption(AnswerOption answerOption)
    {
        return answerOptionMapper.insertAnswerOption(answerOption);
    }

    /**
     * 修改题目选项
     * 
     * @param answerOption 题目选项
     * @return 结果
     */
    @Override
    public int updateAnswerOption(AnswerOption answerOption)
    {
        return answerOptionMapper.updateAnswerOption(answerOption);
    }

    /**
     * 批量删除题目选项
     * 
     * @param answerOptionIds 需要删除的题目选项主键
     * @return 结果
     */
    @Override
    public int deleteAnswerOptionByAnswerOptionIds(Long[] answerOptionIds)
    {
        return answerOptionMapper.deleteAnswerOptionByAnswerOptionIds(answerOptionIds);
    }

    /**
     * 删除题目选项信息
     * 
     * @param answerOptionId 题目选项主键
     * @return 结果
     */
    @Override
    public int deleteAnswerOptionByAnswerOptionId(Long answerOptionId)
    {
        return answerOptionMapper.deleteAnswerOptionByAnswerOptionId(answerOptionId);
    }
}
