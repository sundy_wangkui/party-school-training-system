package com.psedu.exam.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

import java.util.Date;

/**
 * 题目选项对象 ex_answer_option
 * 
 * @author mingyue
 * @date 2022-03-29
 */

@TableName(value = "ex_answer_option", excludeProperty = {
        "createBy", "updateBy", "createTime", "updateTime"
})
@EqualsAndHashCode(callSuper = true)
@Data
public class AnswerOption extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 选项ID */
    @TableId(type = IdType.AUTO)
    private Long answerOptionId;

    /** 候选题目ID */
    @Excel(name = "候选题目ID")
    private Long questionId;

    /** 选项内容 */
    @Excel(name = "选项内容")
    private String content;

    /** 是否正确;0错误，1正确 */
    @Excel(name = "是否正确;0错误，1正确")
    private Integer isCorrect;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 图片链接 */
    private String imageUrl;

    /** 选项分析 */
    private String analysis;

}
