package com.psedu.exam.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.psedu.exam.domain.AnswerOption;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
// TODO 检验
public class QuestionVo {
    /** 题目ID */
    private Long questionId;

    /** 题目类型;0简答题，1单选题，2判断题，3多选题，4填空题 */
    private Integer type;

    /** 题目内容 */
    private String content;

    /** 难度等级;1-5 */
    private Integer difficultyRating;

    /** 适用入党积极分子;0不适用，1适用 */
    private Integer suitAct;

    /** 适用发展对象 */
    private Integer suitDev;

    /** 适用预备党员 */
    private Integer suitPre;

    /** 适用于正式考试 */
    private Integer realExam;

    /** 创建时间 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    private List<AnswerOption> answerOptions;
}
