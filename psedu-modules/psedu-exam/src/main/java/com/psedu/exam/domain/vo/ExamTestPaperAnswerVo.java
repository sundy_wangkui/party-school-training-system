package com.psedu.exam.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class ExamTestPaperAnswerVo {
    private List<AnswerQuestion> single;
    private List<AnswerQuestion> judge;
    private List<AnswerQuestion> multi;
}
