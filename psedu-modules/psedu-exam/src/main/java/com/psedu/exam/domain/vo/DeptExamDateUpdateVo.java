package com.psedu.exam.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class DeptExamDateUpdateVo {

    private Long examDeptStatusId;

    /** 开始时间 */
    @JsonFormat(pattern = "yy-MM-dd HH:mm")
    private Date startTime;
}
