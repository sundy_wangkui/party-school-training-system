package com.psedu.exam.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

import java.util.Date;

/**
 * 考试发起对象 ex_exam
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@TableName(value = "ex_exam")
@EqualsAndHashCode(callSuper = true)
@Data
public class Exam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 考试ID */
    @TableId(type = IdType.AUTO)
    private Long examId;

    /** 学期ID */
    @Excel(name = "学期ID")
    private Long semeId;

    /** 试题规则仓库ID */
    private String paperRuleRepoId;

    /** 考试名称 */
    private String examName;

    /** 考试状态;0关闭，1允许考试 */
    @Excel(name = "考试状态;0关闭，1允许考试")
    private Integer allowStart;

    /** 允许分党校控制具体考试时间;0不交给分党校，1交给分党校 */
    @Excel(name = "允许分党校控制具体考试时间;0不交给分党校，1交给分党校")
    private Integer controlFlag;

    /** 时间限制;0限制，1不限制 */
    @Excel(name = "时间限制;0限制，1不限制")
    private Integer timeLimitStatus;

    /** 总时间（分钟） */
    @Excel(name = "总时间", readConverterExp = "分=钟")
    private Long totalTime;

    /** 最大重复考试次数 */
    @Excel(name = "最大重复考试次数")
    private Long maxRepeatCount;

    /** 是否正式考试 */
    private Integer officialExam;

    /** 已删除 */
    private Integer deleted;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
