package com.psedu.exam.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.psedu.exam.mapper.AnswerSheetMapper;
import com.psedu.exam.domain.AnswerSheet;
import com.psedu.exam.service.IAnswerSheetService;

/**
 * 答卷Service业务层处理
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@Service
public class AnswerSheetServiceImpl implements IAnswerSheetService 
{
    @Autowired
    private AnswerSheetMapper answerSheetMapper;

    /**
     * 查询答卷
     * 
     * @param answerSheetId 答卷主键
     * @return 答卷
     */
    @Override
    public AnswerSheet selectAnswerSheetByAnswerSheetId(Long answerSheetId)
    {
        return answerSheetMapper.selectAnswerSheetByAnswerSheetId(answerSheetId);
    }

    /**
     * 查询答卷列表
     * 
     * @param answerSheet 答卷
     * @return 答卷
     */
    @Override
    public List<AnswerSheet> selectAnswerSheetList(AnswerSheet answerSheet)
    {
        return answerSheetMapper.selectAnswerSheetList(answerSheet);
    }

    /**
     * 新增答卷
     * 
     * @param answerSheet 答卷
     * @return 结果
     */
    @Override
    public int insertAnswerSheet(AnswerSheet answerSheet)
    {
        return answerSheetMapper.insertAnswerSheet(answerSheet);
    }

    /**
     * 修改答卷
     * 
     * @param answerSheet 答卷
     * @return 结果
     */
    @Override
    public int updateAnswerSheet(AnswerSheet answerSheet)
    {
        return answerSheetMapper.updateAnswerSheet(answerSheet);
    }

    /**
     * 批量删除答卷
     * 
     * @param answerSheetIds 需要删除的答卷主键
     * @return 结果
     */
    @Override
    public int deleteAnswerSheetByAnswerSheetIds(Long[] answerSheetIds)
    {
        return answerSheetMapper.deleteAnswerSheetByAnswerSheetIds(answerSheetIds);
    }

    /**
     * 删除答卷信息
     * 
     * @param answerSheetId 答卷主键
     * @return 结果
     */
    @Override
    public int deleteAnswerSheetByAnswerSheetId(Long answerSheetId)
    {
        return answerSheetMapper.deleteAnswerSheetByAnswerSheetId(answerSheetId);
    }
}
