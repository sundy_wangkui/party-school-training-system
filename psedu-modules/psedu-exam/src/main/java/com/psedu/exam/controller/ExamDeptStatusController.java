package com.psedu.exam.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.psedu.exam.domain.vo.DeptExamDateUpdateVo;
import com.psedu.exam.domain.vo.DeptExamVo;
import com.psedu.common.core.domain.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.exam.domain.ExamDeptStatus;
import com.psedu.exam.service.IExamDeptStatusService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 分党校考试信息Controller
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@RestController
@RequestMapping("/examDeptStatus")
public class ExamDeptStatusController extends BaseController {
    @Autowired
    private IExamDeptStatusService examDeptStatusService;

    /**
     * 查询分党校考试信息列表
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:list")
    @GetMapping("/list")
    public TableDataInfo list(ExamDeptStatus examDeptStatus) {
        startPage();
        List<ExamDeptStatus> list = examDeptStatusService.selectExamDeptStatusList(examDeptStatus);
        return getDataTable(list);
    }

    @RequiresPermissions("psedu-exam:examDeptStatus:list")
    @GetMapping("/deptExamView/{examId}")
    public AjaxResult deptExamView(@PathVariable("examId") Long examId,
                                   @RequestParam(value = "deptId", required = false) Long deptId) {
        List<DeptExamVo> deptExamVos = examDeptStatusService.deptExamView(examId, deptId);
        return AjaxResult.success(deptExamVos);
    }

    /**
     * 导出分党校考试信息列表
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:export")
    @Log(title = "分党校考试信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExamDeptStatus examDeptStatus)
    {
        List<ExamDeptStatus> list = examDeptStatusService.selectExamDeptStatusList(examDeptStatus);
        ExcelUtil<ExamDeptStatus> util = new ExcelUtil<ExamDeptStatus>(ExamDeptStatus.class);
        util.exportExcel(response, list, "分党校考试信息数据");
    }

    /**
     * 获取分党校考试信息详细信息
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:query")
    @GetMapping(value = "/{examDeptStatusId}")
    public AjaxResult getInfo(@PathVariable("examDeptStatusId") Long examDeptStatusId)
    {
        return AjaxResult.success(examDeptStatusService.selectExamDeptStatusByExamDeptStatusId(examDeptStatusId));
    }

    /**
     * 新增分党校考试信息
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:add")
    @Log(title = "分党校考试信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExamDeptStatus examDeptStatus)
    {
        return toAjax(examDeptStatusService.insertExamDeptStatus(examDeptStatus));
    }

    /**
     * 修改分党校考试信息
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:edit")
    @Log(title = "分党校考试信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExamDeptStatus examDeptStatus)
    {
        return toAjax(examDeptStatusService.updateExamDeptStatus(examDeptStatus));
    }

    /**
     * 修改考试时间
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:edit")
    @Log(title = "分党校考试时间", businessType = BusinessType.UPDATE)
    @PutMapping("/startTime")
    public AjaxResult updateStartTime(@RequestBody DeptExamDateUpdateVo deptExamDateUpdateVo)
    {
        return toAjax(examDeptStatusService.updateStartTime(deptExamDateUpdateVo));
    }

    /**
     * 修改开启/关闭考试 的下一状态
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:edit")
    @Log(title = "分党校考试时间", businessType = BusinessType.UPDATE)
    @PutMapping("/updateExamStatus/nextStatus/{examDeptStatusId}")
    public R<ExamDeptStatus> nextStatus(@PathVariable("examDeptStatusId") Long examDeptStatusId) {
        return R.ok(examDeptStatusService.nextStatus(examDeptStatusId));
    }

    /**
     * 删除分党校考试信息
     */
    @RequiresPermissions("psedu-exam:examDeptStatus:remove")
    @Log(title = "分党校考试信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{examDeptStatusIds}")
    public AjaxResult remove(@PathVariable Long[] examDeptStatusIds)
    {
        return toAjax(examDeptStatusService.deleteExamDeptStatusByExamDeptStatusIds(examDeptStatusIds));
    }
}
