package com.psedu.exam.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class QuestionDetail {
    private Long questionId;
    private Integer type;
    private String content;
    private List<AnswerOption> optionList;

    @Data
    public static class AnswerOption {
        private Long answerOptionId;
        private String content;
        private Integer sort;
    }

    public  String getOptionContentByIndex(int index) {
        try{
            return optionList.get(index).getContent();
        } catch (Exception e) {
            return "";
        }
    }

    public Long getOptionIdByIndex(int index) {
        try{
            return optionList.get(index).getAnswerOptionId();
        } catch (Exception e) {
            return 0L;
        }
    }

}



