package com.psedu.exam.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.exam.domain.AnswerOption;
import com.psedu.exam.service.IAnswerOptionService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 题目选项Controller
 * 
 * @author mingyue
 * @date 2022-03-29
 */
@RestController
@RequestMapping("/answerOption")
public class AnswerOptionController extends BaseController
{
    @Autowired
    private IAnswerOptionService answerOptionService;

    /**
     * 查询题目选项列表
     */
//    @RequiresPermissions("psedu-exam:answerOption:list")
    @GetMapping("/list")
    public TableDataInfo list(AnswerOption answerOption)
    {
        startPage();
        List<AnswerOption> list = answerOptionService.selectAnswerOptionList(answerOption);
        return getDataTable(list);
    }

    /**
     * 导出题目选项列表
     */
    @RequiresPermissions("psedu-exam:answerOption:export")
    @Log(title = "题目选项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AnswerOption answerOption)
    {
        List<AnswerOption> list = answerOptionService.selectAnswerOptionList(answerOption);
        ExcelUtil<AnswerOption> util = new ExcelUtil<AnswerOption>(AnswerOption.class);
        util.exportExcel(response, list, "题目选项数据");
    }

    /**
     * 获取题目选项详细信息
     */
    @RequiresPermissions("psedu-exam:answerOption:query")
    @GetMapping(value = "/{answerOptionId}")
    public AjaxResult getInfo(@PathVariable("answerOptionId") Long answerOptionId)
    {
        return AjaxResult.success(answerOptionService.selectAnswerOptionByAnswerOptionId(answerOptionId));
    }

    /**
     * 新增题目选项
     */
    @RequiresPermissions("psedu-exam:answerOption:add")
    @Log(title = "题目选项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AnswerOption answerOption)
    {
        return toAjax(answerOptionService.insertAnswerOption(answerOption));
    }

    /**
     * 修改题目选项
     */
    @RequiresPermissions("psedu-exam:answerOption:edit")
    @Log(title = "题目选项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AnswerOption answerOption)
    {
        return toAjax(answerOptionService.updateAnswerOption(answerOption));
    }

    /**
     * 删除题目选项
     */
    @RequiresPermissions("psedu-exam:answerOption:remove")
    @Log(title = "题目选项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{answerOptionIds}")
    public AjaxResult remove(@PathVariable Long[] answerOptionIds)
    {
        return toAjax(answerOptionService.deleteAnswerOptionByAnswerOptionIds(answerOptionIds));
    }
}
