package com.psedu.exam.service;

import java.util.List;
import com.psedu.exam.domain.ExamDeptStatus;
import com.psedu.exam.domain.vo.DeptExamDateUpdateVo;
import com.psedu.exam.domain.vo.DeptExamVo;

/**
 * 分党校考试信息Service接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface IExamDeptStatusService 
{
    /**
     * 查询分党校考试信息
     * 
     * @param examDeptStatusId 分党校考试信息主键
     * @return 分党校考试信息
     */
    public ExamDeptStatus selectExamDeptStatusByExamDeptStatusId(Long examDeptStatusId);

    /**
     * 查询分党校考试信息列表
     * 
     * @param examDeptStatus 分党校考试信息
     * @return 分党校考试信息集合
     */
    public List<ExamDeptStatus> selectExamDeptStatusList(ExamDeptStatus examDeptStatus);

    /**
     * 新增分党校考试信息
     * 
     * @param examDeptStatus 分党校考试信息
     * @return 结果
     */
    public int insertExamDeptStatus(ExamDeptStatus examDeptStatus);

    /**
     * 修改分党校考试信息
     * 
     * @param examDeptStatus 分党校考试信息
     * @return 结果
     */
    public int updateExamDeptStatus(ExamDeptStatus examDeptStatus);

    /**
     * 批量删除分党校考试信息
     * 
     * @param examDeptStatusIds 需要删除的分党校考试信息主键集合
     * @return 结果
     */
    public int deleteExamDeptStatusByExamDeptStatusIds(Long[] examDeptStatusIds);

    /**
     * 删除分党校考试信息信息
     * 
     * @param examDeptStatusId 分党校考试信息主键
     * @return 结果
     */
    public int deleteExamDeptStatusByExamDeptStatusId(Long examDeptStatusId);

    /**
     * 部门考试视图
     * @param examId 考试Id
     * @param deptId 部门ID
     */
    List<DeptExamVo> deptExamView(Long examId, Long deptId);

    int updateStartTime(DeptExamDateUpdateVo deptExamDateUpdateVo);

    ExamDeptStatus nextStatus(Long examDeptStatusId);
}
