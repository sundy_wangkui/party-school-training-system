package com.psedu.exam.mapper;

import java.util.List;
import com.psedu.exam.domain.ExamDeptStatus;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 分党校考试信息Mapper接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface ExamDeptStatusMapper extends BaseMapperPlus<ExamDeptStatusMapper, ExamDeptStatus, ExamDeptStatus>
{

    /**
     * 查询分党校考试信息列表
     * 
     * @param examDeptStatus 分党校考试信息
     * @return 分党校考试信息集合
     */
    public List<ExamDeptStatus> selectExamDeptStatusList(ExamDeptStatus examDeptStatus);
}
