package com.psedu.exam.mapper;

import java.util.List;
import com.psedu.exam.domain.Exam;
import com.psedu.exam.domain.vo.ExamDetailVo;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 考试发起Mapper接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface ExamMapper extends BaseMapperPlus<ExamMapper, Exam, Exam>
{
    /**
     * 查询考试发起
     * 
     * @param examId 考试发起主键
     * @return 考试发起
     */
    public Exam selectExamByExamId(Long examId);

    /**
     * 查询考试发起列表
     * 
     * @param exam 考试发起
     * @return 考试发起集合
     */
    public List<Exam> selectExamList(Exam exam);

    public List<ExamDetailVo> selectStudentExamDetailList(@Param("deptId") Long deptId,
                                                          @Param("passSemesterIds") List<Long> passSemesterIds);

    /**
     * 批量删除考试发起
     * 
     * @param examIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamByExamIds(Long[] examIds);

    List<ExamDetailVo> selectExamDetailList(@Param("deptId") Long deptId);

}
