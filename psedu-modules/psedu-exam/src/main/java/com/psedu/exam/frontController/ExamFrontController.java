package com.psedu.exam.frontController;

import com.psedu.base.api.RemoteStudentService;
import com.psedu.exam.domain.vo.*;
import com.psedu.exam.service.IExamService;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.security.annotation.RequiresLogin;
import com.psedu.common.security.utils.SecurityUtils;
import com.psedu.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/exam")
public class ExamFrontController extends BaseController {

    @Autowired
    private IExamService examService;

    @Autowired
    private RemoteStudentService remoteStudentService;

    /**
     * 查询考试发起列表
     */
    @RequiresLogin()
    @GetMapping("/examDetail")
    public R<List<ExamDetailVo>> examDetail()
    {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long deptId = loginUser.getDeptId();
        List<ExamDetailVo> list = examService.getUserExamDetailList(deptId, loginUser.getUserid());
        return R.ok(list);
    }

    @RequiresLogin()
    @GetMapping("examTest/{trainObject}")
    public R<PaperVo> examTest(@PathVariable("trainObject") Integer trainObject) {
        PaperVo paperVo = examService.getExamTestPaper(trainObject);
        return R.ok(paperVo);
    }

    @RequiresLogin
    @PostMapping("submitTestPaper")
    public R<MarkTestPaperVo> submitTestPaper(@RequestBody ExamTestPaperAnswerVo examTestPaperAnswerVo) {
        MarkTestPaperVo markPaperVo = examService.submitTestPaper(examTestPaperAnswerVo);
        return R.ok(markPaperVo);
    }


    @RequiresLogin
    @PostMapping("/submitExamPaper")
    public R<MarkPaperVo> submitExamPaper(@RequestBody ExamPaperAnswerVo examPaperAnswerVo) {
        Long userId = SecurityUtils.getUserId();
        MarkPaperVo markPaperVo = examService.submitExamPaper(examPaperAnswerVo, userId);
        return R.ok(markPaperVo);
    }

    @RequiresLogin
    @PostMapping("/enterExam/{examDeptStatusId}")
    public R<PaperVo> enterExamAndGetPaper(@PathVariable("examDeptStatusId") Long examDeptStatusId) {
        Long userId = SecurityUtils.getUserId();
        PaperVo paperVo = examService.enterExamAndGetPaper(examDeptStatusId, userId);
        return R.ok(paperVo);
    }
}
