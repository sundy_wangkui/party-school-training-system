package com.psedu.exam.service;

import java.util.List;

import com.psedu.base.api.domain.dto.StudentDTO;
import com.psedu.exam.domain.Exam;
import com.psedu.exam.domain.ExamDeptStatus;
import com.psedu.exam.domain.Paper;
import com.psedu.exam.domain.vo.PaperExam;
import com.psedu.exam.domain.vo.PaperVo;

/**
 * 试卷Service接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface IPaperService 
{
    /**
     * 查询试卷
     * 
     * @param paperId 试卷主键
     * @return 试卷
     */
    public Paper selectPaperByPaperId(Long paperId);

    /**
     * 查询试卷列表
     * 
     * @param paper 试卷
     * @return 试卷集合
     */
    public List<Paper> selectPaperList(Paper paper);

    /**
     * 新增试卷
     * 
     * @param paper 试卷
     * @return 结果
     */
    public int insertPaper(Paper paper);

    /**
     * 修改试卷
     * 
     * @param paper 试卷
     * @return 结果
     */
    public int updatePaper(Paper paper);

    /**
     * 批量删除试卷
     * 
     * @param paperIds 需要删除的试卷主键集合
     * @return 结果
     */
    public int deletePaperByPaperIds(Long[] paperIds);

    /**
     * 删除试卷信息
     * 
     * @param paperId 试卷主键
     * @return 结果
     */
    public int deletePaperByPaperId(Long paperId);

    Paper recordPaperProblem(PaperExam paperExam, PaperVo paperVo);

    PaperVo getPaperVoByPaperId(Paper paper);

    PaperVo createExamPaper(ExamDeptStatus examDeptStatus, Long examId, Exam exam, Long semeId, StudentDTO studentDto);

    void createPaperProblem(PaperVo paperVo, Integer trainObject);
}
