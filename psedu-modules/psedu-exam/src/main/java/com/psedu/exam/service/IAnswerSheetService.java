package com.psedu.exam.service;

import java.util.List;
import com.psedu.exam.domain.AnswerSheet;

/**
 * 答卷Service接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface IAnswerSheetService 
{
    /**
     * 查询答卷
     * 
     * @param answerSheetId 答卷主键
     * @return 答卷
     */
    public AnswerSheet selectAnswerSheetByAnswerSheetId(Long answerSheetId);

    /**
     * 查询答卷列表
     * 
     * @param answerSheet 答卷
     * @return 答卷集合
     */
    public List<AnswerSheet> selectAnswerSheetList(AnswerSheet answerSheet);

    /**
     * 新增答卷
     * 
     * @param answerSheet 答卷
     * @return 结果
     */
    public int insertAnswerSheet(AnswerSheet answerSheet);

    /**
     * 修改答卷
     * 
     * @param answerSheet 答卷
     * @return 结果
     */
    public int updateAnswerSheet(AnswerSheet answerSheet);

    /**
     * 批量删除答卷
     * 
     * @param answerSheetIds 需要删除的答卷主键集合
     * @return 结果
     */
    public int deleteAnswerSheetByAnswerSheetIds(Long[] answerSheetIds);

    /**
     * 删除答卷信息
     * 
     * @param answerSheetId 答卷主键
     * @return 结果
     */
    public int deleteAnswerSheetByAnswerSheetId(Long answerSheetId);
}
