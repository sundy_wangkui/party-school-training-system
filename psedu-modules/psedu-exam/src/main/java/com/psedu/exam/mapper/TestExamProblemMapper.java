package com.psedu.exam.mapper;

import java.util.List;
import com.psedu.exam.domain.TestExamProblem;

/**
 * 导入试题Mapper接口
 * 
 * @author mingyue
 * @date 2022-04-13
 */
public interface TestExamProblemMapper 
{
    /**
     * 查询导入试题
     * 
     * @param problemId 导入试题主键
     * @return 导入试题
     */
    public TestExamProblem selectTestExamProblemByProblemId(Long problemId);

    /**
     * 查询导入试题列表
     * 
     * @param testExamProblem 导入试题
     * @return 导入试题集合
     */
    public List<TestExamProblem> selectTestExamProblemList(TestExamProblem testExamProblem);

    /**
     * 新增导入试题
     * 
     * @param testExamProblem 导入试题
     * @return 结果
     */
    public int insertTestExamProblem(TestExamProblem testExamProblem);

    /**
     * 修改导入试题
     * 
     * @param testExamProblem 导入试题
     * @return 结果
     */
    public int updateTestExamProblem(TestExamProblem testExamProblem);

    /**
     * 删除导入试题
     * 
     * @param problemId 导入试题主键
     * @return 结果
     */
    public int deleteTestExamProblemByProblemId(Long problemId);

    /**
     * 批量删除导入试题
     * 
     * @param problemIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTestExamProblemByProblemIds(Long[] problemIds);
}