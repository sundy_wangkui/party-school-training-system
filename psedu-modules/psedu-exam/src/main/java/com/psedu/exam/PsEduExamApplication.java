package com.psedu.exam;

import com.psedu.common.security.annotation.EnableCustomConfig;
import com.psedu.common.security.annotation.EnableRyFeignClients;
import com.psedu.common.swagger.annotation.EnableCustomSwagger2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringCloudApplication
//@EnableDubbo
@MapperScan("com.psedu.exam.mapper")
@EnableScheduling
public class PsEduExamApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(PsEduExamApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  党校考试模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
