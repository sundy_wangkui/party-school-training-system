package com.psedu.exam.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 答卷对象 ex_answer_sheet
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@TableName(value = "ex_answer_sheet", excludeProperty = {
        "createBy", "updateBy", "createTime", "updateTime"
})
@EqualsAndHashCode(callSuper = true)
@Data
public class AnswerSheet extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 答卷ID */
    @TableId(type = IdType.AUTO)
    private Long answerSheetId;

    /** 报名ID */
    @Excel(name = "报名ID")
    private Long applyId;

    /** 学期ID */
    @Excel(name = "学期ID")
    private Long semeId;

    private Integer officialExam;

    /** 重复次数 */
    @Excel(name = "重复次数")
    private Integer repeatCount;

    /** 试题ID */
    @Excel(name = "试题ID")
    private Long paperId;

    /** 成绩 */
    @Excel(name = "成绩")
    private BigDecimal score;

    /** 单选题分数 */
    @Excel(name = "单选题分数")
    private BigDecimal radioScore;

    /** 多选题分数 */
    @Excel(name = "多选题分数")
    private BigDecimal multipleScore;

    /** 判断题分数 */
    @Excel(name = "判断题分数")
    private BigDecimal judgeScore;

    /** 填空题分数 */
    @Excel(name = "填空题分数")
    private BigDecimal fillScore;

    /** 答卷状态;0未开始，1未提交，2已提交 */
    @Excel(name = "答卷状态;0未开始，1未提交，2已提交")
    private Integer status;

    /** 作答json */
    @Excel(name = "作答json")
    private String answerSheetJson;

}
