package com.psedu.exam.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.psedu.exam.mapper.PaperQuestionMapper;
import com.psedu.exam.domain.PaperQuestion;
import com.psedu.exam.service.IPaperQuestionService;

/**
 * 试卷题目Service业务层处理
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@Service
public class PaperQuestionServiceImpl implements IPaperQuestionService 
{
    @Autowired
    private PaperQuestionMapper paperQuestionMapper;

    /**
     * 查询试卷题目
     * 
     * @param paperQuestionId 试卷题目主键
     * @return 试卷题目
     */
    @Override
    public PaperQuestion selectPaperQuestionByPaperQuestionId(Long paperQuestionId)
    {
        return paperQuestionMapper.selectPaperQuestionByPaperQuestionId(paperQuestionId);
    }

    /**
     * 查询试卷题目列表
     * 
     * @param paperQuestion 试卷题目
     * @return 试卷题目
     */
    @Override
    public List<PaperQuestion> selectPaperQuestionList(PaperQuestion paperQuestion)
    {
        return paperQuestionMapper.selectPaperQuestionList(paperQuestion);
    }

    /**
     * 新增试卷题目
     * 
     * @param paperQuestion 试卷题目
     * @return 结果
     */
    @Override
    public int insertPaperQuestion(PaperQuestion paperQuestion)
    {
        return paperQuestionMapper.insertPaperQuestion(paperQuestion);
    }

    /**
     * 修改试卷题目
     * 
     * @param paperQuestion 试卷题目
     * @return 结果
     */
    @Override
    public int updatePaperQuestion(PaperQuestion paperQuestion)
    {
        return paperQuestionMapper.updatePaperQuestion(paperQuestion);
    }

    /**
     * 批量删除试卷题目
     * 
     * @param paperQuestionIds 需要删除的试卷题目主键
     * @return 结果
     */
    @Override
    public int deletePaperQuestionByPaperQuestionIds(Long[] paperQuestionIds)
    {
        return paperQuestionMapper.deletePaperQuestionByPaperQuestionIds(paperQuestionIds);
    }

    /**
     * 删除试卷题目信息
     * 
     * @param paperQuestionId 试卷题目主键
     * @return 结果
     */
    @Override
    public int deletePaperQuestionByPaperQuestionId(Long paperQuestionId)
    {
        return paperQuestionMapper.deletePaperQuestionByPaperQuestionId(paperQuestionId);
    }
}
