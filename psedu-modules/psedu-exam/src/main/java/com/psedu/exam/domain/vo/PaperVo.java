package com.psedu.exam.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class PaperVo {
    private String examName;
    private Date startTime;
    private Date endTime;
    private List<QuestionDetail> singleQuestionDetailList;
    private List<QuestionDetail> judgeQuestionDetailList;
    private List<QuestionDetail> multiQuestionDetailList;

    @JsonIgnore
    public List<QuestionDetail> getAllQuestionDetail() {
        List<QuestionDetail> questionDetailList = new ArrayList<>();
        questionDetailList.addAll(singleQuestionDetailList);
        questionDetailList.addAll(judgeQuestionDetailList);
        questionDetailList.addAll(multiQuestionDetailList);
        return questionDetailList;
    }
}
