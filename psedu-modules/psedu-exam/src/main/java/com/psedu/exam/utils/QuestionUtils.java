package com.psedu.exam.utils;

import com.psedu.exam.domain.AnswerOption;
import com.psedu.exam.domain.vo.QuestionDetail;
import com.psedu.exam.domain.vo.QuestionVo;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class QuestionUtils {

    /**
     * 转换数据库 questionVo 对象到 QuestionDetail ，取出多余属性
     * @param questions QuestionVo
     * @return QuestionDetail List
     */
    public static List<QuestionDetail> convertQuestionDetail(List<QuestionVo> questions) {
        return questions.stream().map(questionVo -> {
            QuestionDetail questionDetail = new QuestionDetail();
            BeanUtils.copyProperties(questionVo, questionDetail);
            List<AnswerOption> answerOptions = questionVo.getAnswerOptions();
            List<QuestionDetail.AnswerOption> simpleAnswerOptions = answerOptions.stream().map(answerOption -> {
                QuestionDetail.AnswerOption simpleAnswerOption = new QuestionDetail.AnswerOption();
                BeanUtils.copyProperties(answerOption, simpleAnswerOption);
                return simpleAnswerOption;
            }).collect(Collectors.toList());
            questionDetail.setOptionList(simpleAnswerOptions);
            return questionDetail;
        }).collect(Collectors.toList());
    }
}
