package com.psedu.exam.service;

import java.util.List;
import com.psedu.exam.domain.AnswerOption;

/**
 * 题目选项Service接口
 * 
 * @author mingyue
 * @date 2022-03-29
 */
public interface IAnswerOptionService 
{
    /**
     * 查询题目选项
     * 
     * @param answerOptionId 题目选项主键
     * @return 题目选项
     */
    public AnswerOption selectAnswerOptionByAnswerOptionId(Long answerOptionId);

    /**
     * 查询题目选项列表
     * 
     * @param answerOption 题目选项
     * @return 题目选项集合
     */
    public List<AnswerOption> selectAnswerOptionList(AnswerOption answerOption);

    /**
     * 新增题目选项
     * 
     * @param answerOption 题目选项
     * @return 结果
     */
    public int insertAnswerOption(AnswerOption answerOption);

    /**
     * 修改题目选项
     * 
     * @param answerOption 题目选项
     * @return 结果
     */
    public int updateAnswerOption(AnswerOption answerOption);

    /**
     * 批量删除题目选项
     * 
     * @param answerOptionIds 需要删除的题目选项主键集合
     * @return 结果
     */
    public int deleteAnswerOptionByAnswerOptionIds(Long[] answerOptionIds);

    /**
     * 删除题目选项信息
     * 
     * @param answerOptionId 题目选项主键
     * @return 结果
     */
    public int deleteAnswerOptionByAnswerOptionId(Long answerOptionId);
}
