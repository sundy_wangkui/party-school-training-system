package com.psedu.exam.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class ExamPaperAnswerVo {
    private Long examDeptStatusId;
    private List<AnswerQuestion> single;
    private List<AnswerQuestion> judge;
    private List<AnswerQuestion> multi;
}
