package com.psedu.exam.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 试卷对象 ex_paper
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@TableName(value = "ex_paper", excludeProperty = {
        "createBy", "updateBy", "createTime", "updateTime"
})
@EqualsAndHashCode(callSuper = true)
@Data
public class Paper extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 试卷ID */
    @TableId(type = IdType.AUTO)
    private Long paperId;

    /** 考试ID */
    @Excel(name = "考试ID")
    private Long examId;

    /** 单选数量 */
    @Excel(name = "单选数量")
    private Long radioCount;

    /** 多选题数量 */
    @Excel(name = "多选题数量")
    private Long multipleCount;

    /** 判断题数量 */
    @Excel(name = "判断题数量")
    private Long judgeCount;

    /** 填空题数量 */
    @Excel(name = "填空题数量")
    private Long fillCount;

    /** 简答题数量 */
    @Excel(name = "简答题数量")
    private Long saqCount;

    /** 当前重复次数 */
    @Excel(name = "当前重复次数")
    private Integer repeatCount;

}
