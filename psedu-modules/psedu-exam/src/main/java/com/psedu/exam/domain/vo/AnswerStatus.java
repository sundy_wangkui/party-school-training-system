package com.psedu.exam.domain.vo;

import lombok.Data;

@Data
public class AnswerStatus {
    private Long questionId;
    private Boolean answerCorrect;
}