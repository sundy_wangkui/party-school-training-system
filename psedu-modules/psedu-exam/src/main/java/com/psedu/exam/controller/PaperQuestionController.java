package com.psedu.exam.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.exam.domain.PaperQuestion;
import com.psedu.exam.service.IPaperQuestionService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 试卷题目Controller
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@RestController
@RequestMapping("/paperQuestion")
public class PaperQuestionController extends BaseController
{
    @Autowired
    private IPaperQuestionService paperQuestionService;

    /**
     * 查询试卷题目列表
     */
    @RequiresPermissions("psedu-exam:paperQuestion:list")
    @GetMapping("/list")
    public TableDataInfo list(PaperQuestion paperQuestion)
    {
        startPage();
        List<PaperQuestion> list = paperQuestionService.selectPaperQuestionList(paperQuestion);
        return getDataTable(list);
    }

    /**
     * 导出试卷题目列表
     */
    @RequiresPermissions("psedu-exam:paperQuestion:export")
    @Log(title = "试卷题目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PaperQuestion paperQuestion)
    {
        List<PaperQuestion> list = paperQuestionService.selectPaperQuestionList(paperQuestion);
        ExcelUtil<PaperQuestion> util = new ExcelUtil<PaperQuestion>(PaperQuestion.class);
        util.exportExcel(response, list, "试卷题目数据");
    }

    /**
     * 获取试卷题目详细信息
     */
    @RequiresPermissions("psedu-exam:paperQuestion:query")
    @GetMapping(value = "/{paperQuestionId}")
    public AjaxResult getInfo(@PathVariable("paperQuestionId") Long paperQuestionId)
    {
        return AjaxResult.success(paperQuestionService.selectPaperQuestionByPaperQuestionId(paperQuestionId));
    }

    /**
     * 新增试卷题目
     */
    @RequiresPermissions("psedu-exam:paperQuestion:add")
    @Log(title = "试卷题目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PaperQuestion paperQuestion)
    {
        return toAjax(paperQuestionService.insertPaperQuestion(paperQuestion));
    }

    /**
     * 修改试卷题目
     */
    @RequiresPermissions("psedu-exam:paperQuestion:edit")
    @Log(title = "试卷题目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PaperQuestion paperQuestion)
    {
        return toAjax(paperQuestionService.updatePaperQuestion(paperQuestion));
    }

    /**
     * 删除试卷题目
     */
    @RequiresPermissions("psedu-exam:paperQuestion:remove")
    @Log(title = "试卷题目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{paperQuestionIds}")
    public AjaxResult remove(@PathVariable Long[] paperQuestionIds)
    {
        return toAjax(paperQuestionService.deletePaperQuestionByPaperQuestionIds(paperQuestionIds));
    }
}
