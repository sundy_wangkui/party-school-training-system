package com.psedu.exam.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.exam.domain.Paper;
import com.psedu.exam.service.IPaperService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 试卷Controller
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@RestController
@RequestMapping("/paper")
public class PaperController extends BaseController
{
    @Autowired
    private IPaperService paperService;

    /**
     * 查询试卷列表
     */
    @RequiresPermissions("psedu-exam:paper:list")
    @GetMapping("/list")
    public TableDataInfo list(Paper paper)
    {
        startPage();
        List<Paper> list = paperService.selectPaperList(paper);
        return getDataTable(list);
    }

    /**
     * 导出试卷列表
     */
    @RequiresPermissions("psedu-exam:paper:export")
    @Log(title = "试卷", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Paper paper)
    {
        List<Paper> list = paperService.selectPaperList(paper);
        ExcelUtil<Paper> util = new ExcelUtil<Paper>(Paper.class);
        util.exportExcel(response, list, "试卷数据");
    }

    /**
     * 获取试卷详细信息
     */
    @RequiresPermissions("psedu-exam:paper:query")
    @GetMapping(value = "/{paperId}")
    public AjaxResult getInfo(@PathVariable("paperId") Long paperId)
    {
        return AjaxResult.success(paperService.selectPaperByPaperId(paperId));
    }

    /**
     * 新增试卷
     */
    @RequiresPermissions("psedu-exam:paper:add")
    @Log(title = "试卷", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Paper paper)
    {
        return toAjax(paperService.insertPaper(paper));
    }

    /**
     * 修改试卷
     */
    @RequiresPermissions("psedu-exam:paper:edit")
    @Log(title = "试卷", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Paper paper)
    {
        return toAjax(paperService.updatePaper(paper));
    }

    /**
     * 删除试卷
     */
    @RequiresPermissions("psedu-exam:paper:remove")
    @Log(title = "试卷", businessType = BusinessType.DELETE)
	@DeleteMapping("/{paperIds}")
    public AjaxResult remove(@PathVariable Long[] paperIds)
    {
        return toAjax(paperService.deletePaperByPaperIds(paperIds));
    }
}
