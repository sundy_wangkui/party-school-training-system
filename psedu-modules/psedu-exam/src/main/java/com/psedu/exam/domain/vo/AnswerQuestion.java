package com.psedu.exam.domain.vo;

import lombok.Data;

import java.util.List;

/**
 * @author Mingyue
 */
@Data
public class AnswerQuestion {
    private Long questionId;
    private List<Long> optionIdList;
}
