package com.psedu.exam.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.exam.domain.AnswerSheet;
import com.psedu.exam.service.IAnswerSheetService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 答卷Controller
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@RestController
@RequestMapping("/answerSheet")
public class AnswerSheetController extends BaseController
{
    @Autowired
    private IAnswerSheetService answerSheetService;

    /**
     * 查询答卷列表
     */
    @RequiresPermissions("psedu-exam:answerSheet:list")
    @GetMapping("/list")
    public TableDataInfo list(AnswerSheet answerSheet)
    {
        startPage();
        List<AnswerSheet> list = answerSheetService.selectAnswerSheetList(answerSheet);
        return getDataTable(list);
    }

    /**
     * 导出答卷列表
     */
    @RequiresPermissions("psedu-exam:answerSheet:export")
    @Log(title = "答卷", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AnswerSheet answerSheet)
    {
        List<AnswerSheet> list = answerSheetService.selectAnswerSheetList(answerSheet);
        ExcelUtil<AnswerSheet> util = new ExcelUtil<AnswerSheet>(AnswerSheet.class);
        util.exportExcel(response, list, "答卷数据");
    }

    /**
     * 获取答卷详细信息
     */
    @RequiresPermissions("psedu-exam:answerSheet:query")
    @GetMapping(value = "/{answerSheetId}")
    public AjaxResult getInfo(@PathVariable("answerSheetId") Long answerSheetId)
    {
        return AjaxResult.success(answerSheetService.selectAnswerSheetByAnswerSheetId(answerSheetId));
    }

    /**
     * 新增答卷
     */
    @RequiresPermissions("psedu-exam:answerSheet:add")
    @Log(title = "答卷", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AnswerSheet answerSheet)
    {
        return toAjax(answerSheetService.insertAnswerSheet(answerSheet));
    }

    /**
     * 修改答卷
     */
    @RequiresPermissions("psedu-exam:answerSheet:edit")
    @Log(title = "答卷", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AnswerSheet answerSheet)
    {
        return toAjax(answerSheetService.updateAnswerSheet(answerSheet));
    }

    /**
     * 删除答卷
     */
    @RequiresPermissions("psedu-exam:answerSheet:remove")
    @Log(title = "答卷", businessType = BusinessType.DELETE)
	@DeleteMapping("/{answerSheetIds}")
    public AjaxResult remove(@PathVariable Long[] answerSheetIds)
    {
        return toAjax(answerSheetService.deleteAnswerSheetByAnswerSheetIds(answerSheetIds));
    }
}
