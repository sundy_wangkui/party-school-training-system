package com.psedu.exam.domain.vo;

import lombok.Data;

@Data
public class MarkPaperVo {
    private Boolean examPassed;
    private Integer score;
}
