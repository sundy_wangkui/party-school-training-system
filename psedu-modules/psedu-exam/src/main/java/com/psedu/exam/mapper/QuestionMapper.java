package com.psedu.exam.mapper;

import java.util.List;
import com.psedu.exam.domain.Question;
import com.psedu.exam.domain.vo.QuestionVo;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 候选题目Mapper接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface QuestionMapper extends BaseMapperPlus<QuestionMapper, Question, Question>
{
    /**
     * 查询候选题目
     * 
     * @param questionId 候选题目主键
     * @return 候选题目
     */
    public Question selectQuestionByQuestionId(Long questionId);

    /**
     * 查询候选题目列表
     * 
     * @param question 候选题目
     * @return 候选题目集合
     */
    public List<Question> selectQuestionList(Question question);

    /**
     * 新增候选题目
     * 
     * @param question 候选题目
     * @return 结果
     */
    public int insertQuestion(Question question);

    /**
     * 修改候选题目
     * 
     * @param question 候选题目
     * @return 结果
     */
    public int updateQuestion(Question question);

    /**
     * 删除候选题目
     * 
     * @param questionId 候选题目主键
     * @return 结果
     */
    public int deleteQuestionByQuestionId(Long questionId);

    /**
     * 批量删除候选题目
     * 
     * @param questionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteQuestionByQuestionIds(Long[] questionIds);

    /**
     * 获取题目详细
     * @param questionId 题目ID
     * @return 题目和选项信息
     */
    QuestionVo selectDetailById(@Param("questionId") Long questionId);

    Question selectOneRandomQuestion(@Param("trainObject") Integer trainObject);

    List<QuestionVo> selectRandomQuestionDetail(@Param("trainObject") Integer trainObject,
                                                @Param("questionCount") Integer questionCount,
                                                @Param("type") Integer type);

    List<QuestionVo> selectQuestionDetail(@Param("questionIdList") List<Long> questionIdList);
}
