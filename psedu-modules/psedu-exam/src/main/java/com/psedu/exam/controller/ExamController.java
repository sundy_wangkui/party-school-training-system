package com.psedu.exam.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.psedu.exam.api.domain.vo.ExamBo;
import com.psedu.exam.api.domain.vo.SimpleExamVo;
import com.psedu.common.core.domain.R;
import com.psedu.common.security.annotation.InnerAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.exam.domain.Exam;
import com.psedu.exam.service.IExamService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 考试发起Controller
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@RestController
@RequestMapping("/exam")
public class ExamController extends BaseController
{
    @Autowired
    private IExamService examService;

    @GetMapping("/getExamSelectOptions")
    public R<List<SimpleExamVo>> getExamSelectOptions() {
        return R.ok(examService.getExamSelectOptions());
    }

    /**
     * 查询考试发起列表
     */
    @RequiresPermissions("psedu-exam:exam:list")
    @GetMapping("/list")
    public TableDataInfo list(Exam exam)
    {
        startPage();
        List<Exam> list = examService.selectExamList(exam);
        return getDataTable(list);
    }

    @InnerAuth
    @GetMapping("/getExamsBySemeIds/{deptId}/{semeIds}")
    public R<List<ExamBo>> getExamsBySemeIdsAndDeptId(@PathVariable("deptId") Long deptId,
                                                      @PathVariable("semeIds") List<Long> semeIds) {
        return R.ok(examService.getExamsBySemeIds(deptId, semeIds));
    }

    /**
     * 导出考试发起列表
     */
    @RequiresPermissions("psedu-exam:exam:export")
    @Log(title = "考试发起", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Exam exam)
    {
        List<Exam> list = examService.selectExamList(exam);
        ExcelUtil<Exam> util = new ExcelUtil<Exam>(Exam.class);
        util.exportExcel(response, list, "考试发起数据");
    }

    /**
     * 获取考试发起详细信息
     */
    @RequiresPermissions("psedu-exam:exam:query")
    @GetMapping(value = "/{examId}")
    public AjaxResult getInfo(@PathVariable("examId") Long examId)
    {
        return AjaxResult.success(examService.selectExamByExamId(examId));
    }

    /**
     * 新增考试发起
     */
    @RequiresPermissions("psedu-exam:exam:add")
    @Log(title = "考试发起", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult insertExamAndDeptStatus(@RequestBody Exam exam)
    {
        return toAjax(examService.insertExamAndDeptStatus(exam));
    }

    /**
     * 修改考试发起
     */
    @RequiresPermissions("psedu-exam:exam:edit")
    @Log(title = "考试发起", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Exam exam)
    {
        return toAjax(examService.updateExam(exam));
    }

    /**
     * 修改考试允许开启状态
     */
    @RequiresPermissions("psedu-exam:exam:edit")
    @Log(title = "考试允许开启", businessType = BusinessType.UPDATE)
    @PutMapping("/allowStart/{examId}/{newStatus}")
    public AjaxResult updateStatus(@PathVariable("examId") Long examId, @PathVariable("newStatus") Integer status)
    {
        return toAjax(examService.updateExamStatus(examId, status));
    }

    /**
     * 删除考试发起
     */
    @RequiresPermissions("psedu-exam:exam:remove")
    @Log(title = "考试发起", businessType = BusinessType.DELETE)
	@DeleteMapping("/{examIds}")
    public AjaxResult remove(@PathVariable Long[] examIds)
    {
        return toAjax(examService.deleteExamByExamIds(examIds));
    }
}
