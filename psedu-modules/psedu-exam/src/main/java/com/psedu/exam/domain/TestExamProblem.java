package com.psedu.exam.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 导入试题对象 psedu_exam_problem
 * 
 * @author mingyue
 * @date 2022-04-13
 */
public class TestExamProblem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long problemId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String description;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String answer;

    /** 单选 多选 */
    @Excel(name = "单选 多选")
    private String type;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String a;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String b;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String c;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String d;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String e;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String f;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String g;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer suitAct;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer suitDev;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer suitPre;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer status;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long correct;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long submit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date time;

    public void setProblemId(Long problemId) 
    {
        this.problemId = problemId;
    }

    public Long getProblemId() 
    {
        return problemId;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setAnswer(String answer) 
    {
        this.answer = answer;
    }

    public String getAnswer() 
    {
        return answer;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setA(String a) 
    {
        this.a = a;
    }

    public String getA() 
    {
        return a;
    }
    public void setB(String b) 
    {
        this.b = b;
    }

    public String getB() 
    {
        return b;
    }
    public void setC(String c) 
    {
        this.c = c;
    }

    public String getC() 
    {
        return c;
    }
    public void setD(String d) 
    {
        this.d = d;
    }

    public String getD() 
    {
        return d;
    }
    public void setE(String e) 
    {
        this.e = e;
    }

    public String getE() 
    {
        return e;
    }
    public void setF(String f) 
    {
        this.f = f;
    }

    public String getF() 
    {
        return f;
    }
    public void setG(String g) 
    {
        this.g = g;
    }

    public String getG() 
    {
        return g;
    }
    public void setSuitAct(Integer suitAct) 
    {
        this.suitAct = suitAct;
    }

    public Integer getSuitAct() 
    {
        return suitAct;
    }
    public void setSuitDev(Integer suitDev) 
    {
        this.suitDev = suitDev;
    }

    public Integer getSuitDev() 
    {
        return suitDev;
    }
    public void setSuitPre(Integer suitPre) 
    {
        this.suitPre = suitPre;
    }

    public Integer getSuitPre() 
    {
        return suitPre;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setCorrect(Long correct) 
    {
        this.correct = correct;
    }

    public Long getCorrect() 
    {
        return correct;
    }
    public void setSubmit(Long submit) 
    {
        this.submit = submit;
    }

    public Long getSubmit() 
    {
        return submit;
    }
    public void setTime(Date time) 
    {
        this.time = time;
    }

    public Date getTime() 
    {
        return time;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("problemId", getProblemId())
            .append("description", getDescription())
            .append("answer", getAnswer())
            .append("type", getType())
            .append("a", getA())
            .append("b", getB())
            .append("c", getC())
            .append("d", getD())
            .append("e", getE())
            .append("f", getF())
            .append("g", getG())
            .append("suitAct", getSuitAct())
            .append("suitDev", getSuitDev())
            .append("suitPre", getSuitPre())
            .append("status", getStatus())
            .append("correct", getCorrect())
            .append("submit", getSubmit())
            .append("time", getTime())
            .toString();
    }
}