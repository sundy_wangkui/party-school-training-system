package com.psedu.exam.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.psedu.exam.domain.ExamDeptStatus;
import com.psedu.common.core.annotation.Excel;
import lombok.Data;

import java.util.List;

/**
 * 一个部门考试框，有所有本次几轮考试的列表
 * @author Mingyue
 */
@Data
public class DeptExamVo {

    private Long deptId;

    private String deptName;

    private Boolean underway;

    /** 总时间 */
    private Long totalTime;

    private List<ExamDeptStatus> examDeptStatusList;
}
