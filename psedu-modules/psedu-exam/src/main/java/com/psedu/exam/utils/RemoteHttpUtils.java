package com.psedu.exam.utils;

import com.psedu.common.core.constant.HttpStatus;
import com.psedu.common.core.domain.R;
import org.apache.poi.ss.formula.functions.T;

public class RemoteHttpUtils {

    public static T getResponseData(R<T> httpResponse) {
        int code = httpResponse.getCode();
        if(HttpStatus.SUCCESS != code) {
            return null;
        }
        return httpResponse.getData();
    }
}
