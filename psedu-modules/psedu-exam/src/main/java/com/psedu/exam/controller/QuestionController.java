package com.psedu.exam.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.psedu.exam.domain.vo.AnswerQuestion;
import com.psedu.exam.domain.vo.QuestionDetail;
import com.psedu.exam.domain.vo.QuestionVo;
import com.psedu.common.core.domain.R;
import com.psedu.common.security.annotation.RequiresLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.exam.domain.Question;
import com.psedu.exam.service.IQuestionService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 候选题目Controller
 * 
 * @author mingyue
 * @date 2022-03-30
 */
@RestController
@RequestMapping("/question")
public class QuestionController extends BaseController
{
    @Autowired
    private IQuestionService questionService;

    @RequiresLogin
    @GetMapping("/oneRandomQuestion/{trainObject}")
    public R<QuestionDetail> oneRandomQuestion(@PathVariable Integer trainObject) {
        return R.ok(questionService.getOneRandomQuestionDetail(trainObject));
    }

    @RequiresLogin
    @PostMapping("/checkAnswer")
    public R<Boolean> checkAnswer(@RequestBody AnswerQuestion answerQuestion) {
        return R.ok(questionService.checkAnswer(answerQuestion));
    }


    /**
     * 查询候选题目列表
     */
    @RequiresPermissions("psedu-exam:question:list")
    @GetMapping("/list")
    public TableDataInfo list(Question question)
    {
        startPage();
        List<Question> list = questionService.selectQuestionList(question);
        return getDataTable(list);
    }

    /**
     * 导出候选题目列表
     */
    @RequiresPermissions("psedu-exam:question:export")
    @Log(title = "候选题目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Question question)
    {
        List<Question> list = questionService.selectQuestionList(question);
        ExcelUtil<Question> util = new ExcelUtil<Question>(Question.class);
        util.exportExcel(response, list, "候选题目数据");
    }

    /**
     * 获取候选题目详细信息
     */
    @RequiresPermissions("psedu-exam:question:query")
    @GetMapping(value = "/{questionId}")
    public AjaxResult getInfo(@PathVariable("questionId") Long questionId)
    {
        return AjaxResult.success(questionService.getQuestionDetail(questionId));
    }

    /**
     * 新增候选题目
     */
    @RequiresPermissions("psedu-exam:question:add")
    @Log(title = "候选题目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody QuestionVo questionVo)
    {
        return toAjax(questionService.insertQuestionAndAnswer(questionVo));
    }


    /**
     * 修改候选题目
     */
    // TODO 完成
    @RequiresPermissions("psedu-exam:question:edit")
    @Log(title = "候选题目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody QuestionVo questionVo)
    {
        return toAjax(questionService.updateQuestionAndAnswer(questionVo));
    }

    /**
     * 删除候选题目
     */
    @RequiresPermissions("psedu-exam:question:remove")
    @Log(title = "候选题目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{questionIds}")
    public AjaxResult remove(@PathVariable Long[] questionIds)
    {
        return toAjax(questionService.deleteQuestionByQuestionIds(questionIds));
    }
}
