package com.psedu.exam.mapper;

import java.util.List;
import com.psedu.exam.domain.Paper;
import com.psedu.exam.domain.Question;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 试卷Mapper接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface PaperMapper extends BaseMapperPlus<PaperMapper, Paper, Paper>
{
    /**
     * 查询试卷
     * 
     * @param paperId 试卷主键
     * @return 试卷
     */
    public Paper selectPaperByPaperId(Long paperId);

    /**
     * 查询试卷列表
     * 
     * @param paper 试卷
     * @return 试卷集合
     */
    public List<Paper> selectPaperList(Paper paper);

    /**
     * 新增试卷
     * 
     * @param paper 试卷
     * @return 结果
     */
    public int insertPaper(Paper paper);

    /**
     * 修改试卷
     * 
     * @param paper 试卷
     * @return 结果
     */
    public int updatePaper(Paper paper);

    /**
     * 删除试卷
     * 
     * @param paperId 试卷主键
     * @return 结果
     */
    public int deletePaperByPaperId(Long paperId);

    /**
     * 批量删除试卷
     * 
     * @param paperIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePaperByPaperIds(Long[] paperIds);
}
