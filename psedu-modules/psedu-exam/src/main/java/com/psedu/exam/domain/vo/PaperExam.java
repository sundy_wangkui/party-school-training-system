package com.psedu.exam.domain.vo;

import lombok.Data;

@Data
public class PaperExam {
    private Long examId;
    private Integer repeatCount;
}
