package com.psedu.exam.service;

import java.util.List;

import com.psedu.exam.api.domain.vo.ExamBo;
import com.psedu.exam.api.domain.vo.SimpleExamVo;
import com.psedu.exam.domain.Exam;
import com.psedu.exam.domain.vo.*;

/**
 * 考试发起Service接口
 * 
 * @author mingyue
 * @date 2022-03-30
 */
public interface IExamService 
{
    /**
     * 查询考试发起
     * 
     * @param examId 考试发起主键
     * @return 考试发起
     */
    public Exam selectExamByExamId(Long examId);

    /**
     * 查询考试发起列表
     * 
     * @param exam 考试发起
     * @return 考试发起集合
     */
    public List<Exam> selectExamList(Exam exam);

    /**
     * 新增考试发起
     * 
     * @param exam 考试发起
     * @return 结果
     */
    public boolean insertExamAndDeptStatus(Exam exam);

    /**
     * 修改考试发起
     * 
     * @param exam 考试发起
     * @return 结果
     */
    public int updateExam(Exam exam);

    /**
     * 批量删除考试发起
     * 
     * @param examIds 需要删除的考试发起主键集合
     * @return 结果
     */
    public int deleteExamByExamIds(Long[] examIds);

    /**
     * 删除考试发起信息
     * 
     * @param examId 考试发起主键
     * @return 结果
     */
    public int deleteExamByExamId(Long examId);

    /**
     * 更改考试状态
     * @param examId 考试ID
     * @param status 新考试状态
     * @return 更改数量
     */
    int updateExamStatus(Long examId, Integer status);

    /**
     * 通过学期Ids与部门获取考试信息列表
     * @param semeIds 学期Ids
     * @return 考试列表
     */
    List<ExamBo> getExamsBySemeIds(Long deptId, List<Long> semeIds);

    List<SimpleExamVo> getExamSelectOptions();

    List<ExamDetailVo> getExamDetailList(Long deptId);

    PaperVo getExamTestPaper(Integer trainObject);

    MarkTestPaperVo submitTestPaper(ExamTestPaperAnswerVo examTestPaperAnswerVo);

    PaperVo enterExamAndGetPaper(Long examDeptStatusId, Long userId);

    MarkPaperVo submitExamPaper(ExamPaperAnswerVo examPaperAnswerVo, Long userId);

    List<ExamDetailVo> getUserExamDetailList(Long deptId, Long userId);
}
