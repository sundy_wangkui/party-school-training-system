package com.psedu.exam;

import com.psedu.base.api.RemoteSemesterService;
import com.psedu.base.api.domain.dto.SemesterTrainObjectDTO;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.domain.R;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RemoteSemesterServiceTest {

    @Autowired
    RemoteSemesterService remoteSemesterService;

    @Test
    public void testGetTrainObjectBySemesterId() {
        R<SemesterTrainObjectDTO> semesterTrainObjectDTO = remoteSemesterService.getTrainObjectBySemesterId(1L, SecurityConstants.INNER);
        System.out.println(semesterTrainObjectDTO);
        SemesterTrainObjectDTO data = semesterTrainObjectDTO.getData();
        System.out.println(data);
    }
}
