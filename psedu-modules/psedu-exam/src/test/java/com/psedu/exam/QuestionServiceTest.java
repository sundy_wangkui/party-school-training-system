package com.psedu.exam;

import com.psedu.exam.domain.Question;
import com.psedu.exam.domain.vo.QuestionVo;
import com.psedu.exam.mapper.QuestionMapper;
import com.psedu.exam.service.IQuestionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class QuestionServiceTest {
    @Autowired
    private IQuestionService questionService;

    @Autowired
    private QuestionMapper questionMapper;

    @Test
    public void testGetDetail() {
        System.out.println(questionService.getQuestionDetail(2L));
    }

    @Test
    public void testGetOneRandomQuestion() {
        System.out.println(questionService.getOneRandomQuestionDetail(0));
        System.out.println(questionService.getOneRandomQuestionDetail(1));
        System.out.println(questionService.getOneRandomQuestionDetail(2));
    }

    @Test
    public void testSelectQuestionDetail() {
        List<QuestionVo> questionVos = questionMapper.selectQuestionDetail(Arrays.asList(2L, 3L, 4L, 5L));
        System.out.println(questionVos);
    }
}
