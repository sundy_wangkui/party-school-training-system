package com.psedu.exam;

import com.psedu.exam.constant.Constants;
import com.psedu.exam.domain.vo.PaperVo;
import com.psedu.exam.service.IExamService;
import com.psedu.exam.service.impl.ExamServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ExamServiceTest {

    @Autowired
    private IExamService examService;

    @Test
    public void testGetExamTestPaper() {
        examService.getExamTestPaper(Constants.TRAIN_OBJECT_ACTIVE);
    }

    @Test
    public void testEnterExamAndGetPaper() {
        PaperVo paperVo = examService.enterExamAndGetPaper(146L, 104L);
        assert paperVo != null;
    }
}
