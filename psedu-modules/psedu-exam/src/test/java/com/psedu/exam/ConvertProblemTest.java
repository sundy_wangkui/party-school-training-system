package com.psedu.exam;

import com.psedu.exam.domain.AnswerOption;
import com.psedu.exam.domain.Question;
import com.psedu.exam.domain.TestExamProblem;
import com.psedu.exam.mapper.AnswerOptionMapper;
import com.psedu.exam.mapper.QuestionMapper;
import com.psedu.exam.mapper.TestExamProblemMapper;
import com.psedu.exam.service.IQuestionService;
import com.psedu.common.core.utils.BeanCopyUtils;
import com.psedu.common.core.utils.StringUtils;
import lombok.Data;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
public class ConvertProblemTest {
    @Autowired
    private IQuestionService questionService;

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private AnswerOptionMapper answerOptionMapper;

    @Autowired
    private TestExamProblemMapper testExamProblemMapper;

    private int sort = 0;

    @Disabled
    @Test
    public void convertProblem() {
        List<TestExamProblem> testExamProblems = testExamProblemMapper.selectTestExamProblemList(null);
        for (TestExamProblem testExamProblem : testExamProblems) {
            Question question = new Question();
            question.setContent(testExamProblem.getDescription());
            convertType(testExamProblem, question);
            convertSuit(testExamProblem, question);
            questionMapper.insert(question);
            List<SimpleAnswerOption> simpleAnswerOptions = new ArrayList<>();
            sort = 0;
            List<AnswerOption> answerOptions = getAnswerOptions(testExamProblem, question.getQuestionId(), simpleAnswerOptions);
            boolean b = answerOptionMapper.insertBatch(answerOptions);
            System.out.println(b);
        }

    }

    private List<AnswerOption> getAnswerOptions(TestExamProblem testExamProblem, Long questionId, List<SimpleAnswerOption> simpleAnswerOptions) {
        String answer = testExamProblem.getAnswer();
        String a = testExamProblem.getA();
        String b = testExamProblem.getB();
        String c = testExamProblem.getC();
        String d = testExamProblem.getD();
        String e = testExamProblem.getE();
        String f = testExamProblem.getF();
        String g = testExamProblem.getG();
        fillOptions(simpleAnswerOptions, answer, a, "A");
        fillOptions(simpleAnswerOptions, answer, b, "B");
        fillOptions(simpleAnswerOptions, answer, c, "C");
        fillOptions(simpleAnswerOptions, answer, d, "D");
        fillOptions(simpleAnswerOptions, answer, e, "E");
        fillOptions(simpleAnswerOptions, answer, f, "F");
        fillOptions(simpleAnswerOptions, answer, g, "G");
        List<AnswerOption> answerOptions = simpleAnswerOptions.stream().map(simpleAnswerOption -> {
            AnswerOption answerOption = new AnswerOption();
            answerOption.setQuestionId(questionId);
            answerOption.setIsCorrect(simpleAnswerOption.getIsCorrect());
            answerOption.setContent(simpleAnswerOption.getContent());
            answerOption.setSort(simpleAnswerOption.getSort());
            return answerOption;
        }).collect(Collectors.toList());
        return answerOptions;
    }

    private void fillOptions(List<SimpleAnswerOption> simpleAnswerOptions, String answer, String content, String optionA) {
        if(!StringUtils.isEmpty(content)) {
            sort++;
            SimpleAnswerOption simpleAnswerOption = new SimpleAnswerOption();
            simpleAnswerOptions.add(simpleAnswerOption);
            simpleAnswerOption.setContent(content);
            simpleAnswerOption.setSort(sort);
            simpleAnswerOption.setIsCorrect(0);
            if(answer.contains(optionA)) {
                simpleAnswerOption.setIsCorrect(1);
            }
        }
    }

    // 培训对象
    private void convertSuit(TestExamProblem testExamProblem, Question question) {
        question.setSuitAct(0);
        question.setSuitDev(0);
        question.setSuitPre(0);
        if(testExamProblem.getSuitAct().equals(1)) {
            question.setSuitAct(1);
        } else if(testExamProblem.getSuitDev().equals(1)) {
            question.setSuitDev(1);
        } else if(testExamProblem.getSuitPre().equals(1)) {
            question.setSuitPre(1);
        }
    }

    private void convertType(TestExamProblem testExamProblem, Question question) {
        // type
        if(testExamProblem.getType().equals("single")) {
            question.setType(1);
        } else if(testExamProblem.getType().equals("trueorfalse")) {
            question.setType(2);
        } else if(testExamProblem.getType().equals("multiple")) {
            question.setType(3);
        }
    }

    @Data
    public static class SimpleAnswerOption {
        private String content;
        private Integer isCorrect;
        private Integer sort;
    }
}
