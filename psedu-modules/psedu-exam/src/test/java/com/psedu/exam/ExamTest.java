package com.psedu.exam;

import com.psedu.exam.domain.vo.ExamDetailVo;
import com.psedu.exam.mapper.ExamMapper;
import com.psedu.exam.service.IExamService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class ExamTest {
    @Autowired
    private IExamService examService;
    @Autowired
    private ExamMapper examMapper;

    @Test
    public void testGetExamDetailList() {
        List<ExamDetailVo> examDetailList = examService.getExamDetailList(200L);
        System.out.println(examDetailList);
        System.out.println(examMapper.selectStudentExamDetailList(200L, Arrays.asList(1L, 2L)));
    }
}
