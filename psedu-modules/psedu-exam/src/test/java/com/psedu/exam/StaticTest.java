package com.psedu.exam;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class StaticTest {
    @Test
    public void testDate() {
        Date nowDate = new Date();
        DateTime startDate = DateUtil.offsetMinute(nowDate, -45);
        DateTime endDate = DateUtil.offsetMinute(nowDate, 45);
        System.out.println("在考试开始后" +nowDate.after(startDate));
        System.out.println("考试结束前" +nowDate.before(endDate));
        System.out.println(nowDate.after(startDate) && nowDate.before(endDate));
        DateTime diffEndTime = DateUtil.offsetSecond(endDate, -1);
        System.out.println(diffEndTime.after(startDate) && diffEndTime.before(endDate));
    }
}
