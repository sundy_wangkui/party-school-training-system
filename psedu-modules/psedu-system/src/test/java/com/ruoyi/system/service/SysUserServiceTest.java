package com.psedu.system.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
public class SysUserServiceTest {

    @Autowired
    private ISysUserService userService;

    @Test
    public void testSelectDept() {
        System.out.println(userService.getPsStudentNumByDeptId(200L));
    }

    @Test
    public void testGetPsStudentNum() {
        System.out.println(userService.getPsStudentNum());
    }

    @Test
    public void testGetUserProfile() {
        System.out.println(userService.getUserProfile(Arrays.asList(104L, 1L, 2L, 100L)));
    }

    @Test
    public void testGetUserInfo() {
        System.out.println(userService.getUserInfo(Arrays.asList(104L, 1L, 2L, 100L)));
    }

}
