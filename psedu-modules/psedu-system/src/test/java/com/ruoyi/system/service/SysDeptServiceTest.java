package com.psedu.system.service;

import com.google.common.collect.Lists;
import com.psedu.system.api.domain.SysDept;
import com.psedu.system.mapper.SysDeptMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootTest
public class SysDeptServiceTest {
    @Autowired
    private SysDeptMapper sysDeptMapper;
    @Autowired
    private ISysDeptService sysDeptService;

    @Test
    public void testSelectDept() {
        System.out.println(sysDeptMapper.selectSimpleDeptByIds(Arrays.asList(100L, 200L)));
        System.out.println(sysDeptService.selectSimpleDeptList(Arrays.asList(100L, 200L)));
    }

    @Test
    public void testGetPsDeptAndChildDeptList() {
        System.out.println(sysDeptService.getPsDeptAndChildDeptList(200L));
        System.out.println(sysDeptService.getPsDeptAndChildDeptList(100L));
    }
}
