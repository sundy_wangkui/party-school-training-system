package com.psedu.base.service;

import com.psedu.base.domain.Course;
import com.psedu.base.mapper.CourseMapper;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.system.api.RemoteDeptService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
public class SemesterServiceTest {

    @Autowired
    private ISemesterService semesterService;

    @Test
    public void test() {
        System.out.println(semesterService.canSignSemester());
    }

    @Test
    public void testGetSemesterDetail() {
        System.out.println(semesterService.getSemesterDetail(1L));
    }
}
