package com.psedu.base.service;

import com.psedu.base.domain.Course;
import com.psedu.base.mapper.CourseMapper;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.system.api.RemoteDeptService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
public class CourseServiceTest {
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private RemoteDeptService remoteDeptService;
    @Autowired
    private ICourseService courseService;
    @Test
    public void test() {
        System.out.println(courseMapper.selectCourseForDept(new Course(), 200L));
    }

    @Test
    public void testRemoteDept() {
        System.out.println(remoteDeptService.getDeptNameByDeptId(Arrays.asList(100L, 200L), SecurityConstants.INNER).getData());
    }

    @Test
    public void testGetDeptCourseInfo() {
        System.out.println(courseService.getDeptCourseInfo(1L, 200L));
    }

}
