package com.psedu.base.staticTest;

import com.psedu.base.utils.HttpUtils;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileDownloadTest {
    @Test
    public void test() throws IOException {
        HttpUtils.getFile(
                "http://127.0.0.1:9300/statics/2022/04/19/03459c3a-fe38-46ce-8ee8-7d540a2bd643.jpeg",
                File.separator +"projectFile" +File.separator +"03459c3a-fe38-46ce-8ee8-7d540a2bd643.jpeg");
    }
}
