package com.psedu.base.service;

import com.psedu.base.domain.vo.SaveMergeClassVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
public class MergeClassServiceTest {

    @Autowired
    private MergeClassService mergeClassService;


    @Test
    public void testGetMergeDeptInfo() {
        System.out.println(mergeClassService.getMergeDeptInfo(1L));
    }

    @Test
    public void testUpdateMergeDeptInfo() {
        SaveMergeClassVo saveMergeClassVo = new SaveMergeClassVo();
        saveMergeClassVo.setSemeId(1L);
        saveMergeClassVo.setMergeClassId(2L);
        saveMergeClassVo.setDeptIds(Arrays.asList(200L, 201L));
        mergeClassService.saveMergeDeptInfo(saveMergeClassVo);
    }

    @Test
    public void testGetMergeDeptInfoAndUpdate() {
        this.testGetMergeDeptInfo();
        this.testUpdateMergeDeptInfo();
    }
}
