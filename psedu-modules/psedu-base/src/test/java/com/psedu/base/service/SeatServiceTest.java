package com.psedu.base.service;

import com.alibaba.cloud.dubbo.actuate.DubboMetadataEndpointAutoConfiguration;
import com.alibaba.cloud.dubbo.autoconfigure.*;
import org.apache.dubbo.spring.boot.actuate.autoconfigure.DubboEndpointAutoConfiguration;
import org.apache.dubbo.spring.boot.autoconfigure.DubboAutoConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
//@EnableAutoConfiguration(exclude = {
//        DubboAutoConfiguration.class,
//        DubboServiceAutoConfiguration.class,
//        DubboMetadataAutoConfiguration.class,
//        DubboEndpointAutoConfiguration.class,
//        DubboServiceDiscoveryAutoConfiguration.class,
//        DubboOpenFeignAutoConfiguration.class,
//        DubboServiceRegistrationNonWebApplicationAutoConfiguration.class,
//        DubboMetadataEndpointAutoConfiguration.class,
//        DubboLoadBalancedRestTemplateAutoConfiguration.class,
//        DubboServiceRegistrationAutoConfiguration.class
//})
public class SeatServiceTest {
    @Autowired
    private SeatService seatService;

    @Test
    public void testAssignSeat() {
        seatService.assignMergeClassSeat(1L);
    }
}
