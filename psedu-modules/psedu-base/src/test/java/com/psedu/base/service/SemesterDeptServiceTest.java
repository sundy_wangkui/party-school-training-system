package com.psedu.base.service;

import com.psedu.base.domain.SemesterDept;
import com.psedu.base.domain.vo.SemeDeptInfoVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
public class SemesterDeptServiceTest {

    @Autowired ISemesterDeptService semesterDeptService;

    @Test
    public void testSave() {
        SemesterDept semesterDept = new SemesterDept();
        semesterDept.setDeptId(200L).setSemeId(3L).setManagerName("ming1").setCreateBy("sdf");
        System.out.println(semesterDeptService.saveSemesterDept(semesterDept));
    }

    @Test
    public void getSemeDeptInfo() {
        SemeDeptInfoVo semeDeptInfo = semesterDeptService.getSemeDeptInfo(1L, 200L);
        System.out.println(semeDeptInfo);
    }
}
