package com.psedu.base.service;

import com.psedu.base.domain.SignRecord;
import com.psedu.base.domain.vo.SignRecordQueryVo;
import com.psedu.base.mapper.SignRecordMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SignRecordServiceTest {
    @Autowired
    private ISignRecordService signRecordService;
    @Test
    public void testSelectUnSignStudentList() {
        SignRecordQueryVo signRecordQueryVo = new SignRecordQueryVo();
        signRecordQueryVo.setCourseId(1L);
        System.out.println(signRecordService.selectUnSignStudentList(signRecordQueryVo));
    }
}
