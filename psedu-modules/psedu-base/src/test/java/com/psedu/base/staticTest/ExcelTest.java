package com.psedu.base.staticTest;

import com.psedu.base.domain.Apply;
import com.psedu.base.domain.vo.CertificateStudentVo;
import com.psedu.base.utils.ChineseName;
import com.psedu.base.utils.GraduateExcel;
import com.psedu.base.utils.POIUtils;
import com.psedu.base.utils.PSPOIUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class ExcelTest {
    public static void main(String[] args) throws Exception {
        new ExcelTest().testExcelRead();
    }

    @Test
    public void downCertExcel() throws IOException {
        List<CertificateStudentVo> certificateStudentVos = new ArrayList<>();
        CertificateStudentVo certificateStudentVo = new CertificateStudentVo();
        certificateStudentVo.setStudentName("明月");
        certificateStudentVo.setSex("男");
        certificateStudentVo.setBirthday(new Date());
        certificateStudentVo.setUnit("计算机科学与工程学院 18计科2班");
        certificateStudentVo.setTrainStartDate(new Date());
        certificateStudentVo.setTrainEndDate(new Date());
        certificateStudentVo.setScore(new BigDecimal("95"));
        certificateStudentVo.setComment("    该同志参加发展对象培训班学习，考核（考试）合格，准予结业。");
        certificateStudentVo.setIssueOrganization("中共湖南科技大学委员会党校");
        certificateStudentVo.setIssueDate(new Date());
        certificateStudentVo.setProfile("D:\\projectFile\\a1edce26-8ac3-42c8-aa5e-cf64a69458fd.jpeg");
        certificateStudentVos.add(certificateStudentVo);
        certificateStudentVos.add(certificateStudentVo);
        GraduateExcel graduateExcel = new GraduateExcel(certificateStudentVos);
        graduateExcel.downloadExcel2Local();
    }

    @Test
    public void testExcelTitle() throws Exception {
        HSSFWorkbook workbook = loadHSSFWorkbook("D:\\project\\party-school-training-system\\psedu-modules\\psedu-base\\src\\main\\resources\\sysfile\\psSeat.xls");
        if(workbook == null) {
            return;
        }
        HSSFSheet sheet = workbook.getSheetAt(0);
        PSPOIUtils.fillTitle(workbook, sheet, "标题");
        OutputStream fileOutputStream = new FileOutputStream(new File("./excel.xls"));
        workbook.write(fileOutputStream);
    }

    @Test
    public void testExcelRead() throws Exception {
        HSSFWorkbook workbook = loadHSSFWorkbook("D:\\project\\party-school-training-system\\psedu-modules\\psedu-base\\src\\main\\resources\\sysfile\\psSeat.xls");
        if(workbook == null) {
            return;
        }
        HSSFSheet sheet = workbook.getSheetAt(0);
        int seatCount = PSPOIUtils.getSheetSeatCount(sheet);
        System.out.println(seatCount);
        int seatNum = 0;
        // 默认表格样式
        for(int i = 0; i < 30; i++) {
            int width = 4;
            sheet.setColumnWidth(i, 3000);
//            sheet.setColumnWidth(i, 256 *width+184);
        }
        /* 将学员分配进座位 */
        List<Apply> waitAssignApplyList = getApplies();
        List<Apply> assignedApplyList = new ArrayList<>();
        Map<Long, HSSFCellStyle> seatStyleMap = new HashMap<>();
        Set<Long> deptIdSet = waitAssignApplyList.stream().map(Apply::getDeptId).collect(Collectors.toSet());

        for (int i = 0; i < deptIdSet.size(); i++) {
            Long deptId = (Long) deptIdSet.toArray()[i];
            HSSFCellStyle cellStyle = workbook.createCellStyle();
            // 垂直居中
            cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            // 上下居中
            cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            // 边框
            cellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            cellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            cellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            cellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            // 背景
            cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cellStyle.setFillForegroundColor( PSPOIUtils.COLOR_LIST.get(i) );
            seatStyleMap.put(deptId, cellStyle);
        }
        int lastRowNum = sheet.getLastRowNum();
        int nowStudentCount = 0;
        for (int h = 0; h <= lastRowNum; h++) {
            HSSFRow row = sheet.getRow(h);
            for (int l = 0; l <= row.getLastCellNum(); l++) {
                HSSFCell cell = row.getCell(l);
                // 不是空位
                if ( !PSPOIUtils.isEmptySeatCell(cell) ) {
                    continue;
                }

                // 宽度
                sheet.setColumnWidth((short)l, 3000);
                // 分配座位
                if(waitAssignApplyList.size() != 0) {
                    Apply apply = waitAssignApplyList.get(0);
                    waitAssignApplyList.remove(0);
                    assignedApplyList.add(apply);
                    // TOOD merge
                    apply.setSeatNum((long) (apply.getGroupId()*1000 + nowStudentCount));
                    nowStudentCount++;
                    cell.setCellValue(apply.getRealName());
                    HSSFCellStyle cellStyle = seatStyleMap.get(apply.getDeptId());
                    cell.setCellStyle(cellStyle);
                // 清除空位标志
                } else {
                    cell.setCellValue("");
                }
            }
        }

        /**
         * 遍历空位，将List<待分配的学员>装入，并为期分配座位号
         * 遍历List<已分配的学员>，修改学员座位号
         */


        /*int lastRowNum = sheet.getLastRowNum();
        for (int h = 0; h <= lastRowNum; h++) {
            HSSFRow row = sheet.getRow(h);
            for (int l = 0; l <= row.getLastCellNum(); l++) {
                HSSFCell cell = row.getCell(l);
                if (cell == null) {
                    continue;
                }
                String cellValue = POIUtils.getCellValue(cell);
                if( "-".equals(cellValue) ) {
                    cell.setCellValue("ok");
                    seatNum++;
                }
//                if( l == 0 ) {
//                    cell.setCellValue("ok");
//                    HSSFCellStyle style = workbook.createCellStyle();
//                    HSSFFont font = workbook.createFont();
//                    style.setFont(font);
//                    // 右边框
//                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
//                    // 填充
//                    style.setFillForegroundColor(HSSFColor.ORANGE.index);
//                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//                    // 加粗
//                    font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
//                    // 字体
//                    font.setFontHeightInPoints((short) 15);
//                    // 颜色
//                    font.setColor((HSSFColor.RED.index));
//                    cell.setCellStyle(style);
//                }
            }
        }
        */
        OutputStream fileOutputStream = new FileOutputStream(new File("./excel.xls"));
        workbook.write(fileOutputStream);
    }

    @Test
    public void testName() {
        System.out.println(ChineseName.getRealName());
    }

    private List<Apply> getApplies() {
        List<Apply> applyList = new ArrayList<>();
        for(int i = 0; i < 999; i++) {
            Apply apply = new Apply();
            apply.setApplyId( (long) i);
            apply.setRealName("明月" +i);
            apply.setDeptId( (long) i /100 );
            apply.setGroupId(2);
            applyList.add(apply);
        }
        return applyList;
    }

    private HSSFWorkbook loadHSSFWorkbook(String filePath){
        try {
            File file = new File(filePath);
            InputStream inputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            POIFSFileSystem fileSystem = new POIFSFileSystem(bufferedInputStream);
            return new HSSFWorkbook(fileSystem);
        } catch ( IOException e) {
            System.out.println("打开文件失败：" +filePath);
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void testCloneSheet() throws IOException {
        HSSFWorkbook workbook = loadHSSFWorkbook(PSPOIUtils.SEAT_EXCEL_PATH);
        if(workbook == null) {
            return;
        }
//        HSSFSheet sheet = workbook.getSheetAt(0);
        List<String> sheetNameList = POIUtils.cloneSheet(workbook, 5);
        System.out.println(sheetNameList);
        for(int i = 0; i < sheetNameList.size(); i++) {
            workbook.setSheetName(i, "第" +i +"合班座位表");
        }
        OutputStream fileOutputStream = new FileOutputStream(new File("./excel.xls"));
        workbook.write(fileOutputStream);
    }

}
