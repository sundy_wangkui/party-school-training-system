package com.psedu.base.service;

import com.alibaba.cloud.dubbo.actuate.DubboMetadataEndpointAutoConfiguration;
import com.alibaba.cloud.dubbo.autoconfigure.*;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.google.common.collect.Lists;
import com.psedu.base.domain.Apply;
import com.psedu.base.domain.vo.DivideGroupParam;
import com.psedu.base.mapper.ApplyMapper;
import com.psedu.base.service.impl.ApplyServiceImpl;
import com.psedu.base.utils.ChineseName;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.domain.R;
import com.psedu.common.mybatis.config.MybatisPlusConfiguration;
import com.psedu.common.mybatis.core.page.PageQuery;
import com.psedu.common.security.utils.SecurityUtils;
import com.psedu.system.api.RemoteDeptService;
import com.psedu.system.api.domain.vo.SysSimpleDeptVo;
import org.apache.dubbo.spring.boot.actuate.autoconfigure.DubboEndpointAutoConfiguration;
import org.apache.dubbo.spring.boot.autoconfigure.DubboAutoConfiguration;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
//@EnableAutoConfiguration(exclude = {
//        DubboAutoConfiguration.class,
//        DubboServiceAutoConfiguration.class,
//        DubboMetadataAutoConfiguration.class,
//        DubboEndpointAutoConfiguration.class,
//        DubboServiceDiscoveryAutoConfiguration.class,
//        DubboOpenFeignAutoConfiguration.class,
//        DubboServiceRegistrationNonWebApplicationAutoConfiguration.class,
//        DubboMetadataEndpointAutoConfiguration.class,
//        DubboLoadBalancedRestTemplateAutoConfiguration.class,
//        DubboServiceRegistrationAutoConfiguration.class
//})
public class ApplyServiceTest {
    @Autowired
    private ApplyMapper applyMapper;
    @Autowired
    private IApplyService applyService;
    @Autowired
    private RemoteDeptService remoteDeptService;

    @Disabled
    @Test
    public void addTestPeople() {
        ArrayList<Long> deptIds = new ArrayList<>();
        for (long i = 200; i <= 220; i++) {
            deptIds.add(i);
        }
        R<List<SysSimpleDeptVo>> deptNames = remoteDeptService.getDeptNameByDeptId(deptIds, SecurityConstants.INNER);
        List<SysSimpleDeptVo> deptDataList = deptNames.getData();

        for(int i = 0; i < 400; i++) {
            Apply apply = new Apply();
            Long deptId = (long)200 +new Random().nextInt(10);
            // 用部门名作班级名
            for (SysSimpleDeptVo sysSimpleDeptVo : deptDataList) {
                if(sysSimpleDeptVo.getDeptId().equals(deptId)) {
                    apply.setClasses(
                            sysSimpleDeptVo.getDeptName() +(1 +new Random().nextInt(5)) +"班"
                    );
                }
            }
            apply.setEduBackground("本科");
            apply.setChecked(1);
            apply.setDeptId(deptId);
            apply.setRealName(ChineseName.getRealName());
            apply.setSemeId(1L);
            apply.setPsUserId(104L);
            apply.setHnusterId("1805010219");
            applyMapper.insert(apply);
        }

    }

    @Disabled
    @Test
    public void fixApplyHnusterId() {
        List<Apply> applies = applyMapper.selectList();
        Apply apply = applies.get(0);
        long psUserId = apply.getPsUserId();
        for (Apply apply1 : applies) {
            apply1.setPsUserId(psUserId++);
        }
        applyMapper.updateBatchById(applies);
    }

    @Test
    private void testSelectMergeClassStudent() {
        System.out.println(applyMapper.selectMergeClassStudent(1L, 1));
    }

    @Test
    public void test() {
        /*Apply apply = new Apply();
//        apply.setRealName("明月");
//        System.out.println(applyMapper.selectApplyList(new PageQuery().build(), apply).getRecords());
        System.out.println(applyService.testSelectList(apply));*/
//        applyMapper.update(
//                null,
//                new LambdaUpdateWrapper<Apply>()
//                        .set(Apply::getGroupId, null)
//                        .eq(Apply::getGroupId, 2)
//        );
        DivideGroupParam divideGroupParam = new DivideGroupParam();
        divideGroupParam.setApplyIds(Arrays.asList(18L, 19L, 20L));
        divideGroupParam.setGroupId(1);
        applyMapper.divideGroup(divideGroupParam);
    }

    @Test
    public void testUserApply() {
        System.out.println();
        System.out.println(applyService.userApply(104L));
    }

    @Test
    public void testUserApplySummary() {
        System.out.println(applyService.userApplySummary(104L, 1L));
    }

    @Test
    public void testSelectSemesterDeptApplyCount() {
        System.out.println(applyMapper.selectSemesterDeptApplyCount(2L));
    }

    @Test
    public void testSelectTrainDataList() {
        Apply apply = new Apply();
        apply.setSemeId(1L);
        apply.setDeptId(200L);
        System.out.println(applyMapper.selectTrainDataList(apply));
    }
}
