package com.psedu.base.service.impl;

import java.util.List;
import com.psedu.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.psedu.base.mapper.SignLaunchMapper;
import com.psedu.base.domain.SignLaunch;
import com.psedu.base.service.ISignLaunchService;

/**
 * 发起签到Service业务层处理
 * 
 * @author mingyue
 * @date 2022-05-16
 */
@Service
public class SignLaunchServiceImpl implements ISignLaunchService 
{
    @Autowired
    private SignLaunchMapper signLaunchMapper;

    /**
     * 查询发起签到
     * 
     * @param signLaunchId 发起签到主键
     * @return 发起签到
     */
    @Override
    public SignLaunch selectSignLaunchBySignLaunchId(Long signLaunchId)
    {
        return signLaunchMapper.selectSignLaunchBySignLaunchId(signLaunchId);
    }

    /**
     * 查询发起签到列表
     * 
     * @param signLaunch 发起签到
     * @return 发起签到
     */
    @Override
    public List<SignLaunch> selectSignLaunchList(SignLaunch signLaunch)
    {
        return signLaunchMapper.selectSignLaunchList(signLaunch);
    }

    /**
     * 新增发起签到
     * 
     * @param signLaunch 发起签到
     * @return 结果
     */
    @Override
    public int insertSignLaunch(SignLaunch signLaunch)
    {
        signLaunch.setCreateTime(DateUtils.getNowDate());
        return signLaunchMapper.insertSignLaunch(signLaunch);
    }

    /**
     * 修改发起签到
     * 
     * @param signLaunch 发起签到
     * @return 结果
     */
    @Override
    public int updateSignLaunch(SignLaunch signLaunch)
    {
        return signLaunchMapper.updateSignLaunch(signLaunch);
    }

    /**
     * 批量删除发起签到
     * 
     * @param signLaunchIds 需要删除的发起签到主键
     * @return 结果
     */
    @Override
    public int deleteSignLaunchBySignLaunchIds(Long[] signLaunchIds)
    {
        return signLaunchMapper.deleteSignLaunchBySignLaunchIds(signLaunchIds);
    }

    /**
     * 删除发起签到信息
     * 
     * @param signLaunchId 发起签到主键
     * @return 结果
     */
    @Override
    public int deleteSignLaunchBySignLaunchId(Long signLaunchId)
    {
        return signLaunchMapper.deleteSignLaunchBySignLaunchId(signLaunchId);
    }
}
