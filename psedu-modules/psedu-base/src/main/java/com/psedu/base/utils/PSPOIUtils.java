package com.psedu.base.utils;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class PSPOIUtils {

    public final static String SEAT_EXCEL_PATH =
            ResourceUtils.CLASSPATH_URL_PREFIX +PSPOIUtils.SEAT_RESOURCES_PATH;

    public final static String SEAT_RESOURCES_PATH = "sysfile" +File.separator +"psSeat.xls";

    /** 空位置标识 */
    private final static String EMPTY_SEAT_STR = "-";

    /** 分组位置标识 */
    private final static String GROUP_STYLE_STR = "*";

    /** 标题 */
    private final static String TITLE_STR = "${TITLE}";

    // 颜色列表
    public static final List<Short> COLOR_LIST = Arrays.asList(
            HSSFColor.CORAL.index,
            HSSFColor.RED.index,
            HSSFColor.LIGHT_YELLOW.index,
            HSSFColor.SKY_BLUE.index,
            HSSFColor.BROWN.index,
            HSSFColor.SEA_GREEN.index,
            HSSFColor.GOLD.index,
            HSSFColor.INDIGO.index,
            HSSFColor.MAROON.index,
            HSSFColor.LAVENDER.index,
            HSSFColor.LIGHT_TURQUOISE.index,
            HSSFColor.TURQUOISE.index,
            HSSFColor.PINK.index,
            HSSFColor.LIME.index,
            HSSFColor.TAN.index
    );

    /**
     * 计算Excel位置数
     * @return Excel位置数
     */
    public static int getSheetSeatCount(HSSFSheet sheet) {
        int seatNum = 0;
        try{
            int lastRowNum = sheet.getLastRowNum();
            for (int h = 0; h <= lastRowNum; h++) {
                HSSFRow row = sheet.getRow(h);
                for (int l = 0; l <= row.getLastCellNum(); l++) {
                    HSSFCell cell = row.getCell(l);
                    if (cell == null) {
                        continue;
                    }
                    if( isEmptySeatCell(cell) ) {
                        seatNum++;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return seatNum;
    }

    /**
     * Cell是否是空位置
     * @param cell cell
     * @return 是否是空位置
     */
    public static boolean isEmptySeatCell(Cell cell) {
        String cellValue = POIUtils.getCellValue(cell);
        return EMPTY_SEAT_STR.equals(cellValue);
    }

    /**
     * Cell是否是标题
     * @param cell cell
     * @return 是否是标题
     */
    public static boolean isTitleCell(Cell cell) {
        String cellValue = POIUtils.getCellValue(cell);
        return TITLE_STR.equals(cellValue);
    }

    /**
     * Cell是否是颜色信息
     * @param cell cell
     * @return 是否是颜色信息位置
     */
    public static boolean isStyleInfoCell(Cell cell) {
        String cellValue = POIUtils.getCellValue(cell);
        return GROUP_STYLE_STR.equals(cellValue);
    }

    /**
     * 设置标题
     * @param workbook workbook
     * @param sheet sheet
     * @param title title
     */
    public static void fillTitle(HSSFWorkbook workbook, HSSFSheet sheet, String title) {
        int lastRowNum = sheet.getLastRowNum();
        for (int h = 0; h <= lastRowNum; h++) {
            HSSFRow row = sheet.getRow(h);
            for (int l = 0; l <= row.getLastCellNum(); l++) {
                HSSFCell cell = row.getCell(l);
                if (PSPOIUtils.isTitleCell(cell)) {
                    cell.setCellValue(title);
                    HSSFCellStyle cellStyle = cell.getCellStyle();
                    HSSFFont font = cellStyle.getFont(workbook);
                    font.setFontHeightInPoints((short) 40);
                    return;
                }
            }
        }
    }




}
