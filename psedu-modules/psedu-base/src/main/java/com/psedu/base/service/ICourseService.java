package com.psedu.base.service;

import java.util.List;
import com.psedu.base.domain.Course;
import com.psedu.base.domain.vo.CourseSignLaunchVo;
import com.psedu.base.domain.vo.CourseVo;
import com.psedu.base.domain.vo.SemeCourseVo;
import com.psedu.base.domain.vo.UserCourseVo;

/**
 * 课程Service接口
 * 
 * @author mingyue
 * @date 2022-02-10
 */
public interface ICourseService 
{
    /**
     * 查询课程
     * 
     * @param courseId 课程主键
     * @return 课程
     */
    public CourseSignLaunchVo selectCourseByCourseId(Long courseId);

    /**
     * 查询课程列表
     * 
     * @param course 课程
     * @return 课程集合
     */
    public List<CourseVo> selectCourseList(Course course);

    /**
     * 新增课程
     * 
     * @param course 课程
     * @return 结果
     */
    public int insertCourse(CourseSignLaunchVo course);

    /**
     * 修改课程
     * 
     * @param course 课程
     * @return 结果
     */
    public int updateCourse(CourseSignLaunchVo course);

    /**
     * 批量删除课程
     * 
     * @param courseIds 需要删除的课程主键集合
     * @return 结果
     */
    public int deleteCourseByCourseIds(Long[] courseIds);

    /**
     * 删除课程信息
     * 
     * @param courseId 课程主键
     * @return 结果
     */
    public int deleteCourseByCourseId(Long courseId);

    /**
     * 获取用户的所有课程
     * @param applyId 申请ID
     * @return 用户的所有课程列表
     */
    List<UserCourseVo> getUserCourse(Long applyId);

    /**
     * 获取部门课程列表
     * @param semeId
     * @param deptId
     * @return SemeCourseVo
     */
    SemeCourseVo getDeptCourseInfo(Long semeId, Long deptId);

    boolean liveStatus(Long courseId, Integer liveStatus);
}
