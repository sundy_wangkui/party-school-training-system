package com.psedu.base.domain.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.psedu.common.core.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SimpleUserApplyVo {

    /** 申请ID */
    private Long applyId;

    /** 学期名 */
    private String semeName;

    private Long semeId;

    /** 审核状态 */
    private Integer checked;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

}
