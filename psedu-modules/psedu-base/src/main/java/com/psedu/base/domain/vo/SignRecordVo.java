package com.psedu.base.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.psedu.common.core.annotation.Excel;
import lombok.Data;

@Data
public class SignRecordVo {
    /** 签到记录ID */
    private Long signRecordId;

    private String nickName;

    /** 签到场次ID */
    private Long signLaunchId;

    /** 签到用户ID */
    private Long pseduUserId;

    /** 签到经度 */
    private Double longitude;

    /** 签到纬度 */
    private Double latitude;

    /** 精度 */
    private Double accuracy;
}
