package com.psedu.base.service;

import com.psedu.base.domain.vo.PsDeptAndExamSelectVo;
import com.psedu.base.domain.vo.SemeAndPsDeptSelectVo;

public interface SelectOptionService {

    /**
     * @return 学期、部门
     */
    SemeAndPsDeptSelectVo getSemeAndPsDept();

    /**
     * @return 部门、考试
     */
    PsDeptAndExamSelectVo getPsDeptAndExam();
}
