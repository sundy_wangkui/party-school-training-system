package com.psedu.base.controller;

import com.psedu.base.domain.Apply;
import com.psedu.base.domain.vo.StudentTrainDataVo;
import com.psedu.base.service.StudentService;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.mybatis.core.page.TableDataInfo;
import com.psedu.common.security.annotation.RequiresLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController extends BaseController {

    @Autowired
    private StudentService studentService;

    @RequiresLogin
    @GetMapping("/examList")
    public AjaxResult getExamList() {
        // 通过学员账号查看报名学期，报名学期是否开启考试
        return AjaxResult.success(studentService.getStudentExamList());
    }

    @RequiresLogin
    @GetMapping("/trainDataList")
    public TableDataInfo<StudentTrainDataVo> getTrainDataList(Apply apply) {
        List<StudentTrainDataVo> studentTrainDataVoList = studentService.getTrainDataList(apply);
        return TableDataInfo.build(studentTrainDataVoList);
    }

    @RequiresLogin
    @GetMapping("/studentTrainData/{applyId}")
    public R<StudentTrainDataVo> getTrainData(@PathVariable Long applyId) {
        StudentTrainDataVo studentTrainDataVo = studentService.studentTrainData(applyId);
        return R.ok(studentTrainDataVo);
    }


//    @RequiresPermissions("file:download")
    @PostMapping("/downloadCertificate/{semeId}/{deptId}")
    public void downloadCertificate(@PathVariable("semeId") Long semeId,
                                    @PathVariable("deptId") Long deptId,
                                    HttpServletResponse httpResponse) {
        studentService.downloadCertificate(semeId, deptId, httpResponse);
    }
}
