package com.psedu.base.service;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;

public interface SeatService {
    /**
     * 为所有合班分配座位
     * @param semeId 学期ID
     */
    public HSSFWorkbook assignMergeClassSeat(Long semeId);

    /**
     * 返回学员座位excel
     * @param response httpResponse
     * @param semeId 学期ID
     */
    public void downloadMergeClassSeat(HttpServletResponse response, Long semeId);

    /**
     * 通过流的方式输出excle到页面
     * @param response 响应
     * @param workbook 工作空间
     * @param fileName 文件名
     */
    public void outExcelStream(HttpServletResponse response, Workbook workbook, String fileName);

}
