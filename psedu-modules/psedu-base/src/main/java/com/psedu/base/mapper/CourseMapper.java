package com.psedu.base.mapper;

import java.util.List;
import com.psedu.base.domain.Course;
import com.psedu.base.domain.Semester;
import com.psedu.base.domain.vo.CourseSignLaunchVo;
import com.psedu.common.mybatis.annotation.DataColumn;
import com.psedu.common.mybatis.annotation.DataPermission;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 课程Mapper接口
 * 
 * @author mingyue
 * @date 2022-02-10
 */
public interface CourseMapper extends BaseMapperPlus<CourseMapper, Course, Course>
{
    /**
     * 查询课程
     * 
     * @param courseId 课程主键
     * @return 课程
     */
    public Course selectCourseByCourseId(Long courseId);

    /**
     * 查询课程列表
     * 
     * @param course 课程
     * @return 课程集合
     */
    public List<Course> selectCourseList(Course course);

    /**
     * 部门范围课程列表
     * @param course 课程
     * @param deptId 所在部门
     * @return 课程集合
     */
    public List<Course> selectCourseForDept(@Param("course") Course course, @Param("deptId") Long deptId);

    /**
     * 新增课程
     * 
     * @param course 课程
     * @return 结果
     */
    public int insertCourse(Course course);

    /**
     * 修改课程
     * 
     * @param course 课程
     * @return 结果
     */
    public int updateCourse(Course course);

    /**
     * 删除课程
     * 
     * @param courseId 课程主键
     * @return 结果
     */
    public int deleteCourseByCourseId(Long courseId);

    /**
     * 批量删除课程
     * 
     * @param courseIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCourseByCourseIds(Long[] courseIds);

    /**
     * 获取部门的课程, 合班相同且没规定部门 或者 部门一样就是自己的课程，根据开课时间排序
     * @param deptId 部门ID
     * @param semeId 学期ID
     * @return 分党校在该学期下的课程列表
     */
    List<CourseSignLaunchVo> selectDeptCourse(@Param("deptId") Long deptId, @Param("semeId") Long semeId);
}
