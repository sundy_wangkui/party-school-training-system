package com.psedu.base.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.base.domain.SignLaunch;
import com.psedu.base.service.ISignLaunchService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 发起签到Controller
 * 
 * @author mingyue
 * @date 2022-05-16
 */
@RestController
@RequestMapping("/signLaunch")
public class SignLaunchController extends BaseController
{
    @Autowired
    private ISignLaunchService signLaunchService;

    /**
     * 查询发起签到列表
     */
    @RequiresPermissions("psedu-base:signLaunch:list")
    @GetMapping("/list")
    public TableDataInfo list(SignLaunch signLaunch)
    {
        startPage();
        List<SignLaunch> list = signLaunchService.selectSignLaunchList(signLaunch);
        return getDataTable(list);
    }

    /**
     * 导出发起签到列表
     */
    @RequiresPermissions("psedu-base:signLaunch:export")
    @Log(title = "发起签到", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SignLaunch signLaunch)
    {
        List<SignLaunch> list = signLaunchService.selectSignLaunchList(signLaunch);
        ExcelUtil<SignLaunch> util = new ExcelUtil<SignLaunch>(SignLaunch.class);
        util.exportExcel(response, list, "发起签到数据");
    }

    /**
     * 获取发起签到详细信息
     */
    @RequiresPermissions("psedu-base:signLaunch:query")
    @GetMapping(value = "/{signLaunchId}")
    public AjaxResult getInfo(@PathVariable("signLaunchId") Long signLaunchId)
    {
        return AjaxResult.success(signLaunchService.selectSignLaunchBySignLaunchId(signLaunchId));
    }

    /**
     * 新增发起签到
     */
    @RequiresPermissions("psedu-base:signLaunch:add")
    @Log(title = "发起签到", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SignLaunch signLaunch)
    {
        return toAjax(signLaunchService.insertSignLaunch(signLaunch));
    }

    /**
     * 修改发起签到
     */
    @RequiresPermissions("psedu-base:signLaunch:edit")
    @Log(title = "发起签到", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SignLaunch signLaunch)
    {
        return toAjax(signLaunchService.updateSignLaunch(signLaunch));
    }

    /**
     * 删除发起签到
     */
    @RequiresPermissions("psedu-base:signLaunch:remove")
    @Log(title = "发起签到", businessType = BusinessType.DELETE)
	@DeleteMapping("/{signLaunchIds}")
    public AjaxResult remove(@PathVariable Long[] signLaunchIds)
    {
        return toAjax(signLaunchService.deleteSignLaunchBySignLaunchIds(signLaunchIds));
    }
}
