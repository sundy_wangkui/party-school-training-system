package com.psedu.base.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.psedu.base.domain.vo.SignRecordQueryVo;
import com.psedu.base.domain.vo.SignRecordVo;
import com.psedu.base.domain.vo.StudentPositionVo;
import com.psedu.base.domain.vo.UnSignStudentVo;
import com.psedu.common.security.annotation.RequiresLogin;
import com.psedu.common.security.utils.SecurityUtils;
import com.psedu.system.api.RemoteUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.base.domain.SignRecord;
import com.psedu.base.service.ISignRecordService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 签到记录Controller
 * 
 * @author mingyue
 * @date 2022-05-16
 */
@RestController
@RequestMapping("/signRecord")
public class SignRecordController extends BaseController
{
    @Autowired
    private ISignRecordService signRecordService;
    @Autowired
    private RemoteUserService remoteUserService;

    @PostMapping("/studentSign/{signLaunchId}")
    @RequiresLogin
    public AjaxResult studentSign(@PathVariable("signLaunchId") Long signLaunchId, @RequestBody StudentPositionVo studentPositionVo) {
        Long userId = SecurityUtils.getUserId();
        signRecordService.studentSign(userId, signLaunchId, studentPositionVo);
        return AjaxResult.success();
    }

    /**
     * 查询签到记录列表
     */
    @RequiresPermissions("psedu-base:signRecord:list")
    @GetMapping("/list")
    public TableDataInfo list(SignRecordQueryVo signRecordQueryVo)
    {
        startPage();
        List<SignRecordVo> list = signRecordService.selectSignRecordList(signRecordQueryVo);
        return getDataTable(list);
    }

    /**
     * 未签到名单列表
     */
    @RequiresPermissions("psedu-base:signRecord:list")
    @GetMapping("/unSignStudentList")
    public AjaxResult unSignStudentList(SignRecordQueryVo signRecordQueryVo)
    {
        List<UnSignStudentVo> list = signRecordService.selectUnSignStudentList(signRecordQueryVo);
        return AjaxResult.success(list);
    }

    /**
     * 获取签到记录详细信息
     */
    @RequiresPermissions("psedu-base:signRecord:query")
    @GetMapping(value = "/{signRecordId}")
    public AjaxResult getInfo(@PathVariable("signRecordId") Long signRecordId)
    {
        return AjaxResult.success(signRecordService.selectSignRecordBySignRecordId(signRecordId));
    }

    /**
     * 新增签到记录
     */
    @RequiresPermissions("psedu-base:signRecord:add")
    @Log(title = "签到记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SignRecord signRecord)
    {
        return toAjax(signRecordService.insertSignRecord(signRecord));
    }

    /**
     * 修改签到记录
     */
    @RequiresPermissions("psedu-base:signRecord:edit")
    @Log(title = "签到记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SignRecord signRecord)
    {
        return toAjax(signRecordService.updateSignRecord(signRecord));
    }

    /**
     * 删除签到记录
     */
    @RequiresPermissions("psedu-base:signRecord:remove")
    @Log(title = "签到记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{signRecordIds}")
    public AjaxResult remove(@PathVariable Long[] signRecordIds)
    {
        return toAjax(signRecordService.deleteSignRecordBySignRecordIds(signRecordIds));
    }
}
