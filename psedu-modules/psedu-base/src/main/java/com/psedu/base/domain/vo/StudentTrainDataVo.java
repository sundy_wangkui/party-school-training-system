package com.psedu.base.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.psedu.common.core.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class StudentTrainDataVo {
    /** 申请ID */
    private Long applyId;

    /** 真实姓名 */
    private String realName;

    /** 学号/工号 */
    private String hnusterId;

    /** 党校用户ID */
    private Long psUserId;

    /** 报名学期ID */
    private Long semeId;

    /** 部门ID */
    private Long deptId;

    private String deptName;

    private Long groupId;

    private String groupName;

    private BigDecimal scores;

    private String summary;

    private String seatNum;

}
