package com.psedu.base.mapper;

import java.util.List;

import com.psedu.base.domain.SignRecord;
import com.psedu.base.domain.vo.SignRecordQueryVo;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 签到记录Mapper接口
 * 
 * @author mingyue
 * @date 2022-05-16
 */
public interface SignRecordMapper  extends BaseMapperPlus<SignRecordMapper, SignRecord, SignRecord>
{
    /**
     * 查询签到记录
     * 
     * @param signRecordId 签到记录主键
     * @return 签到记录
     */
    public SignRecord selectSignRecordBySignRecordId(Long signRecordId);

    /**
     * 查询签到记录列表
     * 
     * @param signRecord 签到记录
     * @return 签到记录集合
     */
    public List<SignRecord> selectSignRecordList(SignRecordQueryVo signRecord);

    /**
     * 新增签到记录
     * 
     * @param signRecord 签到记录
     * @return 结果
     */
    public int insertSignRecord(SignRecord signRecord);

    /**
     * 修改签到记录
     * 
     * @param signRecord 签到记录
     * @return 结果
     */
    public int updateSignRecord(SignRecord signRecord);

    /**
     * 删除签到记录
     * 
     * @param signRecordId 签到记录主键
     * @return 结果
     */
    public int deleteSignRecordBySignRecordId(Long signRecordId);

    /**
     * 批量删除签到记录
     * 
     * @param signRecordIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSignRecordBySignRecordIds(Long[] signRecordIds);
}
