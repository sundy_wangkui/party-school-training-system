package com.psedu.base.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class DivideGroupParam {

    private Integer groupId;

    private List<Long> applyIds;
}
