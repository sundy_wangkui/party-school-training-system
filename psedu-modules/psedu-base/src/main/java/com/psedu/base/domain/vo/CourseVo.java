package com.psedu.base.domain.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.psedu.base.domain.Course;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class CourseVo {
    /** 课程ID */
    private Long courseId;

    /** 课程名称 */
    private String courseTitle;

    /** 培训学期 */
    private Long semeId;

    /** 所属部门ID */
    private Long deptId;

    /** 适用范围，合班 or 部门 */
    private String deptScope;

    /** 所属合班ID */
    private Long mergeClassId;

    /** 封面图片链接 */
    private String imageUrl;

    /** 实现的ID */
    private Long realizeCourseId;

    /** 上课地点 */
    private String classroom;

    /** 课程内容 */
    private String content;

    /** 排序 */
    private Long orderNum;

    /** 授课教师 */
    private String teacher;

    /** 需要评教 */
    private Integer evaluate;

    /** 课程开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    /** 已删除 */
    private Integer deleted;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;
}
