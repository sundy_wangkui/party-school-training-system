package com.psedu.base.controller.remoteController;

import com.psedu.base.api.domain.dto.SemesterTrainObjectDTO;
import com.psedu.base.service.ISemesterService;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.security.annotation.InnerAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/semester")
public class SemesterFeignController extends BaseController {

    @Autowired
    private ISemesterService semesterService;

    @InnerAuth
    @GetMapping("/getTrainObjectBySemesterId/{semeId}")
    R<SemesterTrainObjectDTO> getTrainObjectBySemesterId(@PathVariable("semeId") Long semeId) {
        SemesterTrainObjectDTO examTrainObjectDTO = new SemesterTrainObjectDTO();
        examTrainObjectDTO.setSemesterId(semeId);
        Integer trainObject = semesterService.getTrainObjectBySemesterId(semeId);
        examTrainObjectDTO.setTrainObject(trainObject);
        return R.ok(examTrainObjectDTO);
    }
}
