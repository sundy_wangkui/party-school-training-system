package com.psedu.base.domain.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CertificateStudentVo {
    private Long userId;
    private String studentName;
    private String sex;
    private Date birthday;
    private String unit;
    private Date trainStartDate;
    private Date trainEndDate;
    private BigDecimal score;
    private String comment;
    private String issueOrganization;
    private Date issueDate;
    private String profile;
}
