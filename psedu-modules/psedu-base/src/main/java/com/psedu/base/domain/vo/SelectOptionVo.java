package com.psedu.base.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SelectOptionVo<T> {
    public T key;
    public String value;
    public Boolean disabled;
    public Boolean checked;
}
