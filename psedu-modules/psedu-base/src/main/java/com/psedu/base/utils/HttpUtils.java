package com.psedu.base.utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;


public class HttpUtils {

    public static String getFile(String pictureUrl, String fileSavePath) throws IOException {
        //建立图片连接
        URL url = new URL(pictureUrl);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        //设置请求方式
        connection.setRequestMethod("GET");
        //设置超时时间
        connection.setConnectTimeout(10*1000);
        //输入流
        InputStream inStream = connection.getInputStream();
        int len = 0;
        byte[] dataByteArr = new byte[1024];
        File file = new File(fileSavePath);
        if(!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        //输出流，图片输出的目的文件
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileSavePath));
        //以流的方式上传
        while ((len = inStream.read(dataByteArr)) != -1){
            outputStream.write(dataByteArr,0,len);
        }
        //记得关闭流，不然消耗资源
        inStream.close();
        outputStream.close();
        return fileSavePath;
    }
}
