package com.psedu.base.controller;

import cn.hutool.http.HttpResponse;
import com.psedu.base.domain.vo.MergeDeptInfo;
import com.psedu.base.service.SeatService;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.utils.file.FileUtils;
import com.psedu.common.security.annotation.RequiresPermissions;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RestController
@RequestMapping("/seat")
public class SeatController {
    @Autowired
    private SeatService seatService;

    @RequiresPermissions("mergeClass:info:get")
    @PostMapping("/info/{semeId}")
    public void download(HttpServletResponse response, @PathVariable("semeId") Long semeId) {
        seatService.downloadMergeClassSeat(response, semeId);
    }

    /**
     * 修改合班座位表Excel
     */
    public void updateMergeClassSeatExcel() {
        // TODO
    }
}
