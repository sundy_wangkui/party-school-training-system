package com.psedu.base.domain.vo;

import com.psedu.base.domain.Course;
import com.psedu.base.domain.SignLaunch;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CourseSignLaunchVo extends Course {
    private SignLaunch signLaunch;
}
