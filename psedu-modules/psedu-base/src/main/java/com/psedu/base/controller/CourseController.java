package com.psedu.base.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.psedu.base.constant.Constants;
import com.psedu.base.domain.vo.CourseSignLaunchVo;
import com.psedu.base.domain.vo.CourseVo;
import com.psedu.base.domain.vo.UserCourseVo;
import com.psedu.common.security.annotation.RequiresLogin;
import com.psedu.common.security.utils.SecurityUtils;
import com.psedu.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.base.domain.Course;
import com.psedu.base.service.ICourseService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 课程Controller
 * 
 * @author mingyue
 * @date 2022-02-10
 */
@RestController
@RequestMapping("/course")
public class CourseController extends BaseController
{
    @Autowired
    private ICourseService courseService;

    /**
     * 查询课程列表
     */
    @RequiresPermissions("psedu-base:course:list")
    @GetMapping("/list")
    public TableDataInfo list(Course course)
    {
        startPage();
        List<CourseVo> list = courseService.selectCourseList(course);
        return getDataTable(list);
    }

    /**
     * 获取课程详细信息
     */
    @RequiresPermissions("psedu-base:course:query")
    @GetMapping(value = "/{courseId}")
    public AjaxResult getInfo(@PathVariable("courseId") Long courseId)
    {
        return AjaxResult.success(courseService.selectCourseByCourseId(courseId));
    }

    /**
     * 获取学生课程信息
     */
    @RequiresLogin
    @GetMapping(value = "/semeCourseInfo/{semeId}")
    public AjaxResult getUserCourseInfoBySemeId(@PathVariable("semeId") Long semeId)
    {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long deptId = loginUser.getDeptId();
        return AjaxResult.success(courseService.getDeptCourseInfo(semeId, deptId));
    }

    @RequiresLogin
    @GetMapping("/getUserCourse/{applyId}")
    public AjaxResult getUserCourse(@PathVariable("applyId") Long applyId) {
        List<UserCourseVo> userCourseVoList = courseService.getUserCourse(applyId);
        return AjaxResult.success(userCourseVoList);
    }


    /**
     * 新增课程
     */
    @RequiresPermissions("psedu-base:course:add")
    @Log(title = "课程", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CourseSignLaunchVo course)
    {
        return toAjax(courseService.insertCourse(course));
    }

    /**
     * 修改课程
     */
    @RequiresPermissions("psedu-base:course:edit")
    @Log(title = "课程", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CourseSignLaunchVo course)
    {
        return toAjax(courseService.updateCourse(course));
    }

    /**
     * 修改课程
     */
    @RequiresLogin
    @Log(title = "课程直播", businessType = BusinessType.UPDATE)
    @GetMapping("/liveCourseStatus/{courseId}/{liveStatus}")
    public AjaxResult liveStatus(@PathVariable("courseId") Long courseId, @PathVariable("liveStatus") Integer liveStatus)
    {
        return toAjax(courseService.liveStatus(courseId, liveStatus));
    }

    /**
     * 删除课程
     */
    @RequiresPermissions("psedu-base:course:remove")
    @Log(title = "课程", businessType = BusinessType.DELETE)
	@DeleteMapping("/{courseIds}")
    public AjaxResult remove(@PathVariable Long[] courseIds)
    {
        return toAjax(courseService.deleteCourseByCourseIds(courseIds));
    }
}
