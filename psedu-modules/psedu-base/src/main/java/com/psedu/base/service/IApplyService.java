package com.psedu.base.service;

import java.util.List;
import com.psedu.base.domain.Apply;
import com.psedu.base.domain.vo.ApplyVo;
import com.psedu.base.domain.vo.SimpleUserApplyVo;
import com.psedu.common.mybatis.core.page.PageQuery;
import com.psedu.common.mybatis.core.page.TableDataInfo;

/**
 * 培训报名Service接口
 * 
 * @author mingyue
 * @date 2022-01-31
 */
public interface IApplyService 
{
    /**
     * 查询培训报名
     * 
     * @param applyId 培训报名主键
     * @return 培训报名
     */
    public Apply selectApplyByApplyId(Long applyId);

    /**
     * 新增培训报名
     * 
     * @param apply 培训报名
     * @return 结果
     */
    public int insertApply(Apply apply);

    /**
     * 修改培训报名
     * 
     * @param apply 培训报名
     * @return 结果
     */
    public int updateApply(Apply apply);

    /**
     * 批量删除培训报名
     * 
     * @param applyIds 需要删除的培训报名主键集合
     * @return 结果
     */
    public int deleteApplyByApplyIds(Long[] applyIds);

    /**
     * 删除培训报名信息
     * 
     * @param applyId 培训报名主键
     * @return 结果
     */
    public int deleteApplyByApplyId(Long applyId);

    /**
     * 查询培训报名列表
     *
     * @param apply 培训报名
     * @return 培训报名table
     */
    TableDataInfo<Apply> selectApplyList(PageQuery pageQuery, Apply apply);

    public List<Apply> testSelectList(Apply apply);

    /**
     * 根据ID查用戶申请
     * @param userId 用户ID
     * @return 申请列表
     */
    List<SimpleUserApplyVo> userApply(Long userId);

    /**
     * 获取用户学期总结
     * @param userId
     * @param applyId
     * @return
     */
    String userApplySummary(Long userId, Long applyId);

    /**
     * 用户提交申请
     * @param applyVo
     * @return
     */
    int submitApply(ApplyVo applyVo);

    ApplyVo getApplyDetailByApplyId(Long applyId);

    /**
     * 申请通过
     * @param applyId
     * @return
     */
    int applyPass(Long applyId);
}
