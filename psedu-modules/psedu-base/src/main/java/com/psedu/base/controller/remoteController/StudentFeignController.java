package com.psedu.base.controller.remoteController;

import com.psedu.base.api.domain.dto.StudentDTO;
import com.psedu.base.service.StudentService;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.security.annotation.InnerAuth;
import com.psedu.common.security.annotation.RequiresLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentFeignController {

    @Autowired
    private StudentService studentService;

    @InnerAuth
    @GetMapping("/studentInfo/{semesterId}/{psUserId}")
    R<StudentDTO> getStudentInfo(@PathVariable("semesterId") Long semesterId,
                                 @PathVariable("psUserId") Long psUserId,
                                 @RequestHeader(SecurityConstants.FROM_SOURCE) String source) {
        StudentDTO studentDTO = studentService.getStudentInfo(semesterId, psUserId);
        return R.ok(studentDTO);
    }

    @InnerAuth
    @GetMapping("/updateExamScore/{applyId}/{score}")
    R updateExamStudentScore(@PathVariable("applyId") Long applyId,
                                 @PathVariable("score") BigDecimal score) {
        studentService.updateExamStudentScore(applyId, score);
        return R.ok();
    }

    @InnerAuth
    @GetMapping("/getAllPassSemester/{psUserId}")
    R<List<Long>> getAllPassSemester(@PathVariable("psUserId") Long psUserId) {
        return R.ok(studentService.getAllPassSemester(psUserId));
    }
}
