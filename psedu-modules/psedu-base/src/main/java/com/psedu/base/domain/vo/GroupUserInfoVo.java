package com.psedu.base.domain.vo;

import com.psedu.common.core.annotation.Excel;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class GroupUserInfoVo {

    private Long semeId;

    private String semeName;

    private List<Student> waitUserList;

    private List<GroupStudent> groupStudentList;

    @Data
    public static class Student {

        private Long applyId;

        private Integer groupId;

        /** 真实姓名 */
        private String realName;

        /** 学号/工号 */
        private String hnusterId;

        /** 性别 */
        private String sex;

        /** 学历 */
        private String eduBackground;

        /** 所在班级 */
        private String classes;
    }

    @Data
    public static class GroupStudent {
        private Integer groupId;
        private String groupName;
        private List<Student> studentList;
    }

}


