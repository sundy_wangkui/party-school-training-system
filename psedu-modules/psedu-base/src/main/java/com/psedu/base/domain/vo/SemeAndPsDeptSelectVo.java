package com.psedu.base.domain.vo;

import lombok.Data;

import java.util.List;

/**
 * 部门和学期选择框
 */
@Data
public class SemeAndPsDeptSelectVo {
    public List<SelectOptionVo<Long>> semeList;
    public List<SelectOptionVo<Long>> deptList;
}
