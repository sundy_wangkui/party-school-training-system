package com.psedu.base.mapper;

import java.util.List;
import com.psedu.base.domain.Group;
import com.psedu.base.domain.vo.DivideGroupParam;
import org.apache.ibatis.annotations.Param;

/**
 * 小组Mapper接口
 * 
 * @author mingyue
 * @date 2022-03-07
 */
public interface GroupMapper 
{
    /**
     * 查询小组
     * 
     * @param groupId 小组主键
     * @return 小组
     */
    public Group selectGroupByGroupId(Long groupId);

    /**
     * 查询小组列表
     * 
     * @param group 小组
     * @return 小组集合
     */
    public List<Group> selectGroupList(Group group);

    /**
     * 新增小组
     * 
     * @param group 小组
     * @return 结果
     */
    public int insertGroup(Group group);

    /**
     * 修改小组
     * 
     * @param group 小组
     * @return 结果
     */
    public int updateGroup(Group group);

    /**
     * 删除小组
     * 
     * @param groupId 小组主键
     * @return 结果
     */
    public int deleteGroupByGroupId(Long groupId);

    /**
     * 批量删除小组
     * 
     * @param groupIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGroupByGroupIds(Integer[] groupIds);

    /**
     * 部门下的分组
     * @param semeId
     * @param deptId
     * @return
     */
    List<Group> deptGroups(@Param("semeId") Long semeId, @Param("deptId") Long deptId);


}
