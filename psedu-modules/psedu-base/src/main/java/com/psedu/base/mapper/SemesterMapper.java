package com.psedu.base.mapper;

import java.util.List;

import com.psedu.base.domain.Apply;
import com.psedu.base.domain.Semester;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 学期Mapper接口
 * 
 * @author mingyue
 * @date 2022-02-09
 */
public interface SemesterMapper extends BaseMapperPlus<SemesterMapper, Semester, Semester>
{
    /**
     * 查询学期
     * 
     * @param semeId 学期主键
     * @return 学期
     */
    public Semester selectSemesterBySemeId(Long semeId);

    /**
     * 查询学期列表
     * 
     * @param semester 学期
     * @return 学期集合
     */
    public List<Semester> selectSemesterList(Semester semester);

    /**
     * 新增学期
     * 
     * @param semester 学期
     * @return 结果
     */
    public int insertSemester(Semester semester);

    /**
     * 修改学期
     * 
     * @param semester 学期
     * @return 结果
     */
    public int updateSemester(Semester semester);

    /**
     * 删除学期
     * 
     * @param semeId 学期主键
     * @return 结果
     */
    public int deleteSemesterBySemeId(Long semeId);

    /**
     * 批量删除学期
     * 
     * @param semeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSemesterBySemeIds(Long[] semeIds);
}
