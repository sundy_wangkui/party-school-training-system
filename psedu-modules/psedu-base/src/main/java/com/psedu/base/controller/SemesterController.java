package com.psedu.base.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.psedu.base.domain.vo.SemeDetailVo;
import com.psedu.base.domain.vo.SimpleSelectSemesterVo;
import com.psedu.base.domain.vo.SimpleSemesterVo;
import com.psedu.common.security.annotation.RequiresLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.base.domain.Semester;
import com.psedu.base.service.ISemesterService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 学期Controller
 * 
 * @author mingyue
 * @date 2022-02-09
 */
@RestController
@RequestMapping("/semester")
public class SemesterController extends BaseController
{
    @Autowired
    private ISemesterService semesterService;

    @RequiresLogin
    @GetMapping("canSignSemester")
    public AjaxResult canSignSemester() {
        return AjaxResult.success(semesterService.canSignSemester());
    }

    /**
     * 获取学期分党校详细信息
     */
    @RequiresPermissions("psedu-base:semester:list")
    @GetMapping(value = "/semesterDetail/{semeId}")
    public AjaxResult getSemesterDetail(@PathVariable("semeId") Long semeId)
    {
        SemeDetailVo semeDetailVo = semesterService.getSemesterDetail(semeId);
        return AjaxResult.success(semeDetailVo);
    }

    /**
     * 查询学期列表
     */
    @RequiresPermissions("psedu-base:semester:list")
    @GetMapping("/list")
    public TableDataInfo list(Semester semester)
    {
        startPage();
        List<Semester> list = semesterService.selectSemesterList(semester);
        return getDataTable(list);
    }

    @RequiresLogin
    @GetMapping("/simpleSelectList")
    public AjaxResult simpleSelectList()
    {
        List<SimpleSelectSemesterVo> list = semesterService.simpleSelectList();
        return AjaxResult.success(list);
    }

    /**
     * 获取学期详细信息
     */
    @RequiresPermissions("psedu-base:semester:query")
    @GetMapping(value = "/{semeId}")
    public AjaxResult getInfo(@PathVariable("semeId") Long semeId)
    {
        return AjaxResult.success(semesterService.selectSemesterBySemeId(semeId));
    }

    /**
     * 新增学期
     */
    @RequiresPermissions("psedu-base:semester:add")
    @Log(title = "学期", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Semester semester)
    {
        return toAjax(semesterService.insertSemester(semester));
    }

    /**
     * 修改学期
     */
    @RequiresPermissions("psedu-base:semester:edit")
    @Log(title = "学期", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Semester semester)
    {
        return toAjax(semesterService.updateSemester(semester));
    }

    /**
     * 修改学期报名状态
     */
    @RequiresPermissions("psedu-base:semester:edit")
    @Log(title = "学期报名状态", businessType = BusinessType.UPDATE)
    @PutMapping("active")
    public AjaxResult editActive(@RequestBody Semester semester)
    {
        return toAjax(semesterService.updateSemesterStatus(semester));
    }

    /**
     * 修改学期报名状态
     */
    @RequiresPermissions("psedu-base:semester:edit")
    @Log(title = "学期评教状态", businessType = BusinessType.UPDATE)
    @PutMapping("allowEvaluate")
    public AjaxResult editAllowEvaluate(@RequestBody Semester semester)
    {
        return toAjax(semesterService.updateSemester(semester));
    }

    /**
     * 删除学期
     */
    @RequiresPermissions("psedu-base:semester:remove")
    @Log(title = "学期", businessType = BusinessType.DELETE)
	@DeleteMapping("/{semeIds}")
    public AjaxResult remove(@PathVariable Long[] semeIds)
    {
        return toAjax(semesterService.deleteSemesterBySemeIds(semeIds));
    }
}
