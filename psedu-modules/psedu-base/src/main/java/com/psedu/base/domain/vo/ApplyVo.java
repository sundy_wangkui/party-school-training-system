package com.psedu.base.domain.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 培训报名对象 apply
 * 
 * @author mingyue
 * @date 2022-03-04
 */
@Data
public class ApplyVo
{
    /** 申请ID */
    private Long applyId;

    /** 真实姓名 */
    private String realName;

    /** 学号/工号 */
    private String hnusterId;

    /** 报名学期ID */
    private Long semeId;

    /** 报名学期ID */
    private Long deptId;

    /** 本人关于党的信息;年月日，列为入党积极分子日期(入党积极分子培训)/入党日期(预备党员培训)/参加入党积极分子培训结业时间(发展对象培训) */
    private String partyData;

    /** 性别 */
    private String sex;

    /** 民族 */
    private String nation;

    /** 籍贯 */
    private String nativePlace;

    /** 学历 */
    private String eduBackground;

    /** 所在班级 */
    private String classes;

    /** 联系电话 */
    private String phone;

    /** E-mail */
    private String email;

    /** QQ */
    private String qq;

    /** 出生日期 */
    private Date birthday;

    /** 首次申请入党日期 */
    private String submitPaperDate;
}
