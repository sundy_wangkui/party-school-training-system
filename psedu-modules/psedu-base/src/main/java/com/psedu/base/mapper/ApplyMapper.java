package com.psedu.base.mapper;

import java.util.List;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.psedu.base.domain.Apply;
import com.psedu.base.domain.vo.DivideGroupParam;
import com.psedu.base.domain.vo.SemesterDeptApplyCountVo;
import com.psedu.base.domain.vo.SimpleUserApplyVo;
import com.psedu.base.domain.vo.StudentTrainDataVo;
import com.psedu.common.mybatis.annotation.DataColumn;
import com.psedu.common.mybatis.annotation.DataPermission;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 培训报名Mapper接口
 * 
 * @author mingyue
 * @date 2022-01-31
 */
@Mapper
public interface ApplyMapper extends BaseMapperPlus<ApplyMapper, Apply, Apply>
{
    List<Apply> testSelectList(Apply apply);

    @DataPermission({
            @DataColumn(key = "deptName", value = "dept_id")
    })
    Page<Apply> selectApplyList(@Param("page") Page<Apply> page, @Param("apply") Apply apply);

    /**
     * 用户申请
     * @param userId 用户ID
     * @return
     */
    @InterceptorIgnore(tenantLine = "true")
    List<SimpleUserApplyVo> userApplyList(Long userId);

    /**
     * 部门学期下的学员
     * @param semeId
     * @param deptId
     * @return 对应学员列表
     */
//    @DataPermission({
//            @DataColumn(key = "deptName", value = "dept_id")
//    })
    List<Apply> selectDeptSemeStudent(@Param("semeId") Long semeId, @Param("deptId") Long deptId);

    /**
     * 分组添加学员
     * @param divideGroupParam
     */
    void divideGroup(@Param("divideGroupParam") DivideGroupParam divideGroupParam);

    /**
     * 查找学期下的合班的所有学员
     * @param semeId 学期ID
     * @param nowMergeClassId 合班ID
     * @return 学期下的合班的所有学员列表
     */
    @InterceptorIgnore(tenantLine = "true")
    List<Apply> selectMergeClassStudent(@Param("semeId") Long semeId, @Param("nowMergeClassId") int nowMergeClassId);

    List<SemesterDeptApplyCountVo> selectSemesterDeptApplyCount(@Param("semeId") Long semeId);

    List<StudentTrainDataVo> selectTrainDataList(Apply apply);

    StudentTrainDataVo getStudentTrainData(@Param("applyId") Long applyId);
}
