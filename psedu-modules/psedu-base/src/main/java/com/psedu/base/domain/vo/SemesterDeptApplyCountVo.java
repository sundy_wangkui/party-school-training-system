package com.psedu.base.domain.vo;

import lombok.Data;

@Data
public class SemesterDeptApplyCountVo {
    private Long deptId;
    private Long count;
}
