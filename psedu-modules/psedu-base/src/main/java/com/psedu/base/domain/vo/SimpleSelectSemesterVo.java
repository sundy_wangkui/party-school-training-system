package com.psedu.base.domain.vo;

import com.psedu.common.core.annotation.Excel;
import lombok.Data;

@Data
public class SimpleSelectSemesterVo {
    private Long semeId;

    private String semeName;
}
