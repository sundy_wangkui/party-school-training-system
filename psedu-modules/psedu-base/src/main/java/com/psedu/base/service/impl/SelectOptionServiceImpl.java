package com.psedu.base.service.impl;

import com.psedu.base.domain.vo.PsDeptAndExamSelectVo;
import com.psedu.base.domain.vo.SelectOptionVo;
import com.psedu.base.domain.vo.SemeAndPsDeptSelectVo;
import com.psedu.base.domain.vo.SimpleSelectSemesterVo;
import com.psedu.base.service.ISemesterService;
import com.psedu.base.service.SelectOptionService;
import com.psedu.exam.api.RemoteExamService;
import com.psedu.exam.api.domain.vo.SimpleExamVo;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.security.utils.SecurityUtils;
import com.psedu.system.api.RemoteDataScopeService;
import com.psedu.system.api.RemoteDeptService;
import com.psedu.system.api.domain.vo.SysSimpleDeptVo;
import com.psedu.system.api.model.LoginUser;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SelectOptionServiceImpl implements SelectOptionService {

    @Autowired
    private RemoteDeptService remoteDeptService;
    @Autowired
    private ISemesterService semesterService;
    @Autowired
    private RemoteExamService remoteExamService;

    @DubboReference
    private RemoteDataScopeService remoteDataScopeService;

    @Override
    public SemeAndPsDeptSelectVo getSemeAndPsDept() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long deptId = loginUser.getDeptId();
        // TODO 默认
        List<SimpleSelectSemesterVo> semesterList = semesterService.simpleSelectList();
        SemeAndPsDeptSelectVo semeAndPsDeptSelectVo = new SemeAndPsDeptSelectVo();
        List<SelectOptionVo<Long>> resSemeList = new ArrayList<>();
        semeAndPsDeptSelectVo.setSemeList(resSemeList);
        // 学期
        for (SimpleSelectSemesterVo simpleSelectSemesterVo : semesterList) {
            SelectOptionVo<Long> selectOptionVo = new SelectOptionVo<>();
            selectOptionVo.setChecked(false)
                .setDisabled(false)
                .setKey(simpleSelectSemesterVo.getSemeId())
                .setValue(simpleSelectSemesterVo.getSemeName());
            resSemeList.add(selectOptionVo);
        }
        // 部门
        List<SelectOptionVo<Long>> resDeptList = new ArrayList<>();
        semeAndPsDeptSelectVo.setDeptList(resDeptList);
        getPsDeptSelectOptionList(resDeptList, deptId);
        return semeAndPsDeptSelectVo;
    }

    private void getPsDeptSelectOptionList(List<SelectOptionVo<Long>> resDeptList, Long deptId) {
        R<List<SysSimpleDeptVo>> deptListResp = remoteDeptService.getPsDeptAndChildDeptList(deptId);
        List<SysSimpleDeptVo> sysSimpleDeptList = deptListResp.getData();
        for (SysSimpleDeptVo sysSimpleDeptVo : sysSimpleDeptList) {
            SelectOptionVo<Long> selectOptionVo = new SelectOptionVo<>();
            selectOptionVo.setChecked(false)
                    .setDisabled(false)
                    .setKey(sysSimpleDeptVo.getDeptId())
                    .setValue(sysSimpleDeptVo.getDeptName());
            resDeptList.add(selectOptionVo);
        }
    }

    @Override
    public PsDeptAndExamSelectVo getPsDeptAndExam() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long deptId = loginUser.getDeptId();
        PsDeptAndExamSelectVo psDeptAndExamSelectVo = new PsDeptAndExamSelectVo();
        // 部门
        List<SelectOptionVo<Long>> resDeptList = new ArrayList<>();
        psDeptAndExamSelectVo.setDeptList(resDeptList);;
        getPsDeptSelectOptionList(resDeptList, deptId);
        // 考试
        List<SelectOptionVo<Long>> resExamList = getSelectOptionVos();
        psDeptAndExamSelectVo.setExamList(resExamList);
        return psDeptAndExamSelectVo;
    }

    private List<SelectOptionVo<Long>> getSelectOptionVos() {
        R<List<SimpleExamVo>> examSelectOptionsRes =
                remoteExamService.getExamSelectOptions(SecurityConstants.INNER);
        List<SimpleExamVo> simpleExamVoList = examSelectOptionsRes.getData();
        return simpleExamVoList.stream().map(simpleExamVo -> {
            SelectOptionVo<Long> selectOptionVo = new SelectOptionVo<>();
            selectOptionVo.setChecked(false)
                    .setDisabled(false)
                    .setKey(simpleExamVo.getExamId())
                    .setValue(simpleExamVo.getExamName());
            return selectOptionVo;
        }).collect(Collectors.toList());
    }
}
