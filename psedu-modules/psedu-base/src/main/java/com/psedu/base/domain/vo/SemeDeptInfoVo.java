package com.psedu.base.domain.vo;

import com.psedu.base.domain.SemesterDept;
import lombok.Data;

/**
 * @author Mingyue
 */
@Data
public class SemeDeptInfoVo {

    /** 学期部门负责人数据*/
    private SemesterDept semesterDept;

    /** 部门名称 */
    private String deptName;

    /** 申请数量*/
    private Long applyCount;

    /** 审核数量*/
    private Long checkedCount;

    /** 分组人数*/
    private Long groupUserCount;

    /** 注册人数*/
    private Long deptRegisterCount;

}
