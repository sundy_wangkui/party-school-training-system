package com.psedu.base.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

import java.util.Date;

/**
 * 学期对象 psedu_semester
 * 
 * @author mingyue
 * @date 2022-02-09
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "psedu_semester")
public class Semester extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学期ID */
    @TableId(type = IdType.AUTO)
    private Long semeId;

    /** 学期名 */
    @Excel(name = "学期名")
    private String semeName;

    /** 培训对象;0入党积极分子，1发展对象，2预备党员 */
    @Excel(name = "培训对象;0入党积极分子，1发展对象，2预备党员")
    private Integer trainObject;

    /** 允许报名 */
    private Integer active;

    /** 允许评教 */
    private Integer allowEvaluate;

    /** 已删除 */
    private Integer deleted;

    /** 开始时间 */
    private Date startTime;

    /** 结束时间 */
    private Date endTime;

    /** 证书建议 */
    private String certificateComment;

    /** 党校建议*/
    private String partyComment;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
