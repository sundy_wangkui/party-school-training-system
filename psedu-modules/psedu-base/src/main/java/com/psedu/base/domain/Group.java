package com.psedu.base.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 小组对象 psedu_group
 * 
 * @author mingyue
 * @date 2022-03-07
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class Group extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分组ID */
    private Integer groupId;

    /** 所属学期 */
    private Long semeId;

    /** 所属分党校 */
    private Long deptId;

    /** 分组名称 */
    @Excel(name = "分组名称")
    private String groupName;

}
