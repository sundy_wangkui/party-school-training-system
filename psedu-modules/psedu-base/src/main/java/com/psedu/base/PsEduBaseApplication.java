package com.psedu.base;

import com.alibaba.cloud.dubbo.autoconfigure.DubboServiceAutoConfiguration;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.psedu.common.dubbo.properties.DubboCustomProperties;
import com.psedu.common.security.config.ApplicationConfig;
import com.psedu.common.security.feign.FeignAutoConfiguration;
import com.psedu.common.security.utils.SecurityUtils;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.apache.dubbo.spring.boot.autoconfigure.DubboAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import com.psedu.common.security.annotation.EnableCustomConfig;
import com.psedu.common.security.annotation.EnableRyFeignClients;
import com.psedu.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringCloudApplication
@EnableDubbo
@MapperScan("com.psedu.base.mapper")
//@SpringBootApplication(scanBasePackages = {"com.psedu.system.api.factory", "com.psedu.system.api"})
public class PsEduBaseApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(PsEduBaseApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  党校基础模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
