package com.psedu.base.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 学期分党校对象 psedu_semester_dept
 * 
 * @author mingyue
 * @date 2022-03-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "psedu_semester_dept", excludeProperty = {
        "createBy", "updateBy", "createTime", "updateTime"
})
public class SemesterDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分党校ID */
    private Long deptId;

    /** 学期ID */
    private Long semeId;

    /** 分班ID */
    private Long mergeClassId;

    /** 管理员名字 */
    @Excel(name = "管理员名字")
    private String managerName;

    /** 管理员电话 */
    @Excel(name = "管理员电话")
    private String managerPhone;

    /** 管理员QQ */
    @Excel(name = "管理员QQ")
    private String managerQq;

    /** 班长名字 */
    @Excel(name = "班长名字")
    private String monitorName;

    /** 班长电话 */
    @Excel(name = "班长电话")
    private String monitorPhone;

    /** 班长QQ */
    @Excel(name = "班长QQ")
    private String monitorQq;

    /** 分党校校长名 */
    @Excel(name = "分党校校长名")
    private String teacherName;

    /** 开启报名;0关闭，1开启 */
    @Excel(name = "开启报名;0关闭，1开启")
    private Integer started;
}
