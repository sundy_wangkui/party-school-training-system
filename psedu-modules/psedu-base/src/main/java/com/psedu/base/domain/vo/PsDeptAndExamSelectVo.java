package com.psedu.base.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class PsDeptAndExamSelectVo {
    public List<SelectOptionVo<Long>> deptList;
    public List<SelectOptionVo<Long>> examList;
}
