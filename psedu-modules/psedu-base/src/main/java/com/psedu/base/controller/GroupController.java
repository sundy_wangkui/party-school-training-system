package com.psedu.base.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.psedu.base.domain.vo.DivideGroupParam;
import com.psedu.base.domain.vo.GroupUserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.base.domain.Group;
import com.psedu.base.service.IGroupService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 小组Controller
 * 
 * @author mingyue
 * @date 2022-03-07
 */
@RestController
@RequestMapping("/group")
public class GroupController extends BaseController
{
    @Autowired
    private IGroupService groupService;

    /**
     * 查询小组列表
     */
    @RequiresPermissions("psedu-base:group:list")
    @GetMapping("/list")
    public TableDataInfo list(Group group)
    {
        startPage();
        List<Group> list = groupService.selectGroupList(group);
        return getDataTable(list);
    }

    /**
     * 导出小组列表
     */
    @RequiresPermissions("psedu-base:group:export")
    @Log(title = "小组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Group group)
    {
        List<Group> list = groupService.selectGroupList(group);
        ExcelUtil<Group> util = new ExcelUtil<Group>(Group.class);
        util.exportExcel(response, list, "小组数据");
    }

    /**
     * 获取小组详细信息
     */
    @RequiresPermissions("psedu-base:group:query")
    @GetMapping(value = "/{groupId}")
    public AjaxResult getInfo(@PathVariable("groupId") Long groupId)
    {
        return AjaxResult.success(groupService.selectGroupByGroupId(groupId));
    }

    /**
     * 获取小组详细信息
     */
    @RequiresPermissions("psedu-base:group:query")
    @GetMapping(value = "/userGroup/{semeId}/{deptId}")
    public AjaxResult getUserGroupInfoBySemeId(
                @PathVariable("semeId") Long semeId,
                @PathVariable("deptId") Long deptId) {
        return AjaxResult.success(groupService.getUserGroupInfoBySemeId(semeId, deptId));
    }

    /**
     * 新增小组
     */
    @RequiresPermissions("psedu-base:group:add")
    @Log(title = "小组", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Group group)
    {
        return toAjax(groupService.insertGroup(group));
    }

    /**
     * 修改小组
     */
    @RequiresPermissions("psedu-base:group:edit")
    @Log(title = "小组", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Group group)
    {
        return toAjax(groupService.updateGroup(group));
    }

    @RequiresPermissions("psedu-base:group:edit")
    @Log(title = "学员分组", businessType = BusinessType.UPDATE)
    @PutMapping("/studentDivideGroup")
    public AjaxResult studentDivideGroup(@RequestBody DivideGroupParam divideGroupParam) {
        groupService.studentDivideGroup(divideGroupParam);
        return success();
    }

    /**
     * 删除小组
     */
    @RequiresPermissions("psedu-base:group:remove")
    @Log(title = "小组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{groupIds}")
    public AjaxResult remove(@PathVariable Integer[] groupIds)
    {
        return toAjax(groupService.deleteGroupByGroupIds(groupIds));
    }
}
