package com.psedu.base.domain.vo;

import lombok.Data;

@Data
public class SignRecordQueryVo {
    private Long courseId;
    private String nickName;
    private String hunsterId;
}
