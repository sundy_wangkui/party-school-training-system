package com.psedu.base.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 发起签到对象 psedu_sign_launch
 * 
 * @author mingyue
 * @date 2022-05-16
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "psedu_sign_launch", excludeProperty = {
        "createBy", "updateBy", "updateTime"
})
public class SignLaunch extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 签到ID */
    @Excel(name = "签到ID")
    @TableId(type = IdType.AUTO)
    private Long signLaunchId;

    /** 课程ID */
    @Excel(name = "课程ID")
    private Long courseId;

    private String signTitle;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /** 签到纬度 */
    @Excel(name = "签到纬度")
    private Double latitude;

    /** 签到经度 */
    @Excel(name = "签到经度")
    private Double longitude;

    /** 限制精度 */
    private Double accuracy;

    /** 已删除;0未删除，1已删除 */
    private Integer deleted;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
