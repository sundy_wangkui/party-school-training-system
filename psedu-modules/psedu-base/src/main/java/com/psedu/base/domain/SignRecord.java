package com.psedu.base.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 签到记录对象 psedu_sign_record
 * 
 * @author mingyue
 * @date 2022-05-16
 */
@Data
@TableName(value = "psedu_sign_record", excludeProperty = {
        "createBy", "updateBy", "updateTime", "createTime"
})
public class SignRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 签到记录ID */
    @TableId(type = IdType.AUTO)
    private Long signRecordId;

    /** 签到场次ID */
    @Excel(name = "签到场次ID")
    private Long signLaunchId;

    /** 签到用户ID */
    @Excel(name = "签到用户ID")
    private Long pseduUserId;

    /** 签到经度 */
    @Excel(name = "签到经度")
    private Double longitude;

    /** 签到纬度 */
    @Excel(name = "签到纬度")
    private Double latitude;

    /** 精度 */
    @Excel(name = "精度")
    private Double accuracy;
}
