package com.psedu.base.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 培训报名对象 psedu_apply
 * 
 * @author mingyue
 * @date 2022-01-31
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "psedu_apply")
@AllArgsConstructor
@NoArgsConstructor
public class Apply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 申请ID */
    @TableId(type = IdType.AUTO)
    private Long applyId;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String realName;

    /** 学号/工号 */
    private String hnusterId;

    /** 党校用户ID */
    @Excel(name = "党校用户ID")
    private Long psUserId;

    /** 报名学期ID */
    @Excel(name = "报名学期ID")
    private Long semeId;

    /** 部门ID */
    private Long deptId;

    /** 本人关于党的信息;年月日，列为入党积极分子日期(入党积极分子培训)/入党日期(预备党员培训)/参加入党积极分子培训结业时间(发展对象培训) */
    @Excel(name = "本人关于党的信息;年月日，列为入党积极分子日期(入党积极分子培训)/入党日期(预备党员培训)/参加入党积极分子培训结业时间(发展对象培训)")
    private String partyData;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String nativePlace;

    /** 学历 */
    @Excel(name = "学历")
    private String eduBackground;

    /** 所在班级 */
    @Excel(name = "所在班级")
    private String classes;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phone;

    /** E-mail */
    @Excel(name = "E-mail")
    private String email;

    /** QQ */
    @Excel(name = "QQ")
    private String qq;

    /** 出生日期 */
    private Date birthday;

    /** 首次申请入党日期 */
    @Excel(name = "首次申请入党日期")
    private String submitPaperDate;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private Integer checked;

    /** 结业考试得分 */
    private BigDecimal scores;

    /** 分组ID */
    private Integer groupId;

    /** 座位号 */
    private Long seatNum;

    /** 个人小结 */
    private String summary;

    /** 逻辑删除 **/
    private Integer deleted;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
