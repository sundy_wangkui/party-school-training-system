package com.psedu.base.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class SemeDetailVo {
    /** 申请数量*/
    private Long applyCount;

    /** 审核数量*/
    private Long checkedCount;

    /** 分组人数*/
    private Long groupUserCount;

    /** 注册人数*/
    private Long deptRegisterCount;

    private DeptApplyData deptApplyData;

    @Data
    public static class DeptApplyData {
        private List<Long> applyCountList;
        private List<String> deptNameList;
    }
}
