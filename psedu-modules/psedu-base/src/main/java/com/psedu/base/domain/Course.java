package com.psedu.base.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.psedu.common.core.annotation.Excel;
import com.psedu.common.core.web.domain.BaseEntity;

/**
 * 课程对象 psedu_course
 * 
 * @author mingyue
 * @date 2022-02-10
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "psedu_course")
public class Course extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 课程ID */
    @TableId(type = IdType.AUTO)
    private Long courseId;

    /** 课程名称 */
    private String courseTitle;

    /** 培训学期 */
    private Long semeId;

    /** 所属部门ID */
    private Long deptId;

    /** 所属合班ID */
    private Long mergeClassId;

    /** 封面图片链接 */
    private String imageUrl;

    /** 视频链接 */
    private String videoUrl;

    /** 实现的ID */
    private Long realizeCourseId;

    /** 上课地点 */
    private String classroom;

    /** 课程内容 */
    private String content;

    /** 排序 */
    private Long orderNum;

    /** 授课教师 */
    private String teacher;

    /** 需要评教 */
    private Integer evaluate;

    /** 课程开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    /** 已删除 */
    private Integer deleted;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
