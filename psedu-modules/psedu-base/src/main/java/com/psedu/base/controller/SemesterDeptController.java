package com.psedu.base.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.base.domain.SemesterDept;
import com.psedu.base.service.ISemesterDeptService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;
import com.psedu.common.core.web.page.TableDataInfo;

/**
 * 学期分党校Controller
 * 
 * @author mingyue
 * @date 2022-03-12
 */
@RestController
@RequestMapping("/semesterDept")
public class SemesterDeptController extends BaseController
{
    @Autowired
    private ISemesterDeptService semesterDeptService;

    /**
     * 获取学期分党校详细信息
     */
    @RequiresPermissions("psedu-base:semesterDept:query")
    @GetMapping(value = "/{semeId}/{deptId}")
    public AjaxResult getSemeDeptInfo(@PathVariable("semeId") Long semeId, @PathVariable("deptId") Long deptId)
    {
        return AjaxResult.success(semesterDeptService.getSemeDeptInfo(semeId, deptId));
    }

    /**
     * 修改学期分党校
     */
    @RequiresPermissions("psedu-base:semesterDept:edit")
    @Log(title = "学期分党校", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult saveSemesterDept(@RequestBody SemesterDept semesterDept)
    {
        return toAjax(semesterDeptService.saveSemesterDept(semesterDept));
    }
}
