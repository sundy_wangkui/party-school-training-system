package com.psedu.base.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.psedu.base.domain.vo.ApplyVo;
import com.psedu.base.domain.vo.SimpleUserApplyVo;
import com.psedu.common.mybatis.core.page.PageQuery;
import com.psedu.common.mybatis.core.page.TableDataInfo;
import com.psedu.common.security.annotation.RequiresLogin;
import com.psedu.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.psedu.common.log.annotation.Log;
import com.psedu.common.log.enums.BusinessType;
import com.psedu.common.security.annotation.RequiresPermissions;
import com.psedu.base.domain.Apply;
import com.psedu.base.service.IApplyService;
import com.psedu.common.core.web.controller.BaseController;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.core.utils.poi.ExcelUtil;

/**
 * 培训报名Controller
 * 
 * @author mingyue
 * @date 2022-01-31
 */
@RestController
@RequestMapping("/apply")
public class ApplyController extends BaseController
{
    @Autowired
    private IApplyService applyService;

    @RequiresLogin
    @GetMapping("/userApply")
    public AjaxResult userApply() {
        Long userId = SecurityUtils.getUserId();
        return AjaxResult.success(applyService.userApply(userId));
    }

    @RequiresLogin
    @GetMapping(value = "/detail/{applyId}")
    public AjaxResult getDetail(@PathVariable("applyId") Long applyId)
    {
        return AjaxResult.success(applyService.getApplyDetailByApplyId(applyId));
    }

    /**
     * 用户 添加/修改 申请
     * @param applyVo
     * @return
     */
    @RequiresLogin
    @PostMapping("/submit")
    public AjaxResult submitApply(@RequestBody ApplyVo applyVo) {
        return toAjax(applyService.submitApply(applyVo));
    }


    @RequiresLogin
    @GetMapping("/summary/{applyId}")
    public AjaxResult userApplySummary(@PathVariable Long applyId) {
        Long userId = SecurityUtils.getUserId();
        return AjaxResult.success("操作成功", applyService.userApplySummary(userId, applyId));
    }

    /**
     * 查询培训报名列表
     */
    @RequiresPermissions("psedu-base:apply:list")
    @GetMapping("/list")
    public TableDataInfo<Apply> list(Apply apply, PageQuery pageQuery)
    {
//        return applyService.selectApplyList(apply, pageQuery);
        return applyService.selectApplyList(pageQuery, apply);
    }

//    /**
//     * 导出培训报名列表
//     */
//    @RequiresPermissions("psedu-base:apply:export")
//    @Log(title = "培训报名", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, PageQuery pageQuery, Apply apply)
//    {
//        List<Apply> list = applyService.selectApplyList(pageQuery, apply);
//        ExcelUtil<Apply> util = new ExcelUtil<Apply>(Apply.class);
//        util.exportExcel(response, list, "培训报名数据");
//    }

    /**
     * 获取培训报名详细信息
     */
    @RequiresPermissions("psedu-base:apply:query")
    @GetMapping(value = "/{applyId}")
    public AjaxResult getInfo(@PathVariable("applyId") Long applyId)
    {
        return AjaxResult.success(applyService.selectApplyByApplyId(applyId));
    }

    /**
     * 新增培训报名
     */
    @RequiresPermissions("psedu-base:apply:add")
    @Log(title = "培训报名", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Apply apply)
    {
        return toAjax(applyService.insertApply(apply));
    }

    /**
     * 修改培训报名
     */
    @RequiresPermissions("psedu-base:apply:edit")
    @Log(title = "培训报名", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Apply apply)
    {
        return toAjax(applyService.updateApply(apply));
    }

    /**
     * 通过培训报名
     */
    @RequiresPermissions("psedu-base:apply:edit")
    @Log(title = "培训报名", businessType = BusinessType.UPDATE)
    @PutMapping("/pass/{applyId}")
    public AjaxResult applyPass(@PathVariable("applyId") Long applyId)
    {
        return toAjax(applyService.applyPass(applyId));
    }

    /**
     * 删除培训报名
     */
    @RequiresPermissions("psedu-base:apply:remove")
    @Log(title = "培训报名", businessType = BusinessType.DELETE)
	@DeleteMapping("/{applyIds}")
    public AjaxResult remove(@PathVariable Long[] applyIds)
    {
        return toAjax(applyService.deleteApplyByApplyIds(applyIds));
    }
}
