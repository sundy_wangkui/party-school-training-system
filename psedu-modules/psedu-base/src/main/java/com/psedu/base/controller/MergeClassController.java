package com.psedu.base.controller;

import com.psedu.base.domain.vo.MergeDeptInfo;
import com.psedu.base.domain.vo.SaveMergeClassVo;
import com.psedu.base.service.MergeClassService;

import com.psedu.common.core.domain.R;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mergeClass")
public class MergeClassController {
    @Autowired
    private MergeClassService mergeClassService;

    @RequiresPermissions("mergeClass:info:get")
    @GetMapping("/info/{semeId}")
    public R<MergeDeptInfo> getMergeClass(@PathVariable("semeId") Long semeId) {
        MergeDeptInfo mergeDeptInfo = mergeClassService.getMergeDeptInfo(semeId);
        return R.ok(mergeDeptInfo);
    }

    @RequiresPermissions("mergeClass:info:save")
    @PutMapping("/info")
    public AjaxResult updateMergeClass(@RequestBody SaveMergeClassVo saveMergeClassVo) {
        mergeClassService.saveMergeDeptInfo(saveMergeClassVo);
        return AjaxResult.success();
    }

}
