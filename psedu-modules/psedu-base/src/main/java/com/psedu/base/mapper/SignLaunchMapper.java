package com.psedu.base.mapper;

import java.util.List;

import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;
import com.psedu.base.domain.Semester;
import com.psedu.base.domain.SignLaunch;

/**
 * 发起签到Mapper接口
 * 
 * @author mingyue
 * @date 2022-05-16
 */
public interface SignLaunchMapper  extends BaseMapperPlus<SignLaunchMapper, SignLaunch, SignLaunch>
{
    /**
     * 查询发起签到
     * 
     * @param signLaunchId 发起签到主键
     * @return 发起签到
     */
    public SignLaunch selectSignLaunchBySignLaunchId(Long signLaunchId);

    /**
     * 查询发起签到列表
     * 
     * @param signLaunch 发起签到
     * @return 发起签到集合
     */
    public List<SignLaunch> selectSignLaunchList(SignLaunch signLaunch);

    /**
     * 新增发起签到
     * 
     * @param signLaunch 发起签到
     * @return 结果
     */
    public int insertSignLaunch(SignLaunch signLaunch);

    /**
     * 修改发起签到
     * 
     * @param signLaunch 发起签到
     * @return 结果
     */
    public int updateSignLaunch(SignLaunch signLaunch);

    /**
     * 删除发起签到
     * 
     * @param signLaunchId 发起签到主键
     * @return 结果
     */
    public int deleteSignLaunchBySignLaunchId(Long signLaunchId);

    /**
     * 批量删除发起签到
     * 
     * @param signLaunchIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSignLaunchBySignLaunchIds(Long[] signLaunchIds);
}
