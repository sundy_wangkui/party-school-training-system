package com.psedu.base.service;

import java.util.List;
import com.psedu.base.domain.SignRecord;
import com.psedu.base.domain.vo.SignRecordQueryVo;
import com.psedu.base.domain.vo.SignRecordVo;
import com.psedu.base.domain.vo.StudentPositionVo;
import com.psedu.base.domain.vo.UnSignStudentVo;

/**
 * 签到记录Service接口
 * 
 * @author mingyue
 * @date 2022-05-16
 */
public interface ISignRecordService 
{
    /**
     * 查询签到记录
     * 
     * @param signRecordId 签到记录主键
     * @return 签到记录
     */
    public SignRecord selectSignRecordBySignRecordId(Long signRecordId);

    /**
     * 查询签到记录列表
     * 
     * @param signRecord 签到记录
     * @return 签到记录集合
     */
    public List<SignRecordVo> selectSignRecordList(SignRecordQueryVo signRecord);

    /**
     * 新增签到记录
     * 
     * @param signRecord 签到记录
     * @return 结果
     */
    public int insertSignRecord(SignRecord signRecord);

    /**
     * 修改签到记录
     * 
     * @param signRecord 签到记录
     * @return 结果
     */
    public int updateSignRecord(SignRecord signRecord);

    /**
     * 批量删除签到记录
     * 
     * @param signRecordIds 需要删除的签到记录主键集合
     * @return 结果
     */
    public int deleteSignRecordBySignRecordIds(Long[] signRecordIds);

    /**
     * 删除签到记录信息
     * 
     * @param signRecordId 签到记录主键
     * @return 结果
     */
    public int deleteSignRecordBySignRecordId(Long signRecordId);

    void studentSign(Long userId, Long signLaunchId, StudentPositionVo studentPositionVo);

    List<UnSignStudentVo> selectUnSignStudentList(SignRecordQueryVo signRecordQueryVo);
}
