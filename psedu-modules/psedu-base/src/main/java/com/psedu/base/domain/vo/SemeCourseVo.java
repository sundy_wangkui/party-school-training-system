package com.psedu.base.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class SemeCourseVo {
    private Long semeId;
    private String semeName;
    private Long deptId;
    private String deptName;
    private List<CourseSimpleVo> courseSimpleVoList;
}
