package com.psedu.base.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.google.common.collect.Lists;
import com.psedu.base.constant.Constants;
import com.psedu.base.domain.Apply;
import com.psedu.base.domain.Semester;
import com.psedu.base.domain.vo.SemeDeptInfoVo;
import com.psedu.base.mapper.ApplyMapper;
import com.psedu.base.mapper.SemesterMapper;
import com.psedu.common.core.constant.HttpStatus;
import com.psedu.common.core.constant.SecurityConstants;
import com.psedu.common.core.domain.R;
import com.psedu.common.core.exception.ServiceException;
import com.psedu.system.api.RemoteDeptService;
import com.psedu.system.api.RemoteUserService;
import com.psedu.system.api.domain.vo.SysSimpleDeptVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.psedu.base.mapper.SemesterDeptMapper;
import com.psedu.base.domain.SemesterDept;
import com.psedu.base.service.ISemesterDeptService;

import static com.psedu.base.constant.ResponseConstants.FORBIDDEN;

/**
 * 学期分党校Service业务层处理
 * 
 * @author mingyue
 * @date 2022-03-12
 */
@Service
public class SemesterDeptServiceImpl implements ISemesterDeptService
{

    private static final Logger log = LoggerFactory.getLogger(SemesterDeptServiceImpl.class);

    @Autowired
    private SemesterDeptMapper semesterDeptMapper;

    @Autowired
    private ApplyMapper applyMapper;

    @Autowired
    private RemoteUserService remoteUserService;

    @Autowired
    private RemoteDeptService remoteDeptService;

    @Autowired
    private SemesterMapper semesterMapper;


    /**
     * 修改学期分党校
     * 
     * @param semesterDept 学期分党校
     * @return 结果
     */
    @Override
    public int saveSemesterDept(SemesterDept semesterDept)
    {
        Long deptId = semesterDept.getDeptId();
        Long semeId = semesterDept.getSemeId();
        Semester semester = semesterMapper.selectById(semeId);
        if(Constants.SEMESTER_ACTIVE != semester.getActive()) {
            throw new ServiceException("学期已经停止报名", FORBIDDEN);
        }
        SemesterDept oldSemesterDept = semesterDeptMapper.selectSemesterDept(semeId, deptId);
        if( oldSemesterDept == null ) {
            return semesterDeptMapper.insert(semesterDept);
        } else {
            return semesterDeptMapper.update(
                    semesterDept,
                    new LambdaUpdateWrapper<SemesterDept>()
                        .eq(SemesterDept::getDeptId, deptId)
                        .eq(SemesterDept::getSemeId, semeId)
            );
        }
    }

    @Override
    public SemesterDept selectSemesterDept(Long semeId, Long deptId) {
        return semesterDeptMapper.selectSemesterDept(semeId, deptId);
    }

    @Override
    public SemeDeptInfoVo getSemeDeptInfo(Long semeId, Long deptId) {
        SemeDeptInfoVo semeDeptInfoVo = new SemeDeptInfoVo();
        semeDeptInfoVo.setSemesterDept( this.selectSemesterDept(semeId, deptId) );
        // 报名数量
        Long applyCount = applyMapper.selectCount(
                new LambdaQueryWrapper<Apply>()
                        .eq(Apply::getSemeId, semeId)
                        .eq(Apply::getDeptId, deptId)
        );
        semeDeptInfoVo.setApplyCount(applyCount);

        // 已审核人数
        Long checkedCount = applyMapper.selectCount(
                                    new LambdaQueryWrapper<Apply>()
                                            .eq(Apply::getSemeId, semeId)
                                            .eq(Apply::getDeptId, deptId)
                                            .ne(Apply::getChecked, Constants.APPLY_WAIT)
                                );
        semeDeptInfoVo.setCheckedCount(checkedCount);

        // 已分组人数
        Long groupUserCount = applyMapper.selectCount(
                new LambdaQueryWrapper<Apply>()
                        .eq(Apply::getSemeId, semeId)
                        .eq(Apply::getDeptId, deptId)
                        .ne(Apply::getGroupId, 0)
        );
        semeDeptInfoVo.setGroupUserCount(groupUserCount);

        // 注册人数
        try {
            R<Long> registerUserCountR = remoteUserService.getPsStudentNumByDeptId(deptId, SecurityConstants.INNER);
            Long registerUserCount = registerUserCountR.getData();
            semeDeptInfoVo.setDeptRegisterCount(registerUserCount);
        } catch (Exception e) {
            log.error(e.getMessage());
            semeDeptInfoVo.setDeptRegisterCount(-1L);
        }

        // 部门名称
        try {
            R<List<SysSimpleDeptVo>> sysDeptListR = remoteDeptService.getDeptNameByDeptId(
                    Collections.singletonList(deptId),
                    SecurityConstants.INNER
            );
            List<SysSimpleDeptVo> sysDeptList = sysDeptListR.getData();
            SysSimpleDeptVo sysSimpleDeptVo = sysDeptList.get(0);
            semeDeptInfoVo.setDeptName(sysSimpleDeptVo.getDeptName());
        } catch (Exception e) {
            log.error(e.getMessage());
            semeDeptInfoVo.setDeptName("ERROR");
        }

        return semeDeptInfoVo;
    }

}
