package com.psedu.base.constant;

import com.psedu.common.core.constant.HttpStatus;

public class ResponseConstants {
    /**
     * 访问受限，授权过期
     */
    public static final int FORBIDDEN = HttpStatus.FORBIDDEN *10 +1;
}
