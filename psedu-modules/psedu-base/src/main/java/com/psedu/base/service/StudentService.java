package com.psedu.base.service;

import com.psedu.base.api.domain.dto.StudentDTO;
import com.psedu.base.domain.Apply;
import com.psedu.base.domain.vo.StudentTrainDataVo;
import com.psedu.exam.api.domain.vo.ExamBo;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

public interface StudentService {

    List<ExamBo> getStudentExamList();

    StudentDTO getStudentInfo(Long semesterId, Long psUserId);

    int updateExamStudentScore(Long applyId, BigDecimal score);

    List<Long> getAllPassSemester(Long psUserId);

    List<StudentTrainDataVo> getTrainDataList(Apply apply);

    void downloadCertificate(Long semeId, Long deptId, HttpServletResponse httpResponse);

    StudentTrainDataVo studentTrainData(Long applyId);
}
