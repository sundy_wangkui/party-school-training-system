package com.psedu.base.domain.vo;

import lombok.Data;

@Data
public class SemesterDeptVo {
    /** 分党校ID */
    private Long deptId;

    /** 分党校名称 */
    private String deptName;

    /** 学期ID */
    private Long semeId;

    /** 分班ID */
    private Long mergeClassId;

    /** 学员数量 */
    private Long studentNum;
}
