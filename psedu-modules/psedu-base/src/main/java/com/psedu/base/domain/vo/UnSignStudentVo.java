package com.psedu.base.domain.vo;

import lombok.Data;

@Data
public class UnSignStudentVo {
    private String hnusterId;
    private Long psUserId;
    private String nickName;
}
