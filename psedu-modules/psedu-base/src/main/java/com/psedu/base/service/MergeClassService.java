package com.psedu.base.service;

import com.psedu.base.domain.vo.MergeDeptInfo;
import com.psedu.base.domain.vo.SaveMergeClassVo;

/**
 * 座位服务
 */
public interface MergeClassService {


    /**
     * 获取学期分班
     * @param semeId 学期ID
     * @return 分班情况
     */
    MergeDeptInfo getMergeDeptInfo(Long semeId);


    /**
     * 保存合班情况
     * @param saveMergeClassVo 修改数据
     */
    void saveMergeDeptInfo(SaveMergeClassVo saveMergeClassVo);
}
