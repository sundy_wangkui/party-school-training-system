package com.psedu.base.service;

import java.util.List;
import com.psedu.base.domain.SemesterDept;
import com.psedu.base.domain.vo.SemeDeptInfoVo;

/**
 * 学期分党校Service接口
 * 
 * @author mingyue
 * @date 2022-03-12
 */
public interface ISemesterDeptService 
{
    /**
     * 修改学期分党校
     * 
     * @param semesterDept 学期分党校
     * @return 结果
     */
    public int saveSemesterDept(SemesterDept semesterDept);

    /**
     * 获取学期部门信息
     * @param semeId 学期ID
     * @param deptId 部门ID
     * @return 学期部门信息
     */
    public SemesterDept selectSemesterDept(Long semeId, Long deptId);

    /**
     * 获取学期部门数据
     * @param semeId 学期ID
     * @param deptId 部门ID
     * @return 学期部门数据
     */
    public SemeDeptInfoVo getSemeDeptInfo(Long semeId, Long deptId);
}
