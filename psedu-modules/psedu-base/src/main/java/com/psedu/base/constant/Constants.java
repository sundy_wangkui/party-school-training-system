package com.psedu.base.constant;

public class Constants {
    public static final long ADMIN_DEPT_ID = 100L;
    public static final long MIN_COLLEGE_DEPT_ID = 200L;
    public static final long MAX_COLLEGE_DEPT_ID = 250L;

    /** 学期状态 */
    public static final int SEMESTER_INACTIVE = 0;
    public static final int SEMESTER_ACTIVE = 1;
    /** 0未审核，1通过，2不通过 */
    public static final int APPLY_WAIT = 0;
    public static final int APPLY_PASS = 1;
    public static final int APPLY_NO_PASS = 2;
    /** 分班最大数量 */
    public static final int MERGE_CLASS_COUNT = 4;

    public static final int PASS_SCORE = 60;

    public static final int TRAIN_OBJECT_ACT = 0;
    public static final int TRAIN_OBJECT_DEV = 1;
    public static final int TRAIN_OBJECT_PRE = 2;

}
