package com.psedu.base.domain.vo;

import com.psedu.base.domain.SemesterDept;
import lombok.Data;

import java.util.List;

@Data
public class MergeDeptInfo {

    private String trainName;

    /** 等待分班的分党校 */
    private List<SemesterDeptVo> waitDeptList;

    /** 已经分组的分党校 */
    private List<List<SemesterDeptVo>> mergeClassList;

}
