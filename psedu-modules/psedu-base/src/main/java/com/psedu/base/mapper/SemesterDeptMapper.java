package com.psedu.base.mapper;

import java.util.List;

import com.psedu.base.domain.Apply;
import com.psedu.base.domain.SemesterDept;
import com.psedu.common.mybatis.annotation.DataColumn;
import com.psedu.common.mybatis.annotation.DataPermission;
import com.psedu.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 学期分党校Mapper接口
 * 
 * @author mingyue
 * @date 2022-03-12
 */
public interface SemesterDeptMapper extends BaseMapperPlus<SemesterMapper, SemesterDept, SemesterDept>
{

    /**
     * 获取学期分党校信息
     * @param semeId 学期ID
     * @param deptId 部门ID
     * @return 学期分党校信息
     */
    SemesterDept selectSemesterDept(@Param("semeId") Long semeId, @Param("deptId") Long deptId);
}
