package com.psedu.base.service;

import java.util.List;

import com.psedu.base.domain.Semester;
import com.psedu.base.domain.vo.SemeDetailVo;
import com.psedu.base.domain.vo.SimpleSelectSemesterVo;
import com.psedu.base.domain.vo.SimpleSemesterVo;

/**
 * 学期Service接口
 * 
 * @author mingyue
 * @date 2022-02-09
 */
public interface ISemesterService 
{
    /**
     * 查询学期
     * 
     * @param semeId 学期主键
     * @return 学期
     */
    public Semester selectSemesterBySemeId(Long semeId);

    /**
     * 查询学期列表
     * 
     * @param semester 学期
     * @return 学期集合
     */
    public List<Semester> selectSemesterList(Semester semester);

    /**
     * 新增学期
     * 
     * @param semester 学期
     * @return 结果
     */
    public int insertSemester(Semester semester);

    /**
     * 修改学期
     * 
     * @param semester 学期
     * @return 结果
     */
    public int updateSemester(Semester semester);

    /**
     * 修改学期
     *
     * @param semester 学期
     * @return 结果
     */
    public int updateSemesterStatus(Semester semester);

    /**
     * 批量删除学期
     * 
     * @param semeIds 需要删除的学期主键集合
     * @return 结果
     */
    public int deleteSemesterBySemeIds(Long[] semeIds);

    /**
     * 删除学期信息
     * 
     * @param semeId 学期主键
     * @return 结果
     */
    public int deleteSemesterBySemeId(Long semeId);

    /**
     * 下拉简单学期列表
     * @return 学期简单列表
     */
    List<SimpleSelectSemesterVo> simpleSelectList();

    /**
     * 可以报名的学期
     * @return 可以报名的学期
     */
    List<SimpleSemesterVo> canSignSemester();

    Integer getTrainObjectBySemesterId(Long semeId);

    SemeDetailVo getSemesterDetail(Long semeId);
}
