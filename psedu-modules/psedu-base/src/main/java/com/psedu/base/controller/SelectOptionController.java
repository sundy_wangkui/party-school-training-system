package com.psedu.base.controller;

import com.psedu.base.domain.vo.SemeAndPsDeptSelectVo;
import com.psedu.base.service.SelectOptionService;
import com.psedu.common.core.web.domain.AjaxResult;
import com.psedu.common.security.annotation.RequiresLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/selectOption")
public class SelectOptionController {
    @Autowired
    private SelectOptionService selectOptionService;

    @RequiresLogin
    @GetMapping(value = "/semeAndPsDept")
    public AjaxResult getSemeAndPsDept() {
        SemeAndPsDeptSelectVo semeAndPsDeptSelectVo = selectOptionService.getSemeAndPsDept();
        return AjaxResult.success(semeAndPsDeptSelectVo);
    }

    @RequiresLogin
    @GetMapping(value = "/psDeptAndExam")
    public AjaxResult getPsDeptAndExam() {
        return AjaxResult.success(selectOptionService.getPsDeptAndExam());
    }

}
