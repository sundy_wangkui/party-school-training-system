package com.psedu.base.domain.vo;

import lombok.Data;

@Data
public class StudentPositionVo {
    private Double lat;
    private Double lng;
}
