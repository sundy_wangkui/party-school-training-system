package com.psedu.base.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class SaveMergeClassVo {
    /** 学期ID */
    private Long semeId;

    /** 分班ID */
    private Long mergeClassId;

    /** 要修改的部门ID列表 */
    private List<Long> deptIds;
}
