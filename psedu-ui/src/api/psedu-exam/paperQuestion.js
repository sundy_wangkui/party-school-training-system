import request from '@/utils/request'

// 查询试卷题目列表
export function listPaperQuestion(query) {
  return request({
    url: '/psedu-exam/paperQuestion/list',
    method: 'get',
    params: query
  })
}

// 查询试卷题目详细
export function getPaperQuestion(paperQuestionId) {
  return request({
    url: '/psedu-exam/paperQuestion/' + paperQuestionId,
    method: 'get'
  })
}

// 新增试卷题目
export function addPaperQuestion(data) {
  return request({
    url: '/psedu-exam/paperQuestion',
    method: 'post',
    data: data
  })
}

// 修改试卷题目
export function updatePaperQuestion(data) {
  return request({
    url: '/psedu-exam/paperQuestion',
    method: 'put',
    data: data
  })
}

// 删除试卷题目
export function delPaperQuestion(paperQuestionId) {
  return request({
    url: '/psedu-exam/paperQuestion/' + paperQuestionId,
    method: 'delete'
  })
}
