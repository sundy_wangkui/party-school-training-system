import request from '@/utils/request'

// 查询题目选项列表
export function listAnswerOption(query) {
  return request({
    url: '/psedu-exam/answerOption/list',
    method: 'get',
    params: query
  })
}

// 查询题目选项详细
export function getAnswerOption(answerOptionId) {
  return request({
    url: '/psedu-exam/answerOption/' + answerOptionId,
    method: 'get'
  })
}

// 新增题目选项
export function addAnswerOption(data) {
  return request({
    url: '/psedu-exam/answerOption',
    method: 'post',
    data: data
  })
}

// 修改题目选项
export function updateAnswerOption(data) {
  return request({
    url: '/psedu-exam/answerOption',
    method: 'put',
    data: data
  })
}

// 删除题目选项
export function delAnswerOption(answerOptionId) {
  return request({
    url: '/psedu-exam/answerOption/' + answerOptionId,
    method: 'delete'
  })
}
