import request from '@/utils/request'

// 查询答卷列表
export function listAnswerSheet(query) {
  return request({
    url: '/psedu-exam/answerSheet/list',
    method: 'get',
    params: query
  })
}

// 查询答卷详细
export function getAnswerSheet(answerSheetId) {
  return request({
    url: '/psedu-exam/answerSheet/' + answerSheetId,
    method: 'get'
  })
}

// 新增答卷
export function addAnswerSheet(data) {
  return request({
    url: '/psedu-exam/answerSheet',
    method: 'post',
    data: data
  })
}

// 修改答卷
export function updateAnswerSheet(data) {
  return request({
    url: '/psedu-exam/answerSheet',
    method: 'put',
    data: data
  })
}

// 删除答卷
export function delAnswerSheet(answerSheetId) {
  return request({
    url: '/psedu-exam/answerSheet/' + answerSheetId,
    method: 'delete'
  })
}
