import request from '@/utils/request'

// 查询候选题目列表
export function listQuestion(query) {
  return request({
    url: '/psedu-exam/question/list',
    method: 'get',
    params: query
  })
}

// 查询候选题目详细
export function getQuestion(questionId) {
  return request({
    url: '/psedu-exam/question/' + questionId,
    method: 'get'
  })
}

// 新增候选题目
export function addQuestion(data) {
  return request({
    url: '/psedu-exam/question',
    method: 'post',
    data: data
  })
}

// 修改候选题目
export function updateQuestion(data) {
  return request({
    url: '/psedu-exam/question',
    method: 'put',
    data: data
  })
}

// 删除候选题目
export function delQuestion(questionId) {
  return request({
    url: '/psedu-exam/question/' + questionId,
    method: 'delete'
  })
}
