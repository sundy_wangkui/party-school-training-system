import request from '@/utils/request'

// 查询分党校考试信息列表
export function listExamDeptStatus(query) {
  return request({
    url: '/psedu-exam/examDeptStatus/list',
    method: 'get',
    params: query
  })
}

export function deptExamView(examId, deptId) {
  return request({
    url: `/psedu-exam/examDeptStatus/deptExamView/${examId}`,
    method: 'get',
    params: {deptId}
  })
}

// 查询分党校考试信息详细
export function getExamDeptStatus(examDeptStatusId) {
  return request({
    url: '/psedu-exam/examDeptStatus/' + examDeptStatusId,
    method: 'get'
  })
}

// 新增分党校考试信息
export function addExamDeptStatus(data) {
  return request({
    url: '/psedu-exam/examDeptStatus',
    method: 'post',
    data: data
  })
}

// 修改分党校考试信息
export function updateExamDeptStatus(data) {
  return request({
    url: '/psedu-exam/examDeptStatus',
    method: 'put',
    data: data
  })
}


// 修改考试开始时间
export function updateStartTime(data) {
  return request({
    url: '/psedu-exam/examDeptStatus/startTime',
    method: 'put',
    data: data
  })
}

// 修改考试开始时间
export function nextStatus(examDeptStatusId) {
  return request({
    url: `/psedu-exam/examDeptStatus/updateExamStatus/nextStatus/${examDeptStatusId}`,
    method: 'put'
  })
}


// 删除分党校考试信息
export function delExamDeptStatus(examDeptStatusId) {
  return request({
    url: '/psedu-exam/examDeptStatus/' + examDeptStatusId,
    method: 'delete'
  })
}
