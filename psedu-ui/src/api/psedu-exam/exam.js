import request from '@/utils/request'

// 查询考试发起列表
export function listExam(query) {
  return request({
    url: '/psedu-exam/exam/list',
    method: 'get',
    params: query
  })
}

// 查询考试发起详细
export function getExam(examId) {
  return request({
    url: '/psedu-exam/exam/' + examId,
    method: 'get'
  })
}

// 新增考试发起
export function addExam(data) {
  return request({
    url: '/psedu-exam/exam',
    method: 'post',
    data: data
  })
}

// 修改考试发起
export function updateExam(data) {
  return request({
    url: '/psedu-exam/exam',
    method: 'put',
    data: data
  })
}

// 修改考试状态
export function updateExamAllowStart(examId, status) {
  return request({
    url: `/psedu-exam/exam/allowStart/${examId}/${status}`,
    method: 'put'
  })
}

// 删除考试发起
export function delExam(examId) {
  return request({
    url: '/psedu-exam/exam/' + examId,
    method: 'delete'
  })
}
