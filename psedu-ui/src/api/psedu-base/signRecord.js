import request from '@/utils/request'

// 查询签到记录列表
export function listSignRecord(query) {
  return request({
    url: '/psedu-base/signRecord/list',
    method: 'get',
    params: query
  })
}

// 查询未签到记录列表
export function listUnSignStudent(query) {
  return request({
    url: '/psedu-base/signRecord/unSignStudentList',
    method: 'get',
    params: query
  })
}

// 查询签到记录详细
export function getSignRecord(signRecordId) {
  return request({
    url: '/psedu-base/signRecord/' + signRecordId,
    method: 'get'
  })
}

// 新增签到记录
export function addSignRecord(data) {
  return request({
    url: '/psedu-base/signRecord',
    method: 'post',
    data: data
  })
}

// 修改签到记录
export function updateSignRecord(data) {
  return request({
    url: '/psedu-base/signRecord',
    method: 'put',
    data: data
  })
}

// 删除签到记录
export function delSignRecord(signRecordId) {
  return request({
    url: '/psedu-base/signRecord/' + signRecordId,
    method: 'delete'
  })
}
