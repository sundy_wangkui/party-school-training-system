import request from '@/utils/request'

// 查询学期 合班数据
export function getMergeClass(semeId) {
  return request({
    url: `/psedu-base/mergeClass/info/${semeId}`,
    method: 'get'
  })
}

export function saveMergeClass(mergeClassData) {
  return request({
    url: `/psedu-base/mergeClass/info`,
    method: 'put',
    data: mergeClassData
  })
}
