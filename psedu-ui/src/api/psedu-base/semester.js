import request from '@/utils/request'

export function getSemesterDetail(semeId) {
  return request({
    url: `/psedu-base/semester/semesterDetail/${semeId}`,
    method: 'get'
  })
}

// 查询学期列表
export function listSemester(query) {
  return request({
    url: '/psedu-base/semester/list',
    method: 'get',
    params: query
  })
}

// 查询简单学期列表
export function listSimpleSemester() {
  return request({
    url: '/psedu-base/semester/simpleSelectList',
    method: 'get'
  })
}

// 查询学期详细
export function getSemester(semeId) {
  return request({
    url: '/psedu-base/semester/' + semeId,
    method: 'get'
  })
}

// 新增学期
export function addSemester(data) {
  return request({
    url: '/psedu-base/semester',
    method: 'post',
    data: data
  })
}

// 修改学期
export function updateSemester(data) {
  return request({
    url: '/psedu-base/semester',
    method: 'put',
    data: data
  })
}

// 修改学期报名状态
export function updateSemesterActive(data) {
  return request({
    url: '/psedu-base/semester/active',
    method: 'put',
    data: data
  })
}

// 修改学期报名状态
export function updateSemesterAllowEvaluate(data) {
  return request({
    url: '/psedu-base/semester/allowEvaluate',
    method: 'put',
    data: data
  })
}

// 删除学期
export function delSemester(semeId) {
  return request({
    url: '/psedu-base/semester/' + semeId,
    method: 'delete'
  })
}
