import request from '@/utils/request'

// 查询发起签到列表
export function listSignLaunch(query) {
  return request({
    url: '/psedu-base/signLaunch/list',
    method: 'get',
    params: query
  })
}

// 查询发起签到详细
export function getSignLaunch(signLaunchId) {
  return request({
    url: '/psedu-base/signLaunch/' + signLaunchId,
    method: 'get'
  })
}

// 新增发起签到
export function addSignLaunch(data) {
  return request({
    url: '/psedu-base/signLaunch',
    method: 'post',
    data: data
  })
}

// 修改发起签到
export function updateSignLaunch(data) {
  return request({
    url: '/psedu-base/signLaunch',
    method: 'put',
    data: data
  })
}

// 删除发起签到
export function delSignLaunch(signLaunchId) {
  return request({
    url: '/psedu-base/signLaunch/' + signLaunchId,
    method: 'delete'
  })
}
