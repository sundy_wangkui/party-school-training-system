import request from '@/utils/request'

// 查询学期部门列表
export function semeAndPsDeptOption() {
  return request({
    url: '/psedu-base/selectOption/semeAndPsDept',
    method: 'get'
  })
}


// 查询部门、考试列表
export function psDeptAndExamOption() {
  return request({
    url: '/psedu-base/selectOption/psDeptAndExam',
    method: 'get'
  })
}
