import request from '@/utils/request'

// 查询培训报名列表
export function listApply(query) {
  return request({
    url: '/psedu-base/apply/list',
    method: 'get',
    params: query
  })
}

// 查询培训报名详细
export function getApply(applyId) {
  return request({
    url: '/psedu-base/apply/' + applyId,
    method: 'get'
  })
}

// 新增培训报名
export function addApply(data) {
  return request({
    url: '/psedu-base/apply',
    method: 'post',
    data: data
  })
}

// 修改培训报名
export function updateApply(data) {
  return request({
    url: '/psedu-base/apply',
    method: 'put',
    data: data
  })
}

// 通过培训报名
export function applyPass(applyId) {
  return request({
    url: `/psedu-base/apply/pass/${applyId}`,
    method: 'put'
  })
}

// 删除培训报名
export function delApply(applyId) {
  return request({
    url: '/psedu-base/apply/' + applyId,
    method: 'delete'
  })
}

// 报名分组
export function divideGroup(groupId, applyIds) {
  return request({
    url: '/psedu-base/group/studentDivideGroup',
    method: 'put',
    data: {
      groupId,
      applyIds
    }
  })
}
