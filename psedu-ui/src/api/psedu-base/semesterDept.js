import request from '@/utils/request'
// 查询学期分党校详细
export function getSemesterDept(semeId, deptId) {
  return request({
    url: `/psedu-base/semesterDept/${semeId}/${deptId}`,
    method: 'get'
  })
}

// 修改学期分党校
export function saveSemesterDept(data) {
  return request({
    url: '/psedu-base/semesterDept',
    method: 'put',
    data: data
  })
}
