import { download } from '@/utils/request'
import { parseTime } from '@/utils/ruoyi'

export function downloadSeatExcel(semeId, trainName) {
  download(`/psedu-base/seat/info/${semeId}`, {}, `${trainName}-${parseTime(new Date(), '{y}-{m}-{d}')}.xls`);
}
