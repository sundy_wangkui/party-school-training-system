import request from '@/utils/request'
import { download } from '@/utils/request'
import { parseTime } from '@/utils/ruoyi'

// 查询学员培训情况
export function trainDataList(query) {
  return request({
    url: '/psedu-base/student/trainDataList',
    method: 'get',
    params: query
  })
}

export function downloadCertificationExcel(semeId, deptId) {
  download(`/psedu-base/student/downloadCertificate/${semeId}/${deptId}`, {}, `结业证书-${parseTime(new Date(), '{y}-{m}-{d}')}.xls`);
}
