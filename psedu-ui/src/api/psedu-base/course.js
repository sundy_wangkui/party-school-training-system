import request from '@/utils/request'

// 查询课程列表
export function listCourse(query) {
  return request({
    url: '/psedu-base/course/list',
    method: 'get',
    params: query
  })
}

// 直播状态改变
export function liveCourseStatus(courseId, liveStatus) {
  return request({
    url: `/psedu-base/course/liveCourseStatus/${courseId}/${liveStatus}`,
    method: 'get'
  })
}

// 查询课程详细
export function getCourse(courseId) {
  return request({
    url: '/psedu-base/course/' + courseId,
    method: 'get'
  })
}

// 新增课程
export function addCourse(data) {
  return request({
    url: '/psedu-base/course',
    method: 'post',
    data: data
  })
}

// 修改课程
export function updateCourse(data) {
  return request({
    url: '/psedu-base/course',
    method: 'put',
    data: data
  })
}

// 删除课程
export function delCourse(courseId) {
  return request({
    url: '/psedu-base/course/' + courseId,
    method: 'delete'
  })
}
