# 测试环境部署脚本
set -e

# 构建测试版本
npm run build:prod

# cd 到构建输出的目录下
cd dist

# 127.0.0.1
scp -r * root@127.0.0.1:/var/www/dangxiao_admin
